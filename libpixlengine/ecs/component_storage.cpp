#include "ecs/component_storage.hpp"
#include <cassert>
#include "ecs/entity.hpp"

namespace pixl::ecs {
	std::unordered_map<std::string,ComponentStore::ComponentInfo> ComponentStore::m_staticStore;
	//make sure this had a default constructor otherwise this ~~~~~~~~~~~~~~~~~~~~~~~~~^
	//will mock up
	ComponentStore::ComponentInfo::ComponentInfo() : index(typeid(Entity::null_component)) {}

	//might as well add constructor for everything now
	ComponentStore::ComponentInfo::ComponentInfo(
		const cctor_t& ctor, std::string_view n,std::type_index idx) :
			ctor_inplaceof{ctor},name{n},index{idx}
	{
		
	}

	ComponentStore::ComponentInfo ComponentStore::getComponentInfo(std::string_view name)
	{
		//get an iterator
		decltype(m_staticStore)::iterator i;
		if((i = m_staticStore.find(std::string(name))) != m_staticStore.end()) {

			//if the object was found return it.
			return i->second;
		}

		//otherwise return a bunch of nothing
		return ComponentInfo{};
	}

	void ComponentStore::registerComponent(const cctor_t& ctor, std::string_view name,
	                                       std::type_index idx)
	{
		//make the info
		const ComponentInfo info(ctor,name,idx);

		///
		assert(m_staticStore.find(std::string(name)) == m_staticStore.end() ); // cannot register two components with the same name!
		///

		//save the info
		m_staticStore[std::string(name)] = info;

	}
}
