#pragma once

//The Entity Component System has forward-declarations 
//hence this file provides a method to include all necessary files at once
//without getting nasty linker errors

#include "ecs/component_storage.hpp"
#include "ecs/component_base.hpp"
#include "ecs/entity.hpp"