#include "ecs/builtin_transform.hpp"
#include "ecs/component_container.hpp"
#include "ecs/entity.hpp"
#include "graphics/graphics_buffer.hpp"
#include "graphics/perspective.hpp"


typedef std::tuple<pixl::math::vec3f::iterator, pixl::math::vec3f::iterator> vec3_pairs_payload;

//vec3s 
//TODO(algorythmix) this should be implemented in vec3 tbh
const auto v3up = pixl::math::vec3f{ 0.f,0.f,1.f };
const auto v3right = pixl::math::vec3f{ 1.0f,0.0f,0.0f };
const auto v3back = pixl::math::vec3f{ 0.0f,1.0f,0.0f };

//this allows lua to traverse a vec3 element as if it was an array
auto vec3_next(sol::user<vec3_pairs_payload&> tuple, sol::this_state l)
{
	auto&[start, end] = tuple.value;
	if (start == end)
	{
		return std::make_tuple(
			sol::object(sol::nil),
			sol::object(sol::nil)
		);
	}
	char key = '~';
	switch (end - start){
		case  3: key = 'x'; break;
		case  2: key = 'y'; break;
		case  1: key = 'z'; break;
		default:break;
	}
	auto  r = std::make_tuple(sol::object(l, sol::in_place, key ), sol::object(l, sol::in_place,*start));
	std::advance(start, 1);
	return r;

};

//this wraps the array accessor in a pairs() way
auto vec3_pairs(pixl::math::vec3f &v) {
	return std::make_tuple(&vec3_next, sol::user<vec3_pairs_payload>({ v.begin(), v.end() }), sol::nil);
}

//this is the same as above but with an iterator
auto vec3_inext(sol::user<vec3_pairs_payload&> tuple, sol::this_state l)
{
	auto&[start, end] = tuple.value;
	if (start == end)
	{
		return std::make_tuple(
			sol::object(sol::nil),
			sol::object(sol::nil)
		);
	}
	auto  r = std::make_tuple(sol::object(l, sol::in_place, std::distance(start,end)), sol::object(l, sol::in_place, *start));
	std::advance(start, 1);
	return r;
}

//and similarly this is for ipairs()
auto vec3_ipairs(pixl::math::vec3f &v) {
	return std::make_tuple(&vec3_inext, sol::user<vec3_pairs_payload>({ v.begin(), v.end() }), sol::nil);
}

pixl::ecs::luav_delegate pixl::ecs::TransformComponent::lua_registration = luav_delegate::create([](sol::state_view interpreter)
{
	//register the Base Type TransformComponent
	auto type = interpreter.new_usertype<TransformComponent>("Transform", "getName",&TransformComponent::getName ,
		"getPriority",&TransformComponent::getPriority,
		"from", [](ComponentContainer<ComponentBase> & cc)
	{
		if(!cc.is<TransformComponent>())
			throw std::runtime_error("lua-vm bad-pointer-cast");
		return cc.cast<TransformComponent>().lock();
	});
	
	//register the members
	type["position"] = &TransformComponent::position;
	type["rotation"] = &TransformComponent::rotation;
	
	//register translation
	type["translate"] = [](TransformComponent* tc,math::vec3f v)
	{
		return (tc->position += v);
	};

	//register rotation
	type["rotate"] = [](TransformComponent* tc, math::vec3f axis,math::angle<float> angle)
	{
		tc->rotation.rotate_axis(angle, axis);
	};

	//register what an angle is
	auto angle_type = interpreter.new_usertype<math::angle<float>>("angle");

	//register all the functions of an angle
	angle_type["deg"] = &math::angle<float>::deg;
	angle_type["rad"] = &math::angle<float>::rad;
	angle_type["as_deg"] = &math::angle<float>::as_deg;
	angle_type["as_rad"] = &math::angle<float>::as_rad;
	angle_type["sin"] = &math::angle<float>::sin<false>;
	angle_type["cos"] = &math::angle<float>::cos<false>;
	angle_type["tan"] = &math::angle<float>::tan<false>;
	angle_type["acos"] = &math::angle<float>::acos<false>;
	angle_type["asin"] = &math::angle<float>::asin<false>;
	angle_type["atan"] = &math::angle<float>::atan<false>;
	angle_type["atan2"] = &math::angle<float>::atan2<false>;


	//register what a vector is
	auto vec_type = interpreter.new_usertype<math::vec3f>(
		"vec3",
		sol::constructors<math::vec3f(), math::vec3f(float, float, float)>(),
		sol::meta_function::pairs, vec3_pairs,
		"pairs",vec3_pairs,
		sol::meta_function::ipairs, vec3_ipairs,
		"ipairs", vec3_ipairs
	
	);

	//register the x of a vec3
	vec_type["x"] = sol::property(
		static_cast<math::vec3f::type(math::vec3f::*)() const noexcept>(&math::vec3f::get<0>),
		static_cast<void(math::vec3f::*)(const math::vec3f::type&) noexcept>(&math::vec3f::set<0>)
	);

	//register the y of a vector
	vec_type["y"] = sol::property(
		static_cast<math::vec3f::type(math::vec3f::*)() const noexcept>(&math::vec3f::get<1>),
		static_cast<void(math::vec3f::*)(const math::vec3f::type&) noexcept>(&math::vec3f::set<1>)
	);

	//register the z of a vector
	vec_type["z"] = sol::property(
		static_cast<math::vec3f::type(math::vec3f::*)() const noexcept>(&math::vec3f::get<2>),
		static_cast<void(math::vec3f::*)(const math::vec3f::type&) noexcept>(&math::vec3f::set<2>)
	);

	//register some default vectors
	vec_type["up"] = sol::var(std::ref(v3up));
	vec_type["back"] = sol::var(std::ref(v3back));
	vec_type["right"] = sol::var(std::ref(v3right));


	//register what a quaternions is
	auto quaternion_type = interpreter.new_usertype<math::quaternion<float>>("quaternion");

	//and what it can do
	quaternion_type["scalar"] = &math::quaternion<float>::scalar;
	quaternion_type["vector"] = &math::quaternion<float>::vector;
	quaternion_type["length"] = &math::quaternion<float>::length;
	quaternion_type["unit"] =   &math::quaternion<float>::to_unit;
	quaternion_type["inverse"] = &math::quaternion<float>::inverse;
	quaternion_type["conjugate"] = &math::quaternion<float>::conjugate;
	quaternion_type["rotate_vec"] = &math::quaternion<float>::rotate_vec3;


});

bool operator==(const pixl::math::mat4f& lhs,const pixl::math::mat4f & rhs)
{
	for(int i = 0;i < 16;++i)
	{
		if (lhs.data[i] != rhs.data[i]) return false;
	}
	return true;
}

void pixl::ecs::TransformComponent::onUpdate()
{
	rotation = rotation.to_unit();
	float& qw = rotation.scalar;
	float& qx = rotation.vector.x;
	float& qy = rotation.vector.y;
	float& qz = rotation.vector.z;


	// this is some matrix-magic ... for whatever reason this produces the corresponding
	// rotation matrix from the quaternion
	// it is very crucial however that the rotation is normalized first
	
	const math::mat4f A
	(
		 qw,  qz, -qy, qx,
		-qz,  qw,  qx, qy,
		 qy, -qx,  qw, qz,
		-qx, -qy, -qz, qw
	);

	const math::mat4f B
	(
		 qw,  qz, -qy, -qx,
		-qz,  qw,  qx, -qy,
		 qy, -qx,  qw, -qz,
		 qx,  qy,  qz,  qw

	);

	//get the translation matrix 
	const math::mat4f POS = pixl::graphics::translate(math::mat4f::identity(), position);

	//multiply everything and update the Transform of the Parent Object
	getParent()->TransformationMatrix = POS * (A*B);
}
