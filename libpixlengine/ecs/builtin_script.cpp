
#include "ecs/builtin_script.hpp"
#include "ecs/entity.hpp"
#include "ecs/component_container.hpp"
#include "util/container_utils.hpp"

#include <SOL/variadic_args.hpp>

#include <any>

#define QUALIFY(THIS,THAT,FN_RET,...) static_cast<FN_RET (THIS::*)(__VA_ARGS__)>(&THIS:: THAT)

sa::multicast_delegate<void(sol::state_view)> pixl::ecs::ScriptComponent::prebuildEvents;

namespace pixl::ecs
{

	//catch sol errors
	void lippincott_sol()
	{
		try
		{
			throw;
		} catch (sol::error &e)
		{
			core::println_warning("{}", e.what());
		}
	}
	#define protect_sol() catch(...) { lippincott_sol(); }

	ScriptComponent::ScriptComponent(uint8_t priority) : ComponentBase("ScriptComponent", priority)
	{

		//init sol interpreter
		m_interpreter.open_libraries(
			sol::lib::base,
			sol::lib::math,
			sol::lib::string,
			sol::lib::utf8,
			sol::lib::package,
			sol::lib::table,
			sol::lib::debug
		);
	}

	ScriptComponent::ScriptComponent(const ScriptComponent& other) : ComponentBase(other){}
	ScriptComponent::ScriptComponent(ScriptComponent&& other) noexcept: ComponentBase(std::move(other)){}

	ScriptComponent& ScriptComponent::operator=(const ScriptComponent& other)
	{
		if (this == &other)
			return *this;
		ComponentBase::operator =(other);
		return *this;
	}

	ScriptComponent& ScriptComponent::operator=(ScriptComponent&& other) noexcept
	{
		if (this == &other)
			return *this;
		ComponentBase::operator =(std::move(other));
		return *this;
	}

	std::string prx_to_string(const sol::stack_proxy&& prx)
	{
		return prx.get<std::string>();
	}


	void ScriptComponent::onInit()
	{

		//ENGINE "Namespace"
		auto engine_table = m_interpreter["engine"].get_or_create<sol::table>();
		{

			//setup meta-table
			auto meta_table = engine_table[sol::create_if_nil]["meta"];
			meta_table["name"] = m_script_name;



			//seed the rng
			engine_table.set_function("rng_seed", [] {return time(NULL); });

			//this can be used to check if your script-based multithreading branched correctly (usually an library-internal)
			engine_table.set_function("thread_id", [] {	std::stringstream ss; ss << std::this_thread::get_id(); return ss.str(); });

			//push an error to the console
			engine_table.set_function("error",[](sol::variadic_args args)
			{
				pixl::core::println_error("lua: {}", fmt::join(
					pixl::util::inline_transform<std::string,std::vector>(args.begin(), args.end(), prx_to_string),
					" "
				));
			});

			//push a warning to the console
			engine_table.set_function("warning", [](sol::variadic_args args)
			{
				pixl::core::println_warning("lua: {}", fmt::join(
					pixl::util::inline_transform<std::string, std::vector>(args.begin(), args.end(), prx_to_string),
					" "
				));
			});

			//push some informational stuff to the consolse
			engine_table.set_function("info", [](sol::variadic_args args)
			{
				pixl::core::println_debug("lua: {}", fmt::join(
					pixl::util::inline_transform<std::string, std::vector>(args.begin(), args.end(), prx_to_string),
					" "
				));
			});
		}


		//ENTITY "Type"
		auto entity_type = m_interpreter.new_usertype<Entity>("Entity");
		{
			entity_type["getName"] = &Entity::getName;
			entity_type["getComponent"] = QUALIFY(Entity, getComponent, ComponentContainer<ComponentBase>, std::string_view);
			entity_type["hasComponent"] = QUALIFY(Entity, hasComponent, bool, std::string_view);
			entity_type["addComponent"] = QUALIFY(Entity, addComponent, ComponentContainer<ComponentBase>, std::string_view);
			entity_type["addChild"] =	  QUALIFY(Entity, addChild, std::shared_ptr<Entity>, std::string_view);
			entity_type["getChild"] = [](Entity* e, std::string_view name) {return e->getChildByName(name).lock(); };
			entity_type["enumerateChildren"] = &Entity::getChildren;
			entity_type["getDelta"] = &Entity::getSimulationDelta;

			m_interpreter["parent"] = getParent();

		}


		//COMPONENT-"Preamble"
		auto ccontainer_type = m_interpreter.new_usertype<ComponentContainer<ComponentBase>>("CContainer");
		{
			ccontainer_type["retrieve"] = [](ComponentContainer<ComponentBase>* c)
			{
				return c->lock();
			};
		}

		//COMPONENT-"Generic"
		auto component_type = m_interpreter.new_usertype<ComponentBase>("ComponentGeneric", COMPONENT_USERTYPE_BASE(ComponentBase));

		//COMPONENT-"Specialized(Script)"
		auto this_type = m_interpreter.new_usertype<ScriptComponent>("ScriptComponent", COMPONENT_NEW_USERTYPE(ScriptComponent));
		{
			m_interpreter["this"] = this;
		}


		prebuildEvents(static_cast<sol::state_view>(m_interpreter));

		try
		{
			//execute the preamble
			m_interpreter.script
			(
				#include "ecs/builtin_detail/script/new_script.preamble.lua.hpp"
				,"STATIC-PREAMBLE"
			);

			//execute the user script
			m_interpreter.script(m_script,m_script_name);

			//call onInit
			if (m_interpreter["onInit"].get_type() == sol::type::function)
			{
				m_interpreter["onInit"]();
			} else if (m_interpreter["Script"].get_type() == sol::type::table && m_interpreter["Script"]["init"] == sol::type::function)
			{
				m_interpreter["Script"]["init"](m_interpreter["Script"]);
			}
		}

		//make sure lua didn't blow up
		protect_sol()
	}



#if not defined(_DEBUG) && not defined(DEBUG)
	//check if the state needs to be reloaded because it was modified
	bool state_dirty = false;
#endif

	void ScriptComponent::onUpdate()
	{
		//check if the state is locked
		if (!m_can_use_state) return;


//check if we should apply runtime optimizations
#if defined (_DEBUG) || defined (DEBUG)

		//get the Script.update function
		static auto mfun = m_interpreter["Script"]["update"];

		//get the Script
		static auto self = m_interpreter["Script"];

		//first try onUpdate
		if (m_interpreter["onUpdate"].get_type() == sol::type::function) {
			m_interpreter["onUpdate"]();
		}

		//then try Script.update(self)
		else if (self.get_type() == sol::type::table && mfun.get_type() == sol::type::function)
		{
			mfun(m_interpreter["Script"]);
		}
#else

		//static runtime optimizations
		static bool ok = m_interpreter["onUpdate"].get_type() == sol::type::function;
		static auto fun = m_interpreter["onUpdate"];
		static auto mfun = m_interpreter["Script"]["update"];
		static auto self = m_interpreter["Script"];
		static bool mok = self.get_type() == sol::type::table && mfun.get_type() == sol::type::function;

		//reload the state if the script was modified
		if(state_dirty)
		{
			ok = m_interpreter["onUpdate"].get_type() == sol::type::function;
			fun = m_interpreter["onUpdate"];
			mfun = m_interpreter["Script"]["update"];
			self = m_interpreter["Script"];
			mok = self.get_type() == sol::type::table && mfun.get_type() == sol::type::function;
			state_dirty = false;
		}

		//check if the onUpdate function is usable and use that one
		if (ok) fun();

		//or use the member functions Script.update
		else if (mok) mfun(m_interpreter["Script"]);
#endif
	}



	void ScriptComponent::reload()
	{
		//lock state
		m_can_use_state = false;


		//recreate the interpreter
		m_interpreter = sol::state();
		m_interpreter.open_libraries(
			sol::lib::base,
			sol::lib::math,
			sol::lib::string,
			sol::lib::utf8,
			sol::lib::package,
			sol::lib::table,
			sol::lib::debug
		);

		#if not defined(_DEBUG) && not defined(DEBUG)
			state_dirty = true;
		#endif

		//reload everything
		onInit();

		//reenable execution
		m_can_use_state = true;

	}


	void ScriptComponent::onDestroy()
	{
		try {
			//call the destroy handler
			if (m_interpreter["onDestroy"].get_type() == sol::type::function)	m_interpreter["onDestroy"]();
			else if (m_interpreter["Script"].get_type() == sol::type::table && m_interpreter["Script"]["destroy"] == sol::type::function)
			{
				m_interpreter["Script"]["destroy"](m_interpreter["Script"]);
			}
		}
		protect_sol()
	}


	//load a script that will be executed on init
	void ScriptComponent::loadScript(core::resource res)
	{
		m_script = res.str();
	}

	void ScriptComponent::loadScript(io::DiskView view)
	{
		m_script_name = view.filename();

		loadScript(static_cast<core::resource_base<>>(view.read()));
	}

	//execute something direclty upon the sol::state
	void ScriptComponent::execute(luav_delegate func) const
	{
		func(static_cast<sol::state_view>(m_interpreter));
	}
}
#undef protect_sol
