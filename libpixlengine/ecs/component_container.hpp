#pragma once
#include "ecs/component_base.hpp"

#include <mutex>
#include <atomic>

namespace pixl::ecs {


	template <class T>
	class ComponentContainer
	{
	public:
		ComponentContainer(std::weak_ptr<T> ptr) : m_ptr{std::move(ptr)}
		{
		}

		ComponentContainer(nullptr_t) : m_ptr(std::reinterpret_pointer_cast<T>(std::shared_ptr<void*>(nullptr)))
		{
		}


		/** @brief locks the component for save use 
		 * this will acquire the pointer from entity and lock the mutex for this component thus ensuring thread-safety
		 * (note that individual components of the Component might still be unsafe to use, this depends on the implementation of the component)
		 * @nodiscard
		 */
		PIXL_NODISCARD std::shared_ptr<T> lock() const
		{
			auto object = m_ptr.lock();
			if(!locked) object->mtx.lock();
			locked = true;
			return object;
		}

		/** @brief this releases the component, all further operations on that component may yield undefined behaviour
		 */
		void release() const
		{
			if(locked){
				locked = false;

				m_ptr.lock()->mtx.unlock();
			}
		}

		/**@brief this is just like release, in order to make it compatible with std::lock_guard
		 */
		void unlock() const
		{
			release();
		}

		//just in case the user forgets or uses raii
		~ComponentContainer()
		{
			release();
		}

		/**@brief this casts the ComponentContainer from a Component X to a Component of Type Y
		 *i.e.:
		 *@code
		 * ComponentContainer<ComponentBase> script = getComponent("ScriptComponent");
		 * ComponentContainer<ScriptComponent> actual_script = script.cast<ScriptComponent>();
		 *@endcode
		 */
		template <class X>
		auto cast()
		{
			return ComponentContainer<X>(std::reinterpret_pointer_cast<X>(m_ptr.lock()));
		}

		/**@brief checks if the ComponentContainer is secretly of a different Type X
		 *i.e.:
		 *@code
		 *
		 * ComponentContainer<ComponentBase> something = getComponent("ScriptComponent");
		 * bool is_something_a_script = something.is<ScriptComponent>(); => true
		 *
		 *@endcode
		 */
		template <class X>
		bool is() const
		{
			return dynamic_cast<X*>(m_ptr.lock().get()) != nullptr;
		}
		
		//returns the raw pointer
		//advised to NOT USE THIS
		const T& operator*() const
		{
			return *m_ptr.lock();
		}

		//returns the raw pointer
		//advised to NOT USE THIS
		T& getRaw()
		{
			return *m_ptr.lock();
		}



	private:
		mutable bool locked = false;
		std::weak_ptr<T> m_ptr;
	};

	/**@brief this is a modern system that should be used in favor over locking, as it locks the
	 *Component exactly as long as required
	 *usage:
	 *@code
	 *
	 *ComponentContainer<ScriptComponent> script_container;
	 *actCC(script_container,[](ScriptComponent & script){
	 *	... do something with script...
	 *});
	 *
	 *@endcode
	 *@param [in] obj the Container you want to do something with
	 *@param [in] c the callable you want to modify your Contained Component with, this has to accept the Component as its first param
	 * 
	 */
	template<class Obj,class Callable>
	void actCC(ComponentContainer<Obj>& obj,Callable && c)
	{
		std::unique_lock<std::mutex> lock(obj.getRaw().mtx);
		std::invoke(c,obj.getRaw());
	}

	/**@brief this is a modern system that should be used in favor over locking, as it locks the
	 *Component exactly as long as required
	 *usage:
	 *@code
	 *
	 *ComponentContainer<ScriptComponent> script_container;
	 *actCC(script_container,[](ScriptComponent & script){
	 *	... do something with script...
	 *});
	 *
	 *@endcode
	 *@param [in] obj the Container you want to do something with
	 *@param [in] c the callable you want to modify your Contained Component with, this has to accept the Component as its first param
	 *
	 */
	template<class Obj, class Callable>
	void actCC(ComponentContainer<Obj>& obj, Callable & c)
	{
		std::unique_lock<std::mutex> lock(obj.getRaw().mtx);
		std::invoke(c, obj.getRaw());
	}

}
