#pragma once

#include "ecs/component_base.hpp"
#include "math/vector.hpp"
#include "math/quaternion.hpp"
#include "ecs/builtin_script.hpp"

namespace pixl::ecs
{
	/**@class TransformComponent
	 * @brief represents position and rotation of a simple 3D-Object
	 */
	class TransformComponent : public ComponentBase
	{
	public:

		/**@brief this represents the position of this object as a 3d-vector all usual transformations apply
		 * @note this will be updated on every-call of onUpdate
		 */
		math::vec3f position;
		

		/**@brief this represents the rotation of this object as a quaternion all usual transformations apply
		 * @note this will be updated on every-call of onUpdate
		 */
		math::quaternion<float> rotation;

		TransformComponent() : ComponentBase("Transform"){}

		void onUpdate() override;


		friend bool operator==(const TransformComponent& lhs, const TransformComponent& rhs)
		{
			return lhs.position == rhs.position
				&& lhs.rotation == rhs.rotation;
		}

		friend bool operator!=(const TransformComponent& lhs, const TransformComponent& rhs)
		{
			return !(lhs == rhs);
		}

		//registration in the lua environment
		static luav_delegate lua_registration;
	protected:
		inline core::serialize_output serialize() override
		{
			auto out = ComponentBase::serialize();
			out.serializer["position"] = {
					{"x",position.x},
					{"y",position.y},
					{"z",position.z}
			};
			out.serializer["rotation"] = {
				{"l",rotation.scalar},
				{"i",rotation.vector.x},
				{"j",rotation.vector.y},
				{"k",rotation.vector.z}
			};
			return out;
		}
		inline void deserialize(const core::serialize_input& in) override
		{
			ComponentBase::deserialize(in);
			position.x = in.serializer["position"]["x"].get<float>();
			position.y = in.serializer["position"]["y"].get<float>();
			position.z = in.serializer["position"]["z"].get<float>();

			rotation.scalar   = in.serializer["rotation"]["l"].get<float>();
			rotation.vector.x = in.serializer["rotation"]["i"].get<float>();
			rotation.vector.y = in.serializer["rotation"]["j"].get<float>();
			rotation.vector.z = in.serializer["rotation"]["k"].get<float>();

		}
	};

	//register in the componentStore, call in App::onInit()
	inline void registerTransformComponent()
	{
		ComponentStore::registerComponent<TransformComponent>("Transform");
	}
}
