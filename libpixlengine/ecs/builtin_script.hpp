#pragma once
#include "ecs/component_base.hpp"
#include "ecs/component_storage.hpp"

#include "io/disk_view.hpp"
#include "io/disk_resource.hpp"

#include "core/resource.hpp"
#include "core/fast_delegate.hpp"
#include "core/fast_delegate_mc.hpp"
#include "core/debugging.hpp"

#include <SOL/state.hpp>
#include <thread>
#include <atomic>

namespace pixl {
	namespace io {
		class DiskView;
	}
}

//helper to register new usertypes for components
#define COMPONENT_USERTYPE_BASE(X) "getName",&X::getName , "getPriority",&X::getPriority

//helper to make cast for new component usertypes
#if defined(DEBUG) ||defined (_DEBUG)
#define COMPONENT_CAST_LAMBDA(TYPE) [](ComponentContainer<ComponentBase> & cc){\
	if(!cc.is<TYPE>()) throw std::runtime_error("lua-vm bad-pointer-cast");\
	return cc.cast<TYPE>().lock();\
}
#else
#define COMPONENT_CAST_LAMBDA(TYPE) [](ComponentContainer<ComponentBase> & cc){\
	return cc.cast<TYPE>().lock();\
}
#endif

//helper to register cast with the default word "from" for a new component usertype
#define COMPONENT_CASTER(TYPE) "from", COMPONENT_CAST_LAMBDA(TYPE)


/**@brief helper to register everything for a new component usertype (USE THIS ONE IF YOU ARE UNSURE) \n
 * Example @code
 * ScriptComponent::prebuildEvents += luav_delegate::create([](auto& state) {
 *	auto ut = state.new_usertype<MyCoolComponentType>( COMPONENT_NEW_USERTYPE(MyCoolComponentType) )
 *	ut["someCoolFunction"] = &MyCoolComponentType::func; *	
 * }
 * @endcode
 * @param [in] the type of the Component
 */
#define COMPONENT_NEW_USERTYPE(TYPE) COMPONENT_USERTYPE_BASE(TYPE),COMPONENT_CASTER(TYPE)

namespace pixl::ecs {

	typedef sa::delegate<void(sol::state_view)> luav_delegate;

	/**@class ScriptComponent
	 * @brief loads and executes certain parts of a lua-script
	 * 
	 * @note there is two different versions of compatible lua-script
	 * additionally you can give this script a generic script and have it execute once in onInit() \n
	 * the support script types look like this:
	 * @code
	 * 
	 * Script = engine.script:new{
	 *  ... additional stuff ...
	 * }
	 * 
	 * function Script:update()
	 *  ... your functions ...
	 * end
	 * 
	 * function Script:init()
	 *  ... initilization steps ... i.e.:
	 *  register_components("Transform","ScriptComponent16")
	 * end
	 * 
	 * function Script:destroy()
	 *  ... destructor steps ...
	 * end
	 * @endcode
	 * @note alternatively:
	 * @code
	 * 
	 * function onInit()
	 *  ... init stuff ...
	 * end
	 *  
	 * function onUpdate()
	 *  ... update stuff ...
	 * end
	 * 
	 * function onDestroy()
	 *  ... destroy stuff ...
	 * end
	 * @endcode
	 * 
	 * @note anything that is in the global state will be executed on script load
	 */
	class ScriptComponent : public ComponentBase
	{
	public:

		ScriptComponent(uint8_t priority);
		ScriptComponent(const ScriptComponent& other);
		ScriptComponent(ScriptComponent&& other) noexcept;

		ScriptComponent& operator=(const ScriptComponent& other);
		ScriptComponent& operator=(ScriptComponent&& other) noexcept;

		/**@brief this function is called by the engine
		 * in this step your lua-code will be initialized
		 */
		void onInit() override;
		
		/**@brief this function is called by the engine
		 * in this step your lua-code will be called repeatedly
		 */
		void onUpdate() override;

		/**@brief this function is called by the engine
		* in this step your lua-code will be initialized
		*/
		void onDestroy() override;


		/**@brief this is where you can give the engine a file or a resource to load as a lua-script
		 * @param [in] res the resource you want to load as the script
		 * @note this has to be interpretable as a string and will not give the script a meaningful name
		 */
		void loadScript(core::resource res);
		
		/**@brief this is where you can give the engine a disk location to load as a lua-script
		 * @param [in] view the location of the resource you want to load as the script
		 * @note this has to be interpretable as a string the name of the script will be the file-name
		 */
		void loadScript(io::DiskView view);

		/**@brief this reloads the currently loaded script 
		 * @note naturally you would want to load a new version of the script with @code loadScript(<something>) @endcode first
		 * but you don't have to!
		 *
		 */
		void reload();

		//this executes something directly on the lua-state wrapped in a sol::view
		void execute(luav_delegate func) const;

		//this executes a lambda directly on the lua-state wrapped in a sol::view
		template <class T>
		void execute(T&& lambda) const
		{
			execute(luav_delegate::create(lambda));
		}

		//this executes a lambda directly on the lua-state wrapped in a sol::view
		template <class T>
		void execute(const T& lambda) const
		{
			execute(luav_delegate::create(lambda));
		}

		std::string script(const std::string str)
		{
			auto promise = m_interpreter.script(str,[](lua_State*, sol::protected_function_result pfr)
			{
					return pfr;
			});
			if (promise.status() == sol::call_status::ok)
				return promise.get<std::string>();
			else
				return "error";
		}

		/**@brief this set of static functions will always be executed before the script is executed \n
		 * this can be used to register components, make some tools available or anything that you thing all scripts should possess \n
		 * the syntax is a follows:
		 * @code
		 * ScriptComponent::prebuildEvents += luav_delegate::create<&your_event>();
		 * or
		 * ScriptComponent::prebuildEvents += luav_delegate::create([](auto state){
		 *		state.do_something();
		 *		...
		 * });
		 * or
		 * ScriptComponent::prebuildEvents += luav_delegate::create<CSomething,&CSomething::func>(&instance);
		 * 
		 * @endcode
		 *
		 */
		static sa::multicast_delegate<void(sol::state_view)> prebuildEvents;

	protected:

		std::atomic<bool> m_can_use_state = true;
		sol::state m_interpreter;
		std::string m_script;
		std::string m_script_name = str::empty;


	};

	//literally nothing
	inline void nop() {}

	namespace detail{
		//tricky tricks
		template <std::uint8_t priority_>
		class ScriptComponentTPriority : public ScriptComponent
		{
			public:	ScriptComponentTPriority() : ScriptComponent(priority_) {}
		};
	}

	/**@brief execute this somewhere before creating entities (App::onInit() for instance)
	 * to register a bunch of ScriptComponents with different priorities to the ComponentStore by name
	 * 
	 * @note will register:
	 *  - ScriptComponent0
	 *  - ScriptComponent1
	 *  - ...
	 *  - ScriptComponent<MaxPriority-1>
	 *  - ScriptComponent<MaxPriority>
	 */
	template <std::uint8_t priority_ = COMPONENT_MAX_PRIORITY>
	void registerScriptComponent()
	{
		//this works by using a bit of a dirty trick


		ComponentStore::registerComponent(
		ComponentStore::cctor_t::create([] {
				//here we create a Component of Type ScriptComponentTPriority<priority>
				return reinterpret_cast<ComponentBase*>(new detail::ScriptComponentTPriority<priority_>);
			}),
		fmt::format("ScriptComponent{}", priority_),

			//but here we pretend that it is a standard ScriptComponent ... the only difference between the two is how we call the constructor,
			//which is abstracted anyways
			typeid(ScriptComponent)
		);

		//we do this recursively (in the compiler) until we registered all Priority Levels
		if constexpr (priority_ == 0) return;
		registerScriptComponent<priority_ - 1>();

		//note that this method incidentally means that you cannot ask to the ComponentStore to create a ScriptComponent for you by type
	}

}



