#pragma once
#include "common.hpp"
#include "core/fast_delegate.hpp"

#include <type_traits>
#include <typeindex>
#include <string>
#include <unordered_map>

#define PIXL_REGISTER_COMPONENT(Component) ComponentStore::registerComponent<Component>(#Component)

namespace pixl::ecs {
	
	class ComponentBase;
	//this class enables an Entity to be modified by a script

	/**@class ComponentStore
	 * @brief fully static Container that keeps a record on components by name to
	 *  make sure they can be created by scripts or other fun stuff
	 */
	class ComponentStore
	{
	public:

		/**@brief the inplace constructor type as a constructor cannot be bound easily (also allows for make_component stuff)*/
		using cctor_t = sa::delegate<ComponentBase*()>;

		/**@brief ComponentInfo
		 * keeps track of the 3 key ingredients to an component
		 * - How to make it (cctor_t)
		 * - How to call it (string)
		 * - How to call it if it did something wrong (typeid)
		 */
		struct ComponentInfo
		{
			ComponentInfo();
			ComponentInfo(const cctor_t& ctor,std::string_view n,std::type_index idx);

			cctor_t ctor_inplaceof;
			std::string_view name;
			std::type_index index;
		};

		/** @brief retrives a chunk of info from the stored ones */
		PIXL_NODISCARD static ComponentInfo getComponentInfo(std::string_view name);

		/** @brief adds a chunk of information to the store
		 *  @param ctor [in] a constructor replacement function (can be a lambda that wraps the default ctor)
		 *  @param name [in] the unique name the component goes by
		 *  @param idx  [in] the typeid() of the component
		 */
		static void registerComponent(const cctor_t& ctor, std::string_view name, std::type_index idx);

		/** @brief adds a chunk of information to the store
		 *  @tparam Component the component you want to add, the typeid and the ctor are automatically deducted from this
		 *  @param name [in] the name of the component
		 *  @param constructor [in] a constructor function; 
		 *		this will be deduced automatically, but will need to be replaced if Component does not satisfy Default-Constructable
		 */
		template <class Component,typename X = typename std::enable_if_t<std::is_base_of_v<ComponentBase,Component>,Component> >
		static void registerComponent(std::string_view name,cctor_t constructor = cctor_t::create([]{ return new X;}) )
		{
			return registerComponent(constructor,name,typeid(X));
		}

		static bool affiliate(std::type_index idx, std::string_view view)
		{
			const auto i1 = m_staticStore.find(std::string(view));

			return i1 != m_staticStore.end() && i1->second.index == idx;
		}
	private:
		static std::unordered_map<std::string,ComponentInfo> m_staticStore;
	};


}
