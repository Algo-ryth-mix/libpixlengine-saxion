#pragma once
#include <type_traits>
#include <memory>
#include <typeindex>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <mutex>

#include "threading/dispatcher.hpp"
#include "threading/default_dispatcher.hpp"

#include "ecs/component_base.hpp"
#include "ecs/component_container.hpp"
#include "core/serializable_base.hpp"
#include "math/mat.hpp"

namespace pixl::ecs {
	

	/**@class Entity 
	 * @brief represents an all purpose Game Object that can have child-objects and individual components
	 */
	class Entity : public virtual core::serializable_base
	{

	public:
		explicit Entity(std::string name) : m_name{std::move(name)}{};
		Entity(const Entity&);
		Entity(Entity&&) noexcept;
		Entity& operator=(const Entity&);
		Entity& operator=(Entity&&) noexcept;

		/** @brief gets the Delta-Time of the Simulation
		 * @return the delta in Seconds
		 */
		virtual float getSimulationDelta() const noexcept
		{
			return m_simulationDelta;
		}
		
		/** @brief gets the Delta-Time of the Graphics
		 * @return the delta in Seconds
		 */
		virtual float getGraphicsDelta() const noexcept
		{
			return m_graphicsDelta;
		}


		virtual ~Entity();


		std::string getName() const override;

		/**@name invalid pointers
		 * @brief use these to test if you got an invalid component or child
		 * @{
		 */
		inline static std::shared_ptr<ComponentBase> null_component {nullptr};
		inline static std::shared_ptr<Entity> null_entity {nullptr};
		/** @} */

		/**@brief indicates if the passed type is a valid Component, if it is not the type will be undefined
		 * @tparam Component the type you want to check
		 */
		template <typename Component>
		using check_component_t = typename std::enable_if<std::is_base_of<ComponentBase,Component>::value,Component>::type;


		/**@brief gets an attached component as a weak_ptr and casts to the right type automatically
		 * @tparam Component the component you want to probe for
		 * @return a weak_ptr to the Component you requested, if the component could not be found this returns null_component
		 */
		template<typename Component>
		ComponentContainer<check_component_t<Component>> getComponent();

		/**@brief gets an attached component by name
		 * @param name [in] the name of the component you want to look for
		 * @returns a weak_ptr to ComponentBase you need to cast to the type yourself in this configuration
		 */
		ComponentContainer<ComponentBase> getComponent(std::string_view name);

		/**@brief attaches a new component to the Entity by type
		 * @tparam Component the component you want to add to the Entity
		 * @return the component you just added to the Entity, if this operation was unsuccessful this will be null_component
		 */
		template <typename Component>
		ComponentContainer<check_component_t<Component>> addComponent();

		/**@brief attaches a new component to the Entity by name, note this only works if your component was registered to the ComponentStore
		 * @param name [in] the name of the component you want to add to the Entity
		 * @return the component you just added to the Entity as a generic ComponentBase, if this operation was unsuccessful this will be null_component
		 */
		ComponentContainer<ComponentBase> addComponent(std::string_view name);

		/**@brief allows you to manually insert an component 
		 * @param idx [in] the typeid() of the component
		 * @param name [in] the name of the component
		 * @param c [in] a std::shared_ptr to a componentBase (this should be your component downcast'd)
		 * @return true if the component could be inserted and false if it could not
		 */
		bool addComponentRaw(std::type_index idx, std::string_view name, const std::shared_ptr<ComponentBase>& c);

		/**@brief gets all children Entities as a vector 
		 */
		std::vector<std::shared_ptr<Entity>> getChildren() const;

		/**@brief gets a specific Child by name 
		 * @param name [in] the name of the child
		 * @return the child as weak_ptr
		 */
		std::weak_ptr<Entity> getChildByName(std::string_view name);

		
		/**@brief checks if the entity has a specific component attached 
		 * @{
		 * @param idx [in] by typeid() index
		 */
		bool hasComponent(std::type_index idx);
		/**@param name [in] by name of the component*/
		bool hasComponent(std::string_view name);
		/**@}*/

		

		/**@brief checks if the entity has a parent Entity
		 */
		bool hasParent() const;

		/**@brief detaches from the parent Entity*/
		void detach();
		
		/**@brief attaches to an parent Entity */
		void attach(Entity* parent) const;

		std::shared_ptr<Entity> addChild(std::shared_ptr<Entity>);
		std::shared_ptr<Entity> addChild(const Entity& Prefab);
		/**@brief adds a child Entity to this Entity*/
		std::shared_ptr<Entity> addChild(std::string_view name);

		/**@brief removes a child Entity to this Entity*/
		bool removeChild(Entity* child);

		/**@brief out of place constructor, calls initialize on all Children and onInit on all Components*/
		void initialize();

		/**@bief update mechanism, calls update on all Children and onUpdate on all Components */
		void update(float dt);

		/**@bief draw mechanism, calls draw on all Children and onDraw on all Components */
		void draw(float dt);


		/**@brief sets the dispatchers for multi-threaded operations in the entity
		 * @tparam DispatcherUpdate the dispatcher to use for update and logic operations
		 * @tparam DispatcherDraw the dispatcher to use for draw and graphics operations
		 */
		template <class DispatcherUpdate,class DispatcherDraw>
		std::enable_if_t<std::is_base_of_v<threading::Dispatcher,DispatcherDraw> && std::is_base_of_v<threading::Dispatcher,DispatcherUpdate>>
		setDispatchers()
		{
			m_drawDispatcher = std::make_unique<DispatcherDraw>();
			m_updateDispatcher = std::make_unique<DispatcherUpdate>();
		}

		/**@brief get the Entire Transformation Stack, including all parent Transformations
		 * @return the entire stack as a FloatMatrix4x4
		 */
		math::mat4f getTransformationStack();

		/**@brief the Transformation specific to this Entity
		 */
		math::mat4f TransformationMatrix = math::mat4f::identity();

	protected:
		virtual void onUpdateEntity(float dt)	PIXL_NOVIRTIMPLEMENT;
		virtual void onDrawEntity(float dt)		PIXL_NOVIRTIMPLEMENT;
		virtual void onInitEntity()				PIXL_NOVIRTIMPLEMENT;


		core::serialize_output serialize() override;
		void deserialize(const core::serialize_input&) override;

	private:

		//this will cascade on all child entities below this and this object itself
		template <class Callable>
		void cascade(Callable&& f)
		{
			std::invoke(f,this);
			for(auto & child : m_children)
			{
				child->cascade(f);
			}
		}


		float m_simulationDelta = 0.0f;
		float m_graphicsDelta = 0.0f;
		std::unique_ptr<threading::Dispatcher> m_drawDispatcher = std::make_unique<threading::DefaultDispatcher>();
		std::unique_ptr<threading::Dispatcher> m_updateDispatcher = std::make_unique<threading::DefaultDispatcher>();

		mutable std::mutex m_componentsStoreMutex;

		std::unordered_map<std::string_view, std::shared_ptr<ComponentBase>> m_componentStoreName;
		std::unordered_map<std::type_index,  std::shared_ptr<ComponentBase>> m_componentStoreID;
		std::unordered_multimap<std::uint8_t,std::shared_ptr<ComponentBase>> m_componentStorePriority;


		std::vector<std::shared_ptr<Entity>> m_children;
		Entity* m_parent = nullptr;

		std::string m_name;
		void updateSinglePriority(std::uint8_t priority, std::unique_ptr<threading::Dispatcher>& dispatcher);
		void drawSinglePriority(std::uint8_t priority, std::unique_ptr<threading::Dispatcher>& dispatcher);


	};

	template <typename Component>
	ComponentContainer<typename Entity::check_component_t<Component>>
	Entity::getComponent()
	{
		//check if the component is in the Entity
		if (!hasComponent(typeid(Component))) return {std::reinterpret_pointer_cast<Component>(null_component)};

		return {std::reinterpret_pointer_cast<Component>(m_componentStoreID[typeid(Component)])};
	}

	template <typename Component>
	ComponentContainer<typename Entity::check_component_t<Component>>
	Entity::addComponent()
	{
		//construct an instance of the component
		const auto component = std::shared_ptr<ComponentBase>(new Component);

		//try to add it to the Entity
		if (!addComponentRaw(typeid(Component), component->getName(), component)) return {std::reinterpret_pointer_cast<Component>(null_component)};

		//return it to allow write through use
		return {std::reinterpret_pointer_cast<Component>(component)};
	}


}
