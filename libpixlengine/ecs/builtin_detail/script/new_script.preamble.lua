

-- this file contains lua-functions that are backed diretctly into every script
-- you create via ScriptComponent
-- this is executed before your script is executed, all functions are therfore available to your Script
-- note that this uses the engine table exclusively
-- it will also seed your rng for your

-- (C) Raphael Baier 2019 under the MIT-Licenses

-- Latest Changes:
-- Seed RNG
-- CC2s_c
-- engine.script

math.randomseed(engine.rng_seed())

engine["utils"] = {
  uppercase_dict = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
  lowercase_dict = "abcdefghijklmnopqrstuvwxyz"
}

--convert CamelCase to snake_case
function engine.utils.CC2s_c(str)
  assert(type(str) == "string")
  ret = ""
  for i = 1, #str do
    s = str:sub(i,i)
    if(engine.utils.uppercase_dict:find(s) ~= nil) then
      if(i ~= 1 ) then ret = ret .. "_" end
      ret = ret .. s:lower()
    else
      ret = ret .. s
    end
  end
  return ret
end

--Base class for most scripts
engine["script"] = {}

function engine.script:new(o)
  o = o or {}
  setmetatable(o,self)
  self.__index = self
  return o
end

function engine.script.deltaTime(self)
	self = self or {}
	return parent:getDelta()
end


function engine.script.get_components(...)
  local arg  = {...}
  x = {}
  for i,name in ipairs(arg) do
  	assert(type(name) == "string", "Argument was not of right type!")
    if(parent:hasComponent(name)) then
			x[i] = _G[name].from(parent:getComponent(name))
		else
			x[i] = _G[name].from(parent:addComponent(name))
		end
    assert(x[i] ~= nil, "could not get Component: " .. name)
  end
  return unpack(x)
end

function engine.script:require_components(...)
	local arg = {...}
	for _,name in ipairs(arg) do
		assert(type(name) == "string", "Argument was not of right type!")

		snake_case = utils.CC2s_c(name)

		if(parent:hasComponent(name)) then
			self[snake_case] = _G[name].from(parent:getComponent(name))
		else
			self[snake_case] = _G[name].from(parent:addComponent(name))
		end

		assert(self[snake_case] ~= nil, "could not get Component: " .. name)
	end
end

if vec2 ~= nil or vec3 ~= nil or vec4 ~= nil then
--convert vector key (x,y,z,w) to index (1,2,3,4)
  function engine.utils.vkey2idx(key)
    if key == "x" then
      return 1
    elseif key == "y" then
      return 2
    elseif key == "z" then
      return 3
    elseif key == "w" then
      return 4
    end
  end

--convert index(1,2,3,4) to vector-key (x,y,z,w)
  function engine.utils.idx2vkey(idx)
    if idx == 1 then
      return "x"
    elseif idx == 2 then
      return "y"
    elseif idx == 3 then
      return "z"
    elseif idx == 4 then
      return "w"
    end
  end
end