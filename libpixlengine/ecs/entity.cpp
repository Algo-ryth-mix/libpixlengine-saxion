#include "ecs/entity.hpp"
#include "ecs/component_storage.hpp"

#include <cassert>
#include <iostream>

namespace pixl::ecs {

	bool Entity::addComponentRaw(std::type_index idx,std::string_view name, const std::shared_ptr<ComponentBase>& c)
	{
		assert(c && c != null_component);

		//make sure only one thread can add components at a time
		std::unique_lock<std::mutex> lock(m_componentsStoreMutex);

		//check if the component was already added to the Entity
		if(hasComponent(idx))  return false;
		if(hasComponent(name)) return false;

		//insert it in both maps
		m_componentStoreID.insert({idx,c});
		m_componentStoreName.insert({name,c});
											//expanded macro as macros cannot be trusted
		m_componentStorePriority.insert({(((c->priority) > (COMPONENT_MAX_PRIORITY)) ? (c->priority) : (COMPONENT_MAX_PRIORITY)),c});

		//set the parent of the Component
		c->m_parent = this;

		return true;
	}


	std::string Entity::getName() const
	{
		return m_name;
	}

	core::serialize_output Entity::serialize()
	{
		core::serialize_output out;
		out.serializer["type"] = translate_fromSerialization(SERIALIZE_TYPE::TYPE_ENTITY);
		for(auto& child : m_children)
		{
			out.serializeChild(child.get());
		}
		for(auto& [name,component] : m_componentStoreName)
		{
			out.serializeChild(component.get());
		}
		return out;
	}

	void Entity::deserialize(const core::serialize_input& in)
	{
		if(in.serializer.find("name") != in.serializer.end()) m_name = in.serializer["name"];

		for (auto && item : in.serializer.items())
		{
			if (!item.value().is_object()) continue;
			switch (translate_toSerialization(item.value()["type"].get<std::string>()))
			{
				case SERIALIZE_TYPE::TYPE_COMPONENT:
				{
					if (m_componentStoreName.find(item.key()) == m_componentStoreName.end())
					{
						addComponent(item.key());
					}
				}
				case SERIALIZE_TYPE::TYPE_ENTITY:
				{
					if (std::find_if(m_children.begin(), m_children.end(), [&](const auto& child)
					{
						return child->getName() == item.key();
					}) == m_children.end())
					{
						addChild(std::string_view(item.key()));
					}
				}
				default:break;
			}
		}

		for (auto& child : m_children)
		{
			in.deserializeChild(child.get());
		}
		for (auto&[name, component] : m_componentStoreName)
		{
			in.deserializeChild(component.get());
		}


	}

	ComponentContainer<ComponentBase> Entity::getComponent(std::string_view name)
	{
		//check if the component is available
		if (!hasComponent(name)) return {null_component};

		return {m_componentStoreName.at(name)};
	}

	std::vector<std::shared_ptr<Entity>> Entity::getChildren() const
	{
		return m_children;
	}

	std::weak_ptr<Entity> Entity::getChildByName(std::string_view name)
	{
		//find the child by name
		const auto iterator = std::find_if(m_children.begin(), m_children.end(), [&](const std::shared_ptr<Entity>& ptr)
		{
			return ptr->m_name == name;
		});

		//check if the children was found
		if (iterator == m_children.end()) return null_entity;

		return *iterator;
	}

	ComponentContainer<ComponentBase> Entity::addComponent(std::string_view name)
	{

		//harass the ComponentStore for some infos
		const auto info = ComponentStore::getComponentInfo(name);

		//check if the info was ok
		if(info.index == typeid(null_component)) return {null_component};

		//construct it with the inplace constructor
		const auto c = std::shared_ptr<ComponentBase>(info.ctor_inplaceof());

		//add it with all the info we got
		if(!addComponentRaw(info.index,name,c)) return {null_component};

		return {c};
	}

	bool Entity::hasComponent(std::type_index idx)
	{
		//check if that index exists
		return m_componentStoreID.find(idx) != m_componentStoreID.end();
	}

	bool Entity::hasComponent(std::string_view name)
	{
		//check if that name exists
		return m_componentStoreName.find(name) != m_componentStoreName.end();
	}

	bool Entity::hasParent() const
	{
		return m_parent != nullptr;
	}

	void Entity::detach()
	{
		assert(m_parent);
		m_parent->removeChild(this);
	}

	void Entity::attach(Entity* parent) const
	{
		parent->addChild(*this);
	}

	std::shared_ptr<Entity> Entity::addChild(const Entity& Prefab)
	{
		//accept it as a child and add it to the list of other children
		m_children.emplace_back(std::make_shared<Entity>(Prefab));

		//add ourselves as the parent
		m_children.back()->m_parent = this;

		//expose it to the world
		return m_children.back();
	}
	std::shared_ptr<Entity> Entity::addChild(std::shared_ptr<Entity> child)
	{
		//accept it as a child and add it to the list of other children
		m_children.push_back(child);

		//add ourselves as the parent
		m_children.back()->m_parent = this;

		//expose it to the world
		return m_children.back();
	}


	//the next few lines read like a sad child-protection story
	std::shared_ptr<Entity> Entity::addChild(std::string_view name)
	{
		//accept it as a child and add it to the list of other children
		m_children.emplace_back(std::make_unique<Entity>(std::string(name)));

		//add ourselves as the parent
		m_children.back()->m_parent = this;

		//expose it to the world
		return m_children.back();
	}

	bool Entity::removeChild(Entity* child)
	{
		assert(child);

		//check if we have that child
		const auto index = std::find_if(m_children.begin(),m_children.end(),[&](auto& c)
		{
			return c.get() == child;
		});

		//we don't
		if(index == m_children.end()) return false;

		//remove the parent of the child
		child->m_parent = nullptr;

		//move the child to the very back
		*index = std::move(m_children.back());

		//and detach said back
		m_children.pop_back();

		return true;
	}

	//aight sad story over

	void Entity::initialize()
	{
		//init all children
		for(const auto& child : m_children)
			child->initialize();

		//init all components
		for(const auto& [idx,component] : m_componentStoreID) {
			component->onInit();
		}

		onInitEntity();
	}

	enum TAG : threading::Dispatcher::tag_t
	{
		UPDATE,
		DRAW
	};

	void Entity::update(float dt)
	{
		cascade([&](Entity* e)
		{
			e->m_simulationDelta = dt;
		});


		//update all children
		for(uint8_t i = 0; i <= COMPONENT_MAX_PRIORITY ;++i) {
			updateSinglePriority(i,m_updateDispatcher);
			m_updateDispatcher->await_all(UPDATE);

		}
	
		cascade([&](Entity* e)
		{
			e->onUpdateEntity(dt);
		});
	}

	

	void Entity::updateSinglePriority(std::uint8_t priority, std::unique_ptr<threading::Dispatcher>& dispatcher)
	{
		//get the range of this priority
		const auto& [start,end] = m_componentStorePriority.equal_range(priority);

		//update all elements of this priority
		std::for_each(start,end,[&](const auto& c)
		{
			dispatcher->dispatch(sa::delegate<void()>::create<ComponentBase,&ComponentBase::callUpdate>(c.second.get()),UPDATE);
		});

		//update all children
		for(const auto& child : m_children)
			child->updateSinglePriority(priority,dispatcher);


	}


	void Entity::draw(float dt)
	{
		cascade([&](Entity*e)
		{
			e->m_graphicsDelta = dt;
		});
		
		//draw all children
		for(uint8_t i = 0; i <= COMPONENT_MAX_PRIORITY ;++i) {
			drawSinglePriority(i,m_drawDispatcher);
			m_drawDispatcher->await_all(DRAW);
		}
		cascade([&](Entity*e)
		{
				e->onDrawEntity(dt);
		});
		
	}

	math::mat4f Entity::getTransformationStack()
	{
		math::mat4f result = math::mat4f::identity();
		for(Entity* e = this;e != nullptr; e = e->m_parent)
		{
			result = result * e->TransformationMatrix;
		}
		return result;
	}

	void Entity::drawSinglePriority(std::uint8_t priority, std::unique_ptr<threading::Dispatcher>& dispatcher)
	{

		//get the range of this priority
		const auto& [start,end] = m_componentStorePriority.equal_range(priority);

		//draw all elements of this range
		std::for_each(start,end,[&](const auto &c)
		{
			dispatcher->dispatch(sa::delegate<void()>::create<ComponentBase,&ComponentBase::callDraw>(c.second.get()),DRAW);
		});
		
		//draw all children
		for(const auto& child : m_children)
			child->drawSinglePriority(priority,dispatcher);
	}

	
	Entity::Entity(const Entity& e)
	{
		m_children = e.m_children;
		m_parent = e.m_parent;
		m_componentStoreID = e.m_componentStoreID;
		m_componentStoreName = e.m_componentStoreName;
		m_componentStorePriority = e.m_componentStorePriority;
		m_name = e.m_name;
	}
	
	Entity::Entity(Entity&& e) noexcept
	{
		m_children = std::move(e.m_children);
		m_parent = e.m_parent;
		m_componentStoreID = std::move(e.m_componentStoreID);
		m_componentStoreName = std::move(e.m_componentStoreName);
		m_componentStorePriority = std::move(e.m_componentStorePriority);
		m_name = std::move(e.m_name);
	}
	
	Entity& Entity::operator=(const Entity& e)
	{
		m_children = e.m_children;
		m_parent = e.m_parent;
		m_componentStoreID = e.m_componentStoreID;
		m_componentStoreName = e.m_componentStoreName;
		m_componentStorePriority = e.m_componentStorePriority;
		m_name = e.m_name;

		return *this;
	}
	
	Entity& Entity::operator=(Entity&& e) noexcept
	{
		m_children = std::move(e.m_children);
		m_parent = e.m_parent;
		m_componentStoreID = std::move(e.m_componentStoreID);
		m_componentStoreName = std::move(e.m_componentStoreName);
		m_componentStorePriority = std::move(e.m_componentStorePriority);
		m_name = std::move(e.m_name);

		return *this;
	}

	Entity::~Entity()
	{

		if(m_parent) detach();
		//destroy all components
		for(const auto& [idx,component] : m_componentStoreID) {
			if(component.get()) component->onDestroy();
		}
		//the children have their own destructor
	}


}
