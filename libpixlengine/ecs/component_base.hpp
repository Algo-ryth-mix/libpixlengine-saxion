#pragma once
#include <string>
#include <utility>
#include "common.hpp"
#include <mutex>
#include "core/serializable_base.hpp"
#include <cstdint>

#define COMPONENT_MAX_PRIORITY 32

namespace pixl::ecs {
	class Entity;

	template <class T>
	class ComponentContainer;

	class ComponentBase : public virtual core::serializable_base
	{
	public:
		mutable std::mutex mtx;


		//make sure your custom Component implements this and writes the name to the Component
		ComponentBase(std::string name, std::uint8_t priority = 16) : name{ std::move(name) }, m_parent{ nullptr }, priority{priority}{};


		ComponentBase(const ComponentBase& other)
		{
			m_parent = other.m_parent;
			name = other.name;
			priority = other.priority;
		};
		ComponentBase(ComponentBase&& other) noexcept
		{
			m_parent = other.m_parent;
			name = std::move(other.name);
			priority = other.priority;
		}
		ComponentBase& operator=(const ComponentBase& other) {
			m_parent = other.m_parent;
			name = other.name;
			priority = other.priority;
			return *this;
		};
		ComponentBase& operator=(ComponentBase&& other) noexcept {
			m_parent = other.m_parent;
			name = std::move(other.name);
			priority = other.priority;
			return *this;
		}
		virtual ~ComponentBase() = default;


		std::string getName() const override { return name;}

		/**@brief these function control the behaviour of your Component and should be implemented
		 * @{
		 */
		virtual void onInit()		PIXL_NOVIRTIMPLEMENT;
		virtual void onUpdate()		PIXL_NOVIRTIMPLEMENT;
		virtual void onDraw()		PIXL_NOVIRTIMPLEMENT;
		virtual void onDestroy()	PIXL_NOVIRTIMPLEMENT;

		uint8_t getPriority() const { return priority; }

	protected:
		virtual core::serialize_output serialize() override;
		virtual void deserialize(const core::serialize_input&) override;


		/** @} */
		std::string name = str::empty;

		/**@brief this gives your component a way to talk to the parent object */
		Entity* getParent() const { return m_parent;}
	private:
		friend class Entity;

		template<class T> friend class ComponentContainer;
		Entity* m_parent;
		std::uint8_t priority;		
		mutable std::mutex update_mtx;
		mutable std::mutex draw_mtx;

		void callUpdate()
		{
			std::unique_lock<std::mutex> lock(update_mtx);
			onUpdate();
		}


		void callDraw()
		{
			std::unique_lock<std::mutex> lock(draw_mtx);
			onDraw();
		}

	};



	//this is used in place to represent whether something is an Entity a Component or something Custom
	//usually only the value TYPE_CUSTOM should be used when implementing this yourself
	//in the registry it will have the identifier '*' to indicated that it should be ignored
	//by the rest of the serialization interface
	enum class SERIALIZE_TYPE : std::uint8_t
	{
		TYPE_COMPONENT,
		TYPE_ENTITY,
		TYPE_CUSTOM
	};

	//convert type to string representation
	inline constexpr const char* const translate_fromSerialization(const SERIALIZE_TYPE T)
	{
		return (T == SERIALIZE_TYPE::TYPE_COMPONENT) ? "C" : T == SERIALIZE_TYPE::TYPE_ENTITY ? "E" : "*";
	}

	//convert string-representation to type
	inline SERIALIZE_TYPE translate_toSerialization(const std::string& str)
	{
		if (str.length() == 0) return SERIALIZE_TYPE::TYPE_CUSTOM;
		switch (str[0])
		{
		case 'C': return SERIALIZE_TYPE::TYPE_COMPONENT;
		case 'E': return SERIALIZE_TYPE::TYPE_ENTITY;
		default: return SERIALIZE_TYPE::TYPE_CUSTOM;
		}
	}

	inline core::serialize_output ComponentBase::serialize()
	{
		core::serialize_output out;
		out.serializer["type"] = translate_fromSerialization(SERIALIZE_TYPE::TYPE_COMPONENT);
		return out;
	}
	inline void ComponentBase::deserialize(const core::serialize_input&)
	{
	}


}
