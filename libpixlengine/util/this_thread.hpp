#pragma once
#include "common.hpp"
#include <functional>
#include <thread>
#include "core/fast_delegate.hpp"
#include <atomic>

namespace pixl::util{
struct this_thread {

	//lock a thread until a certain function returns true (calling the function until the result yields true)
	static void wait_until(sa::delegate<bool()> fn)
	{
		using namespace std::this_thread;
		using namespace std::chrono_literals;
		do
		{
			if(fn()) return;
			sleep_for(1ms);
		}while(true);
	}
};
}
