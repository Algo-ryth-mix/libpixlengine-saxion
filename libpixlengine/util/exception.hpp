﻿#pragma once

#include "common.hpp"
#include <stdexcept>

namespace pixl::error {
class exception
{
public:
	explicit exception(const char* _Location)
		: m_location(_Location)
	{
	}

	explicit exception(const std::string& _Location)
		: m_location(_Location)
	{
	}
	
	std::string where() const
	{
		return m_location;
	}

private:
	std::string m_location;
};

class runtime_exception : public std::runtime_error, public exception {
public:
	explicit runtime_exception(const std::string& _Message,const std::string& _Location)
		: runtime_error(_Message),::pixl::error::exception(_Location)
	{
	}

	explicit runtime_exception(const char* _Message,const char* _Location)
		: runtime_error(_Message), ::pixl::error::exception(_Location)
	{
	}
};
class logic_exception : public std::logic_error , public exception {
public:
	explicit logic_exception(const std::string& _Message, const std::string& _Location)
		: std::logic_error(_Message.c_str()), ::pixl::error::exception(_Location)
	{

	}
	explicit logic_exception(const char* _Message, const char* _Location)
		: std::logic_error(_Message), ::pixl::error::exception(_Location)
	{
	}
};
}