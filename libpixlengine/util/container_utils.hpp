#pragma once
#include <iterator>
#include <vector>


namespace pixl::util
{
	/**@brief accept any container that has a begin and an end and transform it such that 
	 * it returns a different container type with a different type, provided that the function can transform it
	 * @tparam T the type of elements in the container
	 * @tparam Container the type of the container
	 * @tparam Iter the iterator-type of the arbitrary container (deduced automatically in most cases)
	 * @tparam Alloc allocation information for the Container (also deduced automatically in most cases)
	 * @param [in] a begin of the input range
	 * @param [in] b end of the input range
	 * @param [in] func the transformation function
	 */
	template<class T, template <class,class...> class Container, class Iter,  class... Alloc>
	Container<T,Alloc...> inline_transform(Iter a, const Iter& b, T(*func)(const typename std::iterator_traits<Iter>::value_type&&))
	{
		Container<T,Alloc...> res;
		for (; a != b; ++a)
		{
			res.emplace_back(func(*a));
		}
		return res;
	}

	struct membuf : std::streambuf
	{
		membuf(byte * base,size_t size)
		{
			char * p = reinterpret_cast<char*>(base);
			this->setg(p, p, p + size);
		}
	};

	struct imemstream : virtual membuf,std::istream
	{
		imemstream(byte * base,size_t size) :
			membuf(base,size),
			std::istream(this)
		{}
	};

	inline imemstream byte_vector2istream(byte_vec & vec)
	{
		return imemstream(vec.data(), vec.size());
	}

	
}
