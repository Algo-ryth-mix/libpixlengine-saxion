#pragma once
#include "math/vector.hpp"
#include "math/mat.hpp"
#include "perspective.hpp"
#include "ecs/component_storage.hpp"



namespace pixl::graphics
{

	namespace detail
	{
		//create a look at Matrix from a camera position a camera target and a world up
		inline math::mat4f lookAt(const math::vec3f& position, const math::vec3f& target, math::vec3f up)
		{
			const math::vec3f cameraDir = (position - target).to_unit();
			const math::vec3f cameraRight = crossp(up, cameraDir).to_unit();
			const math::vec3f cameraUp = crossp(cameraDir, cameraRight).to_unit();
			return math::mat4f
			(
				cameraRight.x, cameraRight.y, cameraRight.z, 0.0f,
				cameraUp.x, cameraUp.y, cameraUp.z, 0.0f,
				cameraDir.x, cameraDir.y, cameraDir.z, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			) * translate(math::mat4f::identity(), -position);
		}
	}

	namespace defaults
	{
		const float YAW = -90.0f;
		const float PITCH = 0.0f;
		const float SPEED = 2.5f;
		const float SENSITIVITY = 0.1f;
		const float FOV = 45.0f;
	}


	class Camera : public ecs::ComponentBase
	{
	public:
		
		enum Camera_Movement
		{
			FWD,
			BWD,
			LFT,
			RGT
		};

		Camera(math::vec3f pos = {0.0f,0.0f,0.0f},math::vec3f up = {0.0f,1.0f,0.0f},float yaw  = defaults::YAW,float pitch = defaults::PITCH) :
		ComponentBase("Camera", 1),
		m_position(pos),
		m_worldUp(up),
		m_yaw(yaw),
		m_pitch(pitch)
		{
			updateCameraVectors();
		}

		/**@brief represents a camera move-event that will adjust the Matrix accordingly (aka WASD)
		 * @param [in] direction the Direction you want to move the camera in options are FWD,BWD,LFT,RGT
		 * @param [in] deltaTime the Delta Time of the current frame (should be graphics Delta)
		 * @param [in] keepGrounded if true the y position of the camera is locked to 0
		 */
		void move_event(Camera_Movement direction,float deltaTime = 1.0f,bool keepGrounded = false)
		{
			float velocity = defaults::SPEED * deltaTime;
			if (direction == FWD)
				m_position += m_front * velocity;
			if (direction == BWD)
				m_position -= m_front * velocity;
			if (direction == LFT)
				m_position -= m_right * velocity;
			if (direction == RGT)
				m_position += m_right * velocity;

			if (keepGrounded) m_position.y = 0;

		}

		/**@brief represents a camera-rotate-event that will adjust the Matrix accordingly (aka MouseMove)
		 * @params [in] offs the offset of the mouse (or similar pointing device) from the previous frame
		 * @params [in] constrain_pitch whether or not to constrain the pitch from going over or under 90 degrees
		 */
		void rotate_event(math::vec2f offs,bool constraint_pitch = true)
		{
			offs.x *= defaults::SENSITIVITY;
			offs.y *= defaults::SENSITIVITY;

			m_yaw -= offs.x;
			m_pitch += offs.y;

			if(constraint_pitch)
			{
				m_pitch = std::clamp(m_pitch, -89.9f, 89.9f);
			}

			updateCameraVectors();

		}

		/**@brief updates the camera vectors if the viewport or something similar changed
		 */
		void updateCameraVectors()
		{
			math::vec3f	front;
			front.x = std::cos(m_yaw * math::deg_to_rad<float>()) * std::cos(m_pitch * math::deg_to_rad<float>());
			front.y = std::sin(m_pitch * math::deg_to_rad<float>());
			front.z = std::sin(m_yaw * math::deg_to_rad<float>()) * std::cos(m_pitch * math::deg_to_rad<float>());

			m_front = front.to_unit();

			m_right = crossp(m_front, m_worldUp).to_unit();
			m_up = crossp(m_right, m_front).to_unit();
		
		}

		/**@brief returns the view-matrix multiplied by the inverse-transformation stack of the parent object
		 * use this if you want your camera matrix to be used by something else
		 */
		math::mat4f getViewMatrix() const
		{	
			return detail::lookAt(m_position, m_position + m_front, m_worldUp)  * getParent()->getTransformationStack().inv();
		}

		/**@brief submits the View-Matrix as the Transformation-Matrix of the parent object
		 * use this if you want the Camera to be the Entity itself
		 */
		void submitViewMatrix() const
		{
			getParent()->TransformationMatrix = detail::lookAt(m_position, m_position + m_front, m_up).inv();
		}


	private:
		math::vec3f m_position = { 0.0f,0.0f,0.0f };
		math::vec3f m_worldUp = { 0.0f,1.0f,0.0f };
		math::vec3f m_up = { 0.0f,1.0f,0.0f };
		math::vec3f m_front = { 0.0f,0.0f,1.0f };
		math::vec3f m_right = { 1.0f,0.0f,0.0f };
		float m_yaw;
		float m_pitch;



		
	};
}
namespace pixl::ecs
{
	/**@brief register the camera component in the ComponentStore as "Camera"
	 */
	inline void registerCameraComponent()
	{
		ComponentStore::registerComponent<graphics::Camera>("Camera");
	}
}
