#pragma once
#include "graphics/shader.hpp"
#include "graphics/window_glextension.hpp"
#include "math/mat.hpp"

namespace pixl::gl
{

	class Shader : public graphics::Shader<GLuint,GLuint>
	{
	public:
		GLuint getID() const { return m_program; }

		#pragma region uniforms

		void setInt(int i,const char * where) const
		{
			glUniform1i(glGetUniformLocation(m_program, where), i);
		}

		void setVec2i (math::vec2i i,const char * where) const
		{
			glUniform2iv(glGetUniformLocation(m_program, where), 1, i.data);
		}

		void setVec3i (math::vec3i i,const char * where) const
		{
			glUniform3iv(glGetUniformLocation(m_program, where), 1, i.data);
		}

		void setVec4i(math::vec4i i, const char * where) const
		{
			glUniform4iv(glGetUniformLocation(m_program, where), 1, i.data);
		}

		void setFloat(float i, const char * where) const
		{
			glUniform1f(glGetUniformLocation(m_program, where), i);
		}

		void setVec2f(math::vec2f i, const char * where) const
		{
			glUniform2fv(glGetUniformLocation(m_program, where), 1, i.data);
		}

		void setVec3f(math::vec3f i, const char * where) const
		{
			glUniform3fv(glGetUniformLocation(m_program, where), 1, i.data);
		}

		void setVec4f(math::vec4f i, const char * where) const
		{
			glUniform4fv(glGetUniformLocation(m_program, where), 1, i.data);
		}

		void setMat2x2(math::mat2x2<float> m,const char * where) const
		{
			glUniformMatrix2fv(glGetUniformLocation(m_program, where), 1, GL_TRUE, m.data);
		}
		void setMat3x3(math::mat3x3<float> m, const char * where) const
		{
			glUniformMatrix3fv(glGetUniformLocation(m_program, where), 1, GL_TRUE, m.data);
		}
		void setMat4x4(math::mat4x4<float> m, const char * where) const
		{
			glUniformMatrix4fv(glGetUniformLocation(m_program, where), 1, GL_TRUE, m.data);
		}

		#pragma endregion 

	protected:
		shader_mod_type compileShader(shader_mod_type&) override;
		shader_mod_type loadSource(shader_mod_type, core::resource) override;
		shader_mod_type createShader(graphics::shader_t) override;


		void activateShader(shader_program_type) override;
		void deactivateShader(shader_program_type) override;
		shader_program_type loadCache(core::resource, core::resource) override;
		std::tuple<core::resource, core::resource> extractProgram(shader_program_type) override;
		shader_program_type linkShader() override;
	};
	extern std::atomic<GLuint> activeShader;
}
