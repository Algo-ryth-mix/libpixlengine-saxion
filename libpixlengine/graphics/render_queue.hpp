#pragma once
#include "renderable.hpp"
#include <optional>
#include <map>

namespace pixl::graphics{

	enum RenderQueueSortMode
	{
		TYPE_THEN_INSERT_ORDER,
		TYPE_THEN_MAX_FIRST,
		TYPE_THEN_MIN_FIRST
	};


	class RenderQueue
	{
	public:
		void push_front(const Renderable&r);
		
		void select_type(std::type_index idx);
		const Renderable* pop_back();

		std::vector<std::reference_wrapper<const Renderable>>& get_selected()
		{
			return m_sortedRange;
		}


		RenderQueue& merge(RenderQueue& other);

		void sort(RenderQueueSortMode sort_mode);
		void clear();

	private:
		std::multimap<std::type_index, std::reference_wrapper<const Renderable>> m_renderMap;
		decltype(m_renderMap)::const_iterator m_start, m_end;
		std::vector<std::reference_wrapper<const Renderable>> m_sortedRange;
	};

	inline void RenderQueue::push_front(const Renderable& r)
	{
		m_renderMap.emplace(r.selfType(), std::cref(r));
	}

	inline void RenderQueue::select_type(std::type_index idx)
	{
		auto[start, end] = m_renderMap.equal_range(idx);
		m_start = start;
		m_end = end;
		sort(TYPE_THEN_INSERT_ORDER);
	}

	inline const Renderable* RenderQueue::pop_back()
	{
		if (m_start == m_end) return nullptr;
		auto& obj = (--m_end)->second.get();
		m_renderMap.erase(m_end);
		return &obj;
	}

	inline RenderQueue& RenderQueue::merge(RenderQueue& other)
	{
		m_renderMap.merge(other.m_renderMap);
		return *this;
	}

	inline void RenderQueue::sort(RenderQueueSortMode sort_mode)
	{
		sa::delegate<bool(int, int)> sorter;

		if (sort_mode == TYPE_THEN_INSERT_ORDER)	sorter = decltype(sorter)::create([](auto a, auto b) { return true; });
		else if (sort_mode == TYPE_THEN_MAX_FIRST)	sorter = decltype(sorter)::create([](auto a, auto b) { return a > b; });
		else if (sort_mode == TYPE_THEN_MIN_FIRST)	sorter = decltype(sorter)::create([](auto a, auto b) { return a < b; });


		for(decltype(m_start)::difference_type i = 0; i < std::distance(m_start,m_end); ++i)
		{
			auto* lhs = &std::next(m_start, i)->second.get();
			for(decltype(m_start)::difference_type j = i; j < std::distance(m_start,m_end); ++j)
			{
				auto& rhs = std::next(m_start, j)->second.get();

				const bool test = sorter(lhs->value(), rhs.value());

				if(!test)
				{
					lhs = &rhs;
				}
			}

			m_sortedRange.emplace_back(*lhs);
		}
	}

	inline void RenderQueue::clear()
	{
		m_renderMap.clear();
		m_sortedRange.clear();
	}
}
