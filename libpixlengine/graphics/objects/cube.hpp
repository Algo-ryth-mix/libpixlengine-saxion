#pragma once

#include "ecs/component_base.hpp"
#include "graphics/renderable.hpp"
#include "ecs/entity.hpp"

#include <array>
#include "graphics/gl_texture.hpp"

namespace pixl::objects
{


	/**@class Cube
	 * @brief represents a Basic cube, that can be Rendered using a suitable SubRenderer (e.g.: gl::CubeRenderer)
	 * can be modified with 
	 * 
	 */
	class Cube :  public ecs::ComponentBase , public graphics::Renderable
	{
	public:
		Cube() : ComponentBase("Cube"){}

		struct vertex
		{
			float pos[3];
			float uv[2];
		};

		inline static std::array<vertex,36> vertices = {
			vertex{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}},
			vertex{{ 0.5f, -0.5f, -0.5f}, {1.0f, 0.0f}},
			vertex{{ 0.5f,  0.5f, -0.5f}, {1.0f, 1.0f}},
			vertex{{ 0.5f,  0.5f, -0.5f}, {1.0f, 1.0f}},
			vertex{{-0.5f,  0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}},

			vertex{{-0.5f, -0.5f,  0.5f}, {0.0f, 0.0f}},
			vertex{{ 0.5f, -0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{ 0.5f,  0.5f,  0.5f}, {1.0f, 1.0f}},
			vertex{{ 0.5f,  0.5f,  0.5f}, {1.0f, 1.0f}},
			vertex{{-0.5f,  0.5f,  0.5f}, {0.0f, 1.0f}},
			vertex{{-0.5f, -0.5f,  0.5f}, {0.0f, 0.0f}},

			vertex{{-0.5f,  0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{-0.5f,  0.5f, -0.5f}, {1.0f, 1.0f}},
			vertex{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{-0.5f, -0.5f,  0.5f}, {0.0f, 0.0f}},
			vertex{{-0.5f,  0.5f,  0.5f}, {1.0f, 0.0f}},

			vertex{{ 0.5f,  0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{ 0.5f,  0.5f, -0.5f}, {1.0f, 1.0f}},
			vertex{{ 0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{ 0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{ 0.5f, -0.5f,  0.5f}, {0.0f, 0.0f}},
			vertex{{ 0.5f,  0.5f,  0.5f}, {1.0f, 0.0f}},

			vertex{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{ 0.5f, -0.5f, -0.5f}, {1.0f, 1.0f}},
			vertex{{ 0.5f, -0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{ 0.5f, -0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{-0.5f, -0.5f,  0.5f}, {0.0f, 0.0f}},
			vertex{{-0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}},

			vertex{{-0.5f,  0.5f, -0.5f}, {0.0f, 1.0f}},
			vertex{{ 0.5f,  0.5f, -0.5f}, {1.0f, 1.0f}},
			vertex{{ 0.5f,  0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{ 0.5f,  0.5f,  0.5f}, {1.0f, 0.0f}},
			vertex{{-0.5f,  0.5f,  0.5f}, {0.0f, 0.0f}},
			vertex{{-0.5f,  0.5f, -0.5f}, {0.0f, 1.0f}}
		};

		math::mat4f getTransform() const override
		{
			return m_localTransform * getParent()->getTransformationStack();
		}

		std::type_index selfType() const override
		{
			return typeid (Cube);
		}

		/**@brief set the texture of the cube
		 * @param [in] tex a pointer to the texture you want to set
		 * @note make sure that the texture is of the required type that your Renderer Expects
		 * i.e.: using a gl::CubeRenderer use a gl::Texture
		 *  
		 */
		void texture(graphics::Texture* tex) { m_texture = tex; }


		/**@brief sets the local transform of the cube
		 * this can offset the cube from the actual Entity
		 * @param [in] transform the offset to the transformation-stack of the Entity
		 */
		void localTransform(const math::mat4f& transform) { m_localTransform = transform; }


		graphics::Texture* getTexture() const { return m_texture; }
		
	private:
		graphics::Texture* m_texture = nullptr;
		math::mat4f m_localTransform = math::mat4f::identity();
	};
}
