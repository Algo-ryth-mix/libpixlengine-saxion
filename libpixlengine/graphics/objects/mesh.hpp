#pragma once
#include <TINYOBJ/tiny_obj_loader.hpp>
#include "ecs/component_base.hpp"
#include "graphics/renderable.hpp"
#include "graphics/gl_texture.hpp"
#include "ecs/entity.hpp"
#include "util/container_utils.hpp"
#include "util/exception.hpp"

//TODO(algo-ryth-mix): WIP

namespace pixl::objects
{
	class Mesh : public ecs::ComponentBase,public graphics::Renderable
	{
	public:

		math::mat4f getTransform() const override{ return m_localTransform * getParent()->getTransformationStack();}
		std::type_index selfType() const override { return typeid(Mesh); }
		
	private:

		struct DrawData
		{
			void* ext_data;
			int numTriangles;
			size_t material_id;
		};
		
		void* data;
		
		math::mat4f m_localTransform = math::mat4f::identity();
	};
}
