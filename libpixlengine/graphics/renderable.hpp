#pragma once
#include "math/mat.hpp"
#include <typeindex>


namespace pixl::graphics
{
	class Renderable
	{
	public:
		virtual math::mat4f getTransform() const = 0;
		virtual std::type_index selfType() const = 0;

		virtual int value() const { return -1; }


	};
}
