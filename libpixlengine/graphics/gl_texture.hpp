#pragma once
#include "graphics/texture.hpp"
#include "graphics/window_glextension.hpp"

namespace pixl::gl
{
	
	class Texture :public graphics::Texture
	{
		static inline gl::Texture const* activeTexture = nullptr;


	public:
		Texture(nullptr_t n) : graphics::Texture(n){}
		Texture(const core::resource &resource,bool generate = false) : graphics::Texture(resource)
		{
			if (generate) gen();
			type = graphics::GL;
		}


		void gen() 
		{
			if (!m_buffer) glGenTextures(1, &m_buffer);
		}

		void createTexture(int texture_unit = 0)
		{
			glActiveTexture(GL_TEXTURE0 + texture_unit);
			bind();
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.data());


			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		void bind(int texture_unit = 0) const
		{
			glActiveTexture(GL_TEXTURE0 + texture_unit);
			if (activeTexture == this) return;
			glBindTexture(GL_TEXTURE_2D, m_buffer);
			activeTexture = this;
		}

		void release() const
		{
			if (activeTexture != this) return;
			glBindTexture(GL_TEXTURE_2D, 0);
			activeTexture = nullptr;
		}

		private:
			GLuint m_buffer{};
	};
}
