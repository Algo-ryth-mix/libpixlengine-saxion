#pragma once
#include "graphics/window_glextension.hpp"
#include <type_traits>

namespace pixl::gl
{
	class VertexArray
	{
		static inline VertexArray* activeArray = nullptr;

	public:

		VertexArray(nullptr_t) {}
		VertexArray()
		{
			glGenVertexArrays(1, &m_buffer);
		}


		VertexArray(const VertexArray& other) = default;
		VertexArray(VertexArray&& other) noexcept = default;
		VertexArray& operator=(const VertexArray& other) = default;
		VertexArray& operator=(VertexArray&& other) noexcept = default;

		void bind()
		{
			if (activeArray == this) return;
			glBindVertexArray(m_buffer);
			activeArray = this;
		}

		void setLayout(GLuint location,GLenum data_type,GLuint data_len,GLenum vertex_size,size_t data_offs_in_vertex)
		{
			bind();

			if (data_type == GL_FALSE)
			{
				glDisableVertexAttribArray(location);
				return;
			}

			glVertexAttribPointer(location, data_len, data_type, GL_FALSE, vertex_size, reinterpret_cast<void*>(data_offs_in_vertex));
			glEnableVertexAttribArray(location);

		}


		#define CASE_(A,B) else if(idx == typeid(A)) type = B;
		template<typename T,typename M>
		size_t setLayoutSingle(size_t where,M T::* m,size_t offset = 0)
		{


			GLenum type = GL_FALSE;
			size_t data_size = 1;

			std::type_index idx = typeid(std::remove_all_extents_t<M>);

			if (idx == typeid(GLfloat))
			{
				type = GL_FLOAT;
			}
			CASE_(GLuint,GL_UNSIGNED_INT)
			CASE_(GLubyte,GL_UNSIGNED_BYTE)
			CASE_(GLbyte,GL_BYTE)
			CASE_(GLint,GL_INT)
			CASE_(GLboolean,GL_BOOL)
			CASE_(GLdouble,GL_DOUBLE)
			CASE_(GLshort,GL_SHORT)
			CASE_(GLushort,GL_UNSIGNED_SHORT)

			setLayout(static_cast<GLuint>(where), type,  sizeof(M) / sizeof(m), sizeof(T),offset);
			return sizeof(M);
		}
		#undef CASE_



		template <typename C,typename ...X>
		size_t setLayout(X C::*... members)
		{
			size_t index = 0;
			size_t offs = 0;
			std::tuple member_list = { members... };


			auto f = [&](auto member)
			{
				offs += setLayoutSingle(index++, member, offs);
				return offs;
			};
			size_t dummy[] = { size_t(0), f(members)... };
			return dummy[sizeof...(members)];
		}


		void release()
		{
			if (activeArray != this) return;
			glBindVertexArray(0);
			activeArray = nullptr;
		}

		void gen()
		{
			if (!m_buffer) glGenVertexArrays(1, &m_buffer);
		}


	private:
		GLuint m_buffer{};
	};
}