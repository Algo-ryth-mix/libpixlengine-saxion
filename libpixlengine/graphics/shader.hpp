#pragma once
#include "common.hpp"
#include "core/resource.hpp"
#include <mutex>

namespace pixl::graphics {

	enum shader_t
	{
		SHADER_TYPE_VERTEX,
		SHADER_TYPE_FRAGMENT,
		SHADER_TYPE_GEOMETRY,
		SHADER_TYPE_TESSELLATION_E,
		SHADER_TYPE_TESSELLATION_C,
		SHADER_TYPE_COMPUTE
	};


	template <class ShaderModuleType,class ShaderProgramType>
	class Shader
	{
	public:
		Shader() = default;
		virtual ~Shader() = default;

		void addShader(shader_t type, core::resource source);

		void prepare();


		void loadCachedProgram(core::resource bin,core::resource format);

		std::tuple<core::resource, core::resource> getCachedProgram();


		void bind();

		void release();

	private:
		mutable std::mutex m_shader_mutex;

	protected:
		typedef ShaderModuleType shader_mod_type;
		typedef ShaderProgramType shader_program_type;

		virtual shader_mod_type		createShader(shader_t) PIXL_PUREVIRTUAL;
		virtual shader_mod_type		loadSource(shader_mod_type, core::resource) PIXL_PUREVIRTUAL;
		virtual shader_mod_type		compileShader(shader_mod_type& module) PIXL_PUREVIRTUAL;
		virtual shader_program_type linkShader() PIXL_PUREVIRTUAL;

		virtual shader_program_type	loadCache (core::resource program_cache, core::resource binary_format_file)
		{
			throw std::logic_error("this shader does not support this operation! (load from cache) ");
		}
		virtual std::tuple<core::resource, core::resource> extractProgram(shader_program_type)
		{
			throw std::logic_error("this shader does not support this operation! (extract program) ");
		}


		virtual void activateShader(shader_program_type) PIXL_PUREVIRTUAL;
		virtual void deactivateShader(shader_program_type) PIXL_PUREVIRTUAL;


		std::vector<shader_mod_type> m_modules;
		shader_program_type m_program;

	};
	template <class ShaderModuleType, class ShaderProgramType>
	void Shader<ShaderModuleType, ShaderProgramType>::addShader(shader_t type, core::resource source)
	{
		std::unique_lock<std::mutex> lock(m_shader_mutex);

		auto shader = createShader(type);
		shader = loadSource(shader, source);
		shader = compileShader(shader);
		if (!shader) return;
		m_modules.push_back(shader);
	}

	template <class ShaderModuleType, class ShaderProgramType>
	void Shader<ShaderModuleType, ShaderProgramType>::prepare()
	{
		std::unique_lock<std::mutex> lock(m_shader_mutex);
		m_program = linkShader();
	}

	template <class ShaderModuleType, class ShaderProgramType>
	void Shader<ShaderModuleType, ShaderProgramType>::loadCachedProgram(core::resource bin, core::resource format)
	{
		std::unique_lock<std::mutex> lock(m_shader_mutex);
		m_program = loadCache(bin, format);
	}

	template <class ShaderModuleType, class ShaderProgramType>
	std::tuple<core::resource, core::resource> Shader<ShaderModuleType, ShaderProgramType>::getCachedProgram()
	{
		return extractProgram(m_program);
	}

	template <class ShaderModuleType, class ShaderProgramType>
	void Shader<ShaderModuleType, ShaderProgramType>::bind()
	{
		activateShader(m_program);
	}

	template <class ShaderModuleType, class ShaderProgramType>
	void Shader<ShaderModuleType, ShaderProgramType>::release()
	{
		deactivateShader(m_program);
	}
}
