#pragma once
#include "core/resource.hpp"
#include <STB/stb_image.h>
#include "math/vector.hpp"

namespace pixl::graphics
{
	enum TextureType
	{
		GL,
		VK,
		DX,
		INVALID
	};

	
	class Texture
	{
	public:
		Texture(nullptr_t) : size(0,0),m_channels(){}
		Texture(const core::resource& resource,bool gl_mode = false);
		virtual ~Texture() = default;
		void load(const core::resource& resource, bool gl_mode = false);

		PIXL_NODISCARD const byte_vec& getImageData() const { return data; }
		PIXL_NODISCARD math::vec2i getSize() const { return size; }
		PIXL_NODISCARD int getChannels() const { return m_channels; }

		TextureType getTextureType() const { return type; }
		
	protected:

		TextureType type = INVALID;
		math::vec2i size;
		int m_channels;
		byte_vec data;
	};
}
