#pragma once
#include "core/subscriber.hpp"
#include <GLAD/glad.h>
#include <core/window.hpp>
#include <mutex>
#include "threading/scheduler.hpp"



namespace pixl::gl {

class GLWindowExt : public pixl::core::subscriber_base
{
public:

	void onNotify(core::subject_base* subject_base, core::subject_base::event_t event) override
	{
		if (m_window == nullptr) m_window = (core::Window*)subject_base;

		switch(core::Window::event(event))
		{
		case core::Window::PGLFW_API_INITIALIZED:
		{
			glfwWindowHint(GLFW_SAMPLES, 4);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
			//glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

			#if defined(PIXL_DEBUG_GL)
				glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
			#endif


			#if defined(__APPLE__)
				glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
			#endif
			break;
		}
		case core::Window::PGLFW_WINDOW_SIZE_CHANGED:
		{
			const auto size = m_window->getWindowSize();
			threading::Scheduler::scheduleDrawThread(threading::Scheduler::runnable::create<GLWindowExt,&GLWindowExt::update_viewport>(this));
			break;
		}
		default: ;
		}
	}

	void update_viewport() const
	{
		const auto size = m_window->getWindowSize();
		glViewport(0, 0, static_cast<GLsizei>(size.x), static_cast<GLsizei>(size.y));
	}

	void load_gl() const
	{
		auto* handle = m_window->getHandle();
		glfwMakeContextCurrent(handle);
		gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress));
		glEnable(GL_MULTISAMPLE);

	}

	void cursor_visibility(bool visibility) const
	{
		auto* handle = m_window->getHandle();
		glfwSetInputMode(handle, GLFW_CURSOR, visibility ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
	}

	void vsync(bool enabled) const
	{
		auto * handle = m_window->getHandle();
		glfwSwapInterval(enabled ? 1 : 0);
	}

	void lock_context() const
	{
		threading::Scheduler::scheduleDrawThread(threading::Scheduler::runnable::create<GLWindowExt,&GLWindowExt::load_gl>(this));
	}

	void draw() const
	{
		glfwSwapBuffers(m_window->getHandle());
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	}

	void unlock_context() const
	{
	}

	~GLWindowExt()
	{
		unlock_context();
	}

	bool can_lock() const
	{
		return true;
	}


private:

	core::Window* m_window = nullptr;
};

}
