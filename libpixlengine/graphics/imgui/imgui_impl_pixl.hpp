#pragma once
#include "imgui_impl_glfw.h"
#include "core/window.hpp"
#include "imgui_impl_opengl3.h"

namespace pixl::gl
{
	class pImgui
	{
		public:
		static void init(core::Window* window)
		{
			IMGUI_CHECKVERSION();
			ImGui::CreateContext();

			ImGui::StyleColorsDark();


			ImGui_ImplGlfw_InitForOpenGL(window->getHandle(), false);

			window->callback<core::MOUSEBUTTON>([](core::KeyButtonMode press_mode,core::MouseButton mb,auto mods)
			{
				ImGui_ImplGlfw_MouseButtonCallback(nullptr, static_cast<int>(press_mode),static_cast<int>(mb),mods);
			});
			window->callback<core::MOUSESCROLL>([](math::vec2d v)
			{
				ImGui_ImplGlfw_ScrollCallback(nullptr, v.x, v.y);
			});
			window->callback<core::KEY>([](int keyCode,int scanCode,core::KeyButtonMode press_mode,auto mods)
			{
				ImGui_ImplGlfw_KeyCallback(nullptr, keyCode, scanCode, static_cast<int>(press_mode), mods);
			});
			window->callback<core::CHAR>([](uint32_t code )
			{
				ImGui_ImplGlfw_CharCallback(nullptr, code);
			});

			ImGui_ImplOpenGL3_Init("#version 330 core");
		}

		static void new_frame()
		{
			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
		}

		static void shutdown()
		{
			ImGui_ImplGlfw_Shutdown();
			ImGui_ImplOpenGL3_Shutdown();
		}

	};
}
