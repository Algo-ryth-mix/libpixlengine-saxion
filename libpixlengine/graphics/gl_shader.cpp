#include "graphics/gl_shader.hpp"
#include <iostream>
#include "core/debugging.hpp"

namespace pixl::gl
{
	std::atomic<GLuint> activeShader = NULL;

	

	graphics::Shader<GLuint,GLuint>::shader_mod_type Shader::createShader(graphics::shader_t type)
	{
		switch(type)
		{
		case graphics::SHADER_TYPE_VERTEX:			return glCreateShader(GL_VERTEX_SHADER);
		case graphics::SHADER_TYPE_FRAGMENT:		return glCreateShader(GL_FRAGMENT_SHADER);
		case graphics::SHADER_TYPE_GEOMETRY:		return glCreateShader(GL_GEOMETRY_SHADER);
		case graphics::SHADER_TYPE_TESSELLATION_E:	return glCreateShader(GL_TESS_EVALUATION_SHADER);
		case graphics::SHADER_TYPE_TESSELLATION_C:	return glCreateShader(GL_TESS_CONTROL_SHADER);
		case graphics::SHADER_TYPE_COMPUTE:			return glCreateShader(GL_COMPUTE_SHADER);
		default: return NULL;
		}
	}

	graphics::Shader<GLuint,GLuint>::shader_mod_type Shader::loadSource(shader_mod_type shader, core::resource source)
	{
		const char * src = source.c_str();
		glShaderSource(shader, 1, &src, NULL);
		return shader;

	}

	graphics::Shader<GLuint,GLuint>::shader_mod_type Shader::compileShader(shader_mod_type& module)
	{
		glCompileShader(module);
		GLint success;
		glGetShaderiv(module, GL_COMPILE_STATUS, &success);

		if (!success) {
			GLint len;
			glGetShaderiv(module, GL_INFO_LOG_LENGTH, &len);

			char* error_log = new char[len];
			glGetShaderInfoLog(module, len, &len, error_log);
			core::println_error("shader: compile_error: {}", error_log);
			delete[] error_log;
			return NULL;

		}
		return module;
	}

	graphics::Shader<GLuint,GLuint>::shader_program_type Shader::linkShader()
	{
		const GLuint program = glCreateProgram();
		for(GLuint module : m_modules)
		{
			glAttachShader(program, module);
		}

		glLinkProgram(program);

		for (GLuint module : m_modules)
		{
			glDeleteShader(module);
		}

		return program;
	}

	union binary_format_t
	{
		struct
		{
			byte a, b, c, d;
		};
		struct
		{
			byte bytes[4];
		};
		GLenum format;
	};


	graphics::Shader<unsigned, unsigned>::shader_program_type Shader::loadCache(core::resource program_cache, core::resource binary_format_file)
	{
		const GLuint shader = glCreateProgram();

		auto bv = binary_format_file.data();

		if (bv.size() < 4 * sizeof(byte)) throw std::runtime_error("binary format is invalid");

		binary_format_t binary_format{};
		binary_format.a = bv[0];
		binary_format.b = bv[1];
		binary_format.c = bv[2];
		binary_format.d = bv[3];
		
		glProgramBinary(shader, binary_format.format, program_cache.data().data(), static_cast<GLsizei>(program_cache.data().size()));
		return shader;
	}

	std::tuple<core::resource, core::resource> Shader::extractProgram(shader_program_type prog)
	{
		GLint len;
		glGetProgramiv(prog, GL_PROGRAM_BINARY_LENGTH, &len);

		binary_format_t format{};

		byte_vec bv(len);

		glGetProgramBinary(prog, len, &len, &format.format, bv.data());

		return {core::resource(bv), core::resource(byte_vec(format.bytes, format.bytes + sizeof(GLenum)))};

	}

	void Shader::activateShader(shader_program_type program)
	{
		if (activeShader == program) return;
		else glUseProgram(program);
		activeShader = program;
	}

	void Shader::deactivateShader(shader_program_type program)
	{
		if (activeShader != program) return;
		else glUseProgram(NULL);
		activeShader = program;
	}
}
