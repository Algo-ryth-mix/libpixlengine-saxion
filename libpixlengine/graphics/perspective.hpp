#pragma once
#include "math/mat.hpp"


namespace pixl::graphics
{
	

	inline math::mat4f scale(math::mat4f in, math::vec3f scale_factor)
	{
		math::mat4f scale_mat(
			scale_factor.x, 0.0f, 0.0f, 0.0f,
			0.0f, scale_factor.y, 0.0f, 0.0f,
			0.0f, 0.0f, scale_factor.z, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		);

		return in * scale_mat;
	}

	inline math::mat4f scale(math::mat4f in, float scale_factor)
	{
		const math::mat4f scale_mat(
			scale_factor, 0.0f, 0.0f, 0.0f,
			0.0f, scale_factor, 0.0f, 0.0f,
			0.0f, 0.0f, scale_factor, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		);

		return in * scale_mat;
	}

	inline math::mat4f translate(math::mat4f in, math::vec3f translation)
	{
		const math::mat4f translation_mat(
			1.0f, 0.0f, 0.0f, translation.x,
			0.0f, 1.0f, 0.0f, translation.y,
			0.0f, 0.0f, 1.0f, translation.z,
			0.0f, 0.0f, 0.0f, 1.0f
		);

		return in * translation_mat;
	}

	inline math::mat4f rotateX(math::mat4f in,math::anglef theta)
	{
		const math::mat4f rotation_mat(
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, theta.cos(), -theta.sin(), 0.0f,
			0.0f, theta.sin(), theta.cos(), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		);
		return in * rotation_mat;
	}


	inline math::mat4f rotateY(math::mat4f in, math::anglef theta)
	{
		const math::mat4f rotation_mat(
			theta.cos(), 0.0f, theta.sin(), 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			-theta.sin(), 0.0f, theta.cos(), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		);
		return in * rotation_mat;
	}

	inline math::mat4f rotateZ(math::mat4f in, math::anglef theta)
	{
		const math::mat4f rotation_mat(
			theta.cos(), -theta.sin(), 0.0f, 0.0f,
			theta.sin(), theta.cos(), 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		);
		return in * rotation_mat;
	}

	inline math::mat4f rotateAxis(math::mat4f in, math::anglef theta, math::vec3f axis)
	{
		axis = axis.to_unit();
		const math::mat4f rotation_mat(
			cos(theta) + axis.x * axis.x * (1.0f - cos(theta)), axis.x * axis.y * (1.0f - cos(theta)) - axis.z * sin(theta), axis.x * axis.z * (1.0f - cos(theta)) + axis.y * sin(theta), 0.0f,
			axis.y * axis.x * (1.0f - cos(theta)) + axis.z * sin(theta), cos(theta) + axis.y * axis.y * (1.0f - cos(theta)), axis.y * axis.z * (1.0f - cos(theta)) - axis.x * sin(theta), 0.0f,
			axis.z * axis.x * (1.0f - cos(theta)) - axis.y * sin(theta), axis.z * axis.y * (1.0f - cos(theta)) + axis.x * sin(theta), cos(theta) + axis.z * axis.z * (1.0f - cos(theta)), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		);
		return in * rotation_mat;
	}

	struct FrustumInfo
	{
		float b, t, r, l;
		float n;
		float f;
	};

	inline FrustumInfo makeFrustum(math::angle<float> FOV, float AspectRatio, float near_plane, float far_plane)
	{
		const float scale = FOV.tan<false>() * near_plane;

		FrustumInfo inf{};
		inf.r = AspectRatio * scale;
		inf.l = -inf.r;
		inf.t = scale;
		inf.b = -inf.t;
		inf.n = near_plane;
		inf.f = far_plane;

		return inf;

	}


	inline math::mat4f perspective(const FrustumInfo& inf)
	{
		math::mat4f projection = math::mat4f::identity();

		const float r_width = 1.0f / (inf.r - inf.l);
		const float r_height = 1.0f / (inf.t - inf.b);
		const float r_depth = 1.0f / (inf.f - inf.n);

		const float x = 2.0f * inf.n * r_width;
		const float y = 2.0f * inf.n * r_height;
		const float A = (inf.r + inf.l) * r_width;
		const float B = (inf.t + inf.b) * r_height;
		const float C = -(inf.f + inf.n) * r_depth;
		const float D = -(2.0f * inf.f * inf.n) * r_depth;


		return math::mat4f
		(
			x,		0.0f,	A,		0.0f,
			0.0f,	y,		B,		0.0f,
			0.0f,	0.0f,	C,		D,
			0.0f,	0.0f,  -1.0f,	0.0f
		);
	}

	inline math::mat4f perspective(FrustumInfo&& inf)
	{
		math::mat4f projection = math::mat4f::identity();

		const float r_width = 1.0f / (inf.r - inf.l);
		const float r_height = 1.0f / (inf.t - inf.b);
		const float r_depth = 1.0f / (inf.f - inf.n);

		const float x = 2.0f * inf.n * r_width;
		const float y = 2.0f * inf.n * r_height;
		const float A = (inf.r + inf.l) * r_width;
		const float B = (inf.t + inf.b) * r_height;
		const float C = -(inf.f + inf.n) * r_depth;
		const float D = -(2.0f * inf.f * inf.n) * r_depth;


		return math::mat4f
		(
			x, 0.0f, A, 0.0f,
			0.0f, y, B, 0.0f,
			0.0f, 0.0f, C, D,
			0.0f, 0.0f, -1.0f, 0.0f
		);
	}

	inline math::mat4f ortho(const FrustumInfo& inf)
	{
		return math::mat4f
		(
			2.0f / (inf.r - inf.l), 0.0f, 0.0f, 0.0f,
			0.0f, 2.0f / (inf.t - inf.b), 0.0f, 0.0f,
			0.0f, 0.0f, -2.0f / (inf.f - inf.n), 0.0f,
			(inf.r + inf.l) / (inf.r - inf.l),-(inf.t + inf.b) / (inf.t - inf.b),-(inf.f+ inf.n) / (inf.f - inf.n), 1.0f
		);
	}
	inline math::mat4f ortho(FrustumInfo&& inf)
	{
		return math::mat4f
		(
			2.0f / (inf.r - inf.l), 0.0f, 0.0f, 0.0f,
			0.0f, 2.0f / (inf.t - inf.b), 0.0f, 0.0f,
			0.0f, 0.0f, -2.0f / (inf.f - inf.n), 0.0f,
			(inf.r + inf.l) / (inf.r - inf.l), -(inf.t + inf.b) / (inf.t - inf.b), -(inf.f + inf.n) / (inf.f - inf.n), 1.0f
		);
	}
}
