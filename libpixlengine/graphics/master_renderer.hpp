#pragma once
#include "render_queue.hpp"
#include "renderable.hpp"
#include <typeindex>
#include <map>
#include "renderer.hpp"
#include "perspective.hpp"
#include "camera.hpp"

namespace pixl::graphics
{
	class MasterRenderer
	{
	public:
		template <class SubRenderer, class R = std::enable_if_t<std::is_base_of_v<Renderer, SubRenderer>, SubRenderer>>
		void installRenderer();

		void renderRange(RenderQueue & queue);

		void renderSingle(const Renderable & object);

		void render(math::vec2f screenSize,const Camera* cam);

	private:
		RenderQueue m_drawQueue;
		std::map<std::type_index, std::shared_ptr<Renderer>> m_rendererMap;
	};

	template <class SubRenderer,class R>
	void MasterRenderer::installRenderer()
	{
		std::shared_ptr<Renderer> renderer(new R);

		m_rendererMap.emplace(renderer->supported_type(), renderer);
	}

	inline void MasterRenderer::renderRange( RenderQueue& queue)
	{
		m_drawQueue.merge(queue);
	}

	inline void MasterRenderer::renderSingle(const Renderable& object)
	{
		m_drawQueue.push_front(object);
	}

	inline void MasterRenderer::render(math::vec2f screenSize,const Camera* cam)
	{

		const math::mat4f view = cam->getViewMatrix();
		const math::mat4f projection = perspective(makeFrustum(math::anglef::deg(45), screenSize.w / screenSize.h, 0.1f, 100.0f));

		for (auto&[type, renderer] : m_rendererMap)
		{
			m_drawQueue.select_type(type);

			renderer->prepare(view, projection);

			for (auto & obj : m_drawQueue.get_selected())
			{
				renderer->render(obj);
			}

			renderer->finish();

			

		}
		m_drawQueue.clear();
	}
}
