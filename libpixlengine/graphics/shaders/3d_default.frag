#version 330 core

/* Default Fragment-Shader,
 * color based on uv via vertex-data and texture via uniform sampler
 *
 L Raphael Baier (algo-ryth-mix) 2019 public-domain
 */

out vec4 FragColor;

uniform sampler2D tex;
in vec2 uv;

void main()
{
	FragColor = texture(tex,uv);
}