R"resource(#version 330 core

/* Default Vertex-Shader 
 * Position via Vertex-Data as vec3 on location 0
 * uv as passthrough via Vertex-Data as vec2 on location 1
 * uniform matrices:
 * - model matrix (mat4f) as "model"
 * - view matrix (mat4f) as "view"
 * - projection matrix (mat4f) as "projection"
 *
 L  Raphael Baier 2019 public-domain
 */


layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;

out vec2 uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	uv = aUV;
	gl_Position = projection * view * model * vec4(aPos,1.0f);

})resource"