#pragma once
#include "graphics/window_glextension.hpp"
#include "graphics_buffer.hpp"


namespace pixl::gl
{


	/**@brief represents a simple OpenGLBuffer Type
	 * @tparam TYPE this represents which buffer is going to be implemented
	 * @note GL_ARRAY_BUFFER,GL_ELEMENT_ARRAY_BUFFER & GL_UNIFORM_BUFFER are predefined
	 */
	template<GLenum TYPE>
	class PrimitiveGlBuffer : public graphics::GraphicsBuffer
	{

		//the actual buffer id we got from OpenGL
		GLuint m_buffer = 0;

		//the length of the data in the buffer
		size_t m_len{};

		//the currently active buffer (to make switching automatic)
		inline static GraphicsBuffer const* activeBuffer = nullptr;

	public:

		//empty constructor
		PrimitiveGlBuffer(nullptr_t){}

		//generates the buffer
		PrimitiveGlBuffer()
		{
			glGenBuffers(1, &m_buffer);
		}

		//copy and move operations
		#pragma region copy& move
		PrimitiveGlBuffer(const PrimitiveGlBuffer& other) = default;
		PrimitiveGlBuffer(PrimitiveGlBuffer&& other) noexcept = default;
		PrimitiveGlBuffer& operator=(const PrimitiveGlBuffer& other) = default;
		PrimitiveGlBuffer& operator=(PrimitiveGlBuffer&& other) noexcept = default;
		#pragma endregion


		/**@brief generates the OpenGL-Buffer if it does not already exist
		 */
		void gen()
		{
			if (!m_buffer) glGenBuffers(1, &m_buffer);
		}

		//implements loadData from graphics::GraphicsBuffer
		using GraphicsBuffer::loadData;
		
		void loadData(void* const data, size_t len) override
		{
			m_len = len;

			bind();
			glBufferData(TYPE, len, data, GL_STATIC_DRAW);
		}

		void orphanBuffer(size_t len) override
		{
			loadData(NULL, len);
		}

		void* map_data(size_t beg = 0, size_t end = 0) override
		{
			bind();
			if (end == 0) end = m_len;
			return glMapBufferRange(TYPE,beg,end-beg,GL_MAP_WRITE_BIT);
		}

		void finish_data() override
		{
			bind();
			glUnmapBuffer(TYPE);
		}

		void bind() override
		{
			if (activeBuffer == this) return;
			glBindBuffer(TYPE, m_buffer);
			activeBuffer = this;
		}

		void release() override
		{
			if(activeBuffer != this) return;
			glBindBuffer(TYPE, 0);
			activeBuffer = nullptr;
		}
		GLuint getID() const { return m_buffer; }

	};


	using VertexArrayBuffer = PrimitiveGlBuffer<GL_ARRAY_BUFFER>;
	using ElementArrayBuffer = PrimitiveGlBuffer<GL_ELEMENT_ARRAY_BUFFER>;
	using UniformBuffer = PrimitiveGlBuffer<GL_UNIFORM_BUFFER>;

}
