﻿#include "graphics/renderers/gl_cube_renderer.hpp"
#include "graphics/objects/cube.hpp"
#include "graphics/perspective.hpp"

bool was_depth_testing = false;


namespace pixl::gl
{
	//same for all cubes
	void CubeRenderer::prepare(math::mat4f view, math::mat4f projection)
	{
		if(!m_initialized)
		{

			//get shader
			const core::resource frag_resource(
				#include "graphics/shaders/3d_default.frag.hpp"
			);

			const core::resource vert_resource(
				#include "graphics/shaders/3d_default.vert.hpp" 
			);

			//prepare and compile shader
			
			m_shader.addShader(graphics::SHADER_TYPE_VERTEX, vert_resource);
			m_shader.addShader(graphics::SHADER_TYPE_FRAGMENT, frag_resource);

			m_shader.prepare();
			m_shader.bind();


			//bind the texture unit 0
			m_shader.setInt(0, "tex");


			//generate vertex-arrays
			m_VAO.gen();
			m_VBO.gen();

			m_VAO.bind();

			//load data
			m_VBO.loadData(objects::Cube::vertices);

			//set data layout
			m_VAO.setLayout(&objects::Cube::vertex::pos, &objects::Cube::vertex::uv);


			//clean up
			m_VBO.release();
			m_VAO.release();

			m_initialized = true;

		}

		//bind shader and prepare the matrices
		m_shader.bind();
		m_shader.setMat4x4(projection, "projection");
		m_shader.setMat4x4(view, "view"); m_VAO.bind();

		//check if the depth-test was enabled so we can return the state to how it was afterwards
		was_depth_testing = glIsEnabled(GL_DEPTH_TEST);

		glEnable(GL_DEPTH_TEST);
	}

	void CubeRenderer::render(const graphics::Renderable& object) const
	{
		//cast to a cube
		auto const* cube = dynamic_cast<const objects::Cube*>(&object);

		//check if casting to a cube was successful & if the provided texture is ok
		if (!cube) return;
		if (cube->getTexture()->getTextureType() != graphics::GL) return;

		//bind the texture unit
		reinterpret_cast<gl::Texture*>(cube->getTexture())->bind(0);

		//get the transform of this specific cube
		const math::mat4f model = object.getTransform();

		//set the model matrix
		m_shader.setMat4x4(model, "model");


		//render one cube
		glDrawArrays(GL_TRIANGLES, 0, 36);

	}

	void CubeRenderer::finish()
	{
		//return the depth-test to how it was previously
		if (!was_depth_testing) glDisable(GL_DEPTH_TEST);
	}


}
