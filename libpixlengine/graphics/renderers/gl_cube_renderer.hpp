#pragma once
#include "graphics/renderer.hpp"
#include "graphics/objects/cube.hpp"
#include <typeindex>
#include "graphics/gl_shader.hpp"
#include "graphics/gl_vertex_arrays.hpp"
#include "graphics/gl_buffers.hpp"
#include "graphics/gl_texture.hpp"


namespace pixl::gl
{
	class CubeRenderer : public graphics::Renderer
	{
	public:

		/**@brief advertise which type this renderer-supports (Cubes)*/
		std::type_index supported_type() const override
		{
			return typeid(objects::Cube);
		}

		//renderer prep
		void prepare(math::mat4f view, math::mat4f projection) override;

		//renderer work
		void render(const graphics::Renderable& object) const override;

		//renderer cleanup
		void finish() override;

	private:

		bool m_initialized = false;

		gl::Shader m_shader{};
		gl::VertexArray m_VAO = nullptr;
		gl::VertexArrayBuffer m_VBO = nullptr;

	};
}
