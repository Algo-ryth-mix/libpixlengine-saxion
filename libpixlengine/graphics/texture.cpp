
#include "graphics/texture.hpp"

namespace pixl::graphics
{
	Texture::Texture(const core::resource& resource,bool gl_mode)
	{
		load(resource, gl_mode);
	}

	void Texture::load(const core::resource& resource, bool gl_mode)
	{
		stbi_set_flip_vertically_on_load(gl_mode);

		byte* texture_data = stbi_load_from_memory(resource.data().data(), static_cast<int>(resource.data().size()), size.data, size.data + 1, &m_channels, STBI_rgb_alpha);

		m_channels += 1;
		data = byte_vec(texture_data, texture_data + size.x * size.y * m_channels );

	}
}
