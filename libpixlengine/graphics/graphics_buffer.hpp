#pragma once

namespace pixl::graphics
{
	class GraphicsBuffer
	{
	public:
		virtual ~GraphicsBuffer() = default;


		template<typename Container>
		void loadData(Container&& cot)
		{
			
			const size_t len = cot.size();
			const auto data = cot.data();
			const size_t elem_size = sizeof *data;

			loadData(static_cast<void* const>(data), len * elem_size);
		}

		template<typename Container>
		void loadData(const Container& cot)
		{
			
			const size_t len = cot.size();
			const auto data = cot.data();
			const size_t elem_size = sizeof *data;
			loadData(static_cast<void * const>(data), len * elem_size);
		}


		virtual void loadData(void* const data, size_t len) = 0;
		virtual void orphanBuffer(size_t len) = 0;

		virtual void bind() = 0;
		virtual void release() = 0;
		virtual void * map_data(size_t beg = 0, size_t end = 0) = 0;
		virtual void finish_data() = 0;

	};
}
