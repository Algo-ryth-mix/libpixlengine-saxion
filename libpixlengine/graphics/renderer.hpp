#pragma once
#include <typeindex>
#include "math/mat.hpp"


namespace pixl::graphics{
	class Renderable;

	class Renderer
	{
	public:
		virtual ~Renderer() = default;

		virtual std::type_index supported_type() const = 0;

		virtual void prepare(math::mat4f view, math::mat4f projection) = 0;
		virtual void render(const Renderable& object) const = 0;


		virtual void finish() = 0;
	};
}
