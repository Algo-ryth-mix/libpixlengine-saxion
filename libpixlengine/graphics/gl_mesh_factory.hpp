#pragma once
#include <TINYOBJ/tiny_obj_loader.hpp>
#include "math/vector.hpp"
#include <cassert>
#include "objects/mesh.hpp"
#include "math/vector.hpp"
#include "graphics/window_glextension.hpp"

namespace pixl::gl
{
	namespace detail
	{
		inline math::vec3f calc_surface_normal(const math::vec3f& p1, const math::vec3f& p2, const math::vec3f& p3)
		{
			const math::vec3f p21 = p2 - p1;
			const math::vec3f p31 = p3 - p1;

			const math::vec3f N = crossp(p31, p21);
			return N.to_unit();
		}

		inline bool has_smoothing_group(const tinyobj::shape_t& shape)
		{
			for (auto& smoothing_group_id : shape.mesh.smoothing_group_ids)
			{
				if (smoothing_group_id > 0) return true;
			}
			return false;
		}

		inline void compute_smoothing_normal(const tinyobj::attrib_t& attrib, const tinyobj::shape_t& shape, std::map<int, math::vec3f> smoothVertexNormals)
		{
			for (size_t f = 0; f < shape.mesh.indices.size() / 3; f++)
			{
				// Get the three indexes of the face (all faces are triangular)
				const tinyobj::index_t idx0 = shape.mesh.indices[3 * f + 0];
				const tinyobj::index_t idx1 = shape.mesh.indices[3 * f + 1];
				const tinyobj::index_t idx2 = shape.mesh.indices[3 * f + 2];

				// Get the three vertex indexes and coordinates
				int vi[3];      // indexes
				math::vec3f v[3];  // coordinates

				for (int k = 0; k < 3; k++) {
					vi[0] = idx0.vertex_index;
					vi[1] = idx1.vertex_index;
					vi[2] = idx2.vertex_index;
					assert(vi[0] >= 0);
					assert(vi[1] >= 0);
					assert(vi[2] >= 0);

					v[0][k] = attrib.vertices[3 * vi[0] + k];
					v[1][k] = attrib.vertices[3 * vi[1] + k];
					v[2][k] = attrib.vertices[3 * vi[2] + k];
				}

				// Compute the normal of the face
				const math::vec3f normal = calc_surface_normal(v[0], v[1], v[2]);

				// Add the normal to the three vertexes
				for (int i : vi)
				{
					auto iterator = smoothVertexNormals.find(i);
					if (iterator != smoothVertexNormals.end()) {
						iterator->second += normal;

					}
					else {
						smoothVertexNormals[i] = normal;

					}
				}
			}
			for (auto& normal : smoothVertexNormals)
			{
				normal.second = normal.second.to_unit();
			}
		}
	}
	class MeshFactory
	{
		static objects::Mesh* make_mesh(const std::string& filename,std::vector<tinyobj::material_t>& materials)
		{
			tinyobj::attrib_t attrib;
			std::vector<tinyobj::shape_t> shapes;

			std::string warn, err;
			bool ret = LoadObj(&attrib, &shapes, &materials, &warn, &err, filename.c_str());

			if (!warn.empty()) core::println_warning(warn.c_str());
			if (!err. empty()) core::println_error  (err. c_str());

			if(!ret)
			{
				core::println_error("Failed to load Mesh: {}", filename);
				return nullptr;
			}

			materials.push_back(tinyobj::material_t{});

			float bmin[3], bmax[3];
			
			bmin[0] = bmin[1] = bmin[2] = std::numeric_limits<float>::max();
			bmax[0] = bmax[1] = bmax[2] = -std::numeric_limits<float>::max();

			{
				for (size_t s = 0; s < shapes.size(); s++) {
					DrawObject o;
					std::vector<float> buffer;  // pos(3float), normal(3float), color(3float)

					// Check for smoothing group and compute smoothing normals
					std::map<int, math::vec3f> smoothVertexNormals;
					if (detail::has_smoothing_group(shapes[s]) > 0) {
						std::cout << "Compute smoothingNormal for shape [" << s << "]" << std::endl;
						detail::compute_smoothing_normal(attrib, shapes[s], smoothVertexNormals);
					}

					for (size_t f = 0; f < shapes[s].mesh.indices.size() / 3; f++) {
						tinyobj::index_t idx0 = shapes[s].mesh.indices[3 * f + 0];
						tinyobj::index_t idx1 = shapes[s].mesh.indices[3 * f + 1];
						tinyobj::index_t idx2 = shapes[s].mesh.indices[3 * f + 2];

						int current_material_id = shapes[s].mesh.material_ids[f];

						if ((current_material_id < 0) ||
							(current_material_id >= static_cast<int>(materials.size()))) {
							// Invaid material ID. Use default material.
							current_material_id =
								materials.size() -
								1;  // Default material is added to the last item in `materials`.
						}
						// if (current_material_id >= materials.size()) {
						//    std::cerr << "Invalid material index: " << current_material_id <<
						//    std::endl;
						//}
						//
						float diffuse[3];
						for (size_t i = 0; i < 3; i++) {
							diffuse[i] = materials[current_material_id].diffuse[i];
						}
						float tc[3][2];
						if (attrib.texcoords.size() > 0) {
							if ((idx0.texcoord_index < 0) || (idx1.texcoord_index < 0) ||
								(idx2.texcoord_index < 0)) {
								// face does not contain valid uv index.
								tc[0][0] = 0.0f;
								tc[0][1] = 0.0f;
								tc[1][0] = 0.0f;
								tc[1][1] = 0.0f;
								tc[2][0] = 0.0f;
								tc[2][1] = 0.0f;
							}
							else {
								assert(attrib.texcoords.size() >
									size_t(2 * idx0.texcoord_index + 1));
								assert(attrib.texcoords.size() >
									size_t(2 * idx1.texcoord_index + 1));
								assert(attrib.texcoords.size() >
									size_t(2 * idx2.texcoord_index + 1));

								// Flip Y coord.
								tc[0][0] = attrib.texcoords[2 * idx0.texcoord_index];
								tc[0][1] = 1.0f - attrib.texcoords[2 * idx0.texcoord_index + 1];
								tc[1][0] = attrib.texcoords[2 * idx1.texcoord_index];
								tc[1][1] = 1.0f - attrib.texcoords[2 * idx1.texcoord_index + 1];
								tc[2][0] = attrib.texcoords[2 * idx2.texcoord_index];
								tc[2][1] = 1.0f - attrib.texcoords[2 * idx2.texcoord_index + 1];
							}
						}
						else {
							tc[0][0] = 0.0f;
							tc[0][1] = 0.0f;
							tc[1][0] = 0.0f;
							tc[1][1] = 0.0f;
							tc[2][0] = 0.0f;
							tc[2][1] = 0.0f;
						}

						float v[3][3];
						for (int k = 0; k < 3; k++) {
							int f0 = idx0.vertex_index;
							int f1 = idx1.vertex_index;
							int f2 = idx2.vertex_index;
							assert(f0 >= 0);
							assert(f1 >= 0);
							assert(f2 >= 0);

							v[0][k] = attrib.vertices[3 * f0 + k];
							v[1][k] = attrib.vertices[3 * f1 + k];
							v[2][k] = attrib.vertices[3 * f2 + k];
							bmin[k] = std::min(v[0][k], bmin[k]);
							bmin[k] = std::min(v[1][k], bmin[k]);
							bmin[k] = std::min(v[2][k], bmin[k]);
							bmax[k] = std::max(v[0][k], bmax[k]);
							bmax[k] = std::max(v[1][k], bmax[k]);
							bmax[k] = std::max(v[2][k], bmax[k]);
						}

						float n[3][3];
						{
							bool invalid_normal_index = false;
							if (attrib.normals.size() > 0) {
								int nf0 = idx0.normal_index;
								int nf1 = idx1.normal_index;
								int nf2 = idx2.normal_index;

								if ((nf0 < 0) || (nf1 < 0) || (nf2 < 0)) {
									// normal index is missing from this face.
									invalid_normal_index = true;
								}
								else {
									for (int k = 0; k < 3; k++) {
										assert(size_t(3 * nf0 + k) < attrib.normals.size());
										assert(size_t(3 * nf1 + k) < attrib.normals.size());
										assert(size_t(3 * nf2 + k) < attrib.normals.size());
										n[0][k] = attrib.normals[3 * nf0 + k];
										n[1][k] = attrib.normals[3 * nf1 + k];
										n[2][k] = attrib.normals[3 * nf2 + k];
									}
								}
							}
							else {
								invalid_normal_index = true;
							}

							if (invalid_normal_index && !smoothVertexNormals.empty()) {
								// Use smoothing normals
								int f0 = idx0.vertex_index;
								int f1 = idx1.vertex_index;
								int f2 = idx2.vertex_index;

								if (f0 >= 0 && f1 >= 0 && f2 >= 0) {
									n[0][0] = smoothVertexNormals[f0].x;
									n[0][1] = smoothVertexNormals[f0].y;
									n[0][2] = smoothVertexNormals[f0].z;

									n[1][0] = smoothVertexNormals[f1].x;
									n[1][1] = smoothVertexNormals[f1].y;
									n[1][2] = smoothVertexNormals[f1].z;

									n[2][0] = smoothVertexNormals[f2].x;
									n[2][1] = smoothVertexNormals[f2].y;
									n[2][2] = smoothVertexNormals[f2].z;

									invalid_normal_index = false;
								}
							}

							if (invalid_normal_index) {
								// compute geometric normal
								auto* x = reinterpret_cast<float*>(n);
								x = detail::calc_surface_normal(v[0], v[1], v[2]).data;
								n[1][0] = n[0][0];
								n[1][1] = n[0][1];
								n[1][2] = n[0][2];
								n[2][0] = n[0][0];
								n[2][1] = n[0][1];
								n[2][2] = n[0][2];
							}
						}

						for (int k = 0; k < 3; k++) {
							buffer.push_back(v[k][0]);
							buffer.push_back(v[k][1]);
							buffer.push_back(v[k][2]);
							buffer.push_back(n[k][0]);
							buffer.push_back(n[k][1]);
							buffer.push_back(n[k][2]);
							// Combine normal and diffuse to get color.
							float normal_factor = 0.2;
							float diffuse_factor = 1 - normal_factor;
							float c[3] = { n[k][0] * normal_factor + diffuse[0] * diffuse_factor,
										  n[k][1] * normal_factor + diffuse[1] * diffuse_factor,
										  n[k][2] * normal_factor + diffuse[2] * diffuse_factor };
							float len2 = c[0] * c[0] + c[1] * c[1] + c[2] * c[2];
							if (len2 > 0.0f) {
								float len = sqrtf(len2);

								c[0] /= len;
								c[1] /= len;
								c[2] /= len;
							}
							buffer.push_back(c[0] * 0.5 + 0.5);
							buffer.push_back(c[1] * 0.5 + 0.5);
							buffer.push_back(c[2] * 0.5 + 0.5);

							buffer.push_back(tc[k][0]);
							buffer.push_back(tc[k][1]);
						}
					}

					o.vb_id = 0;
					o.numTriangles = 0;

					// OpenGL viewer does not support texturing with per-face material.
					if (shapes[s].mesh.material_ids.size() > 0 &&
						shapes[s].mesh.material_ids.size() > s) {
						o.material_id = shapes[s].mesh.material_ids[0];  // use the material ID
																		 // of the first face.
					}
					else {
						o.material_id = materials.size() - 1;  // = ID for default material.
					}
					printf("shape[%d] material_id %d\n", int(s), int(o.material_id));

					if (buffer.size() > 0) {
						glGenBuffers(1, &o.vb_id);
						glBindBuffer(GL_ARRAY_BUFFER, o.vb_id);
						glBufferData(GL_ARRAY_BUFFER, buffer.size() * sizeof(float),
							&buffer.at(0), GL_STATIC_DRAW);
						o.numTriangles = buffer.size() / (3 + 3 + 3 + 2) /
							3;  // 3:vtx, 3:normal, 3:col, 2:texcoord

						printf("shape[%d] # of triangles = %d\n", static_cast<int>(s),
							o.numTriangles);
					}

					drawObjects->push_back(o);
				}
			}

			printf("bmin = %f, %f, %f\n", bmin[0], bmin[1], bmin[2]);
			printf("bmax = %f, %f, %f\n", bmax[0], bmax[1], bmax[2]);

			return true;
			
		}
	};
}
