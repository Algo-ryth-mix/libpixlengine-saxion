import subprocess

git_path_glob = "git"

try:
    # this will fail if we try to run this on a non windows machine (which is fine) as those machines usually always have git in the path anyway (how did you clone this otherwise?)
    from winreg import *

    # Computer\HKEY_LOCAL_MACHINE\SOFTWARE\GitForWindows
    aReg = ConnectRegistry(None, HKEY_LOCAL_MACHINE)

    # this will fail it the user does not have git installed on his machine, in which case we also revert back to just trying to call "git" and hoping for the best
    aKey = OpenKey(aReg, r"SOFTWARE\GitForWindows")

    git_path_glob = QueryValueEx(aKey, "InstallPath")[0] + "\\bin\\git.exe"
except ImportError:
    print("bailed... no such import")
    pass
except EnvironmentError:
    print("bailed... no such key")
    pass


#git --no-pager show -s --format='%an <%ae>'
def get_git_commit_author(git_path):
	print(git_path)

	process = subprocess.Popen([git_path,"--no-pager","show","-s","--format='%an<%ae>'"],stdout=subprocess.PIPE)

	output = process.communicate()[0].decode("utf-8")
	
	return output


def get_git_rev(git_path):
    print(git_path)

    # call git and get the revision of this branch
    process = subprocess.Popen([git_path, "rev-parse", "HEAD"], stdout=subprocess.PIPE)

    # parse it
    output = process.communicate()[0].decode("utf-8")

    #return the first 8 numbers
    return output[:8]

# decorate to look like a c++ header
def decorate_file(ref,author):
    txt = "#pragma once\n"
    txt += "#define PIXL_GIT_VERSION_REF 0x" + ref
	txt += "#define PIXL_COMMIT_AUTHOR" + "\"" + author + "\""
    return txt


if __name__ == "__main__":
    print("read version begin!")
    #call git
    version = get_git_rev(git_path_glob)
	author = get_git_commit_author(git_path_glob)
    print("version: " + version)
    file = open("git_rev.h", 'w')
    #write the file
    file.write(decorate_file(version,author))
    file.close()
    print("read version done!")
