#pragma once
#include <cstddef>
#include <ostream>
#include <numeric>
#include <algorithm>
#include <functional>
#include <cmath>
#include <xmmintrin.h>
#include "math/trig.hpp"

namespace pixl::math
{
#ifndef P_CURLY_BRACKETS
	#define P_CURLY_BRACKETS(X) { X }
#endif

	#define P_EXTEND_VEC2(VEC) VEC.x,VEC.y
	#define P_BEXTEND_VEC2(VEC) P_CURLY_BRACKETS(P_EXTEND_VEC2(VEC))

	#define P_EXTEND_VEC3(VEC) VEC.x,VEC.y,VEC.z
	#define P_BEXTEND_VEC3(VEC) P_CURLY_BRACKETS(P_EXTEND_VEC3(VEC))

	#define P_EXTEND_VEC4(VEC) VEC.x,VEC.y,VEC.z,VEC.w
	#define P_BEXTEND_VEC4(VEC) P_CURLY_BRACKETS(P_EXTEND_VEC4(VEC))

	/**
	 *  @addtogroup Math
	 *  @{
	 */


	 /**@struct vec
	  * @brief a (mathematical) vector class with all the operators you would expect
	  * @tparam T the storage type of the vector
	  * @tparam S the dimension of the vector
	  */
	template <class T, std::size_t S>
	struct vec {};

	/**@struct vec_
	 * @brief the base class for @ref vec ... it should not be used directly
	 *  however you might find a lot of @ref vec 's functions defined here
	 * @tparam T the storage type of the vector
	 * @tparam S the dimension of the Vector
	 */
	template <class T,std::size_t S>
	struct vec_
	{
		typedef T value_type;
		typedef T type;
		typedef T& ref;
		typedef T* iterator;
		typedef const T* const_iterator;
		static std::size_t constexpr size_ = S;

		/** @brief the dimension of the vector in function (constexpr) form
		*/
		static std::size_t constexpr size() { return size_; }
		static std::size_t constexpr max_size() { return size_; }

		virtual ~vec_() = default;

		/**@brief the underlying data storage for this vector
		*/
		type data[size_];

		/**@name vec_ ptr group
		 * @brief a pointer to the raw storage
		 */
		/**@{*/
		/**@brief non-const ptr*/
		iterator ptr() { return data; }
		/**@brief const ptr*/
		const_iterator ptr() const { return data; }
		/**@}*/

		/**@name vec_ begin iterators group
		 * @brief begin operators for range based for loops and other fun stl-operations
		 */
		/**@{*/
		/**@brief non-const begin*/
		iterator begin() { return &data[0]; }
		/**@brief const begin*/
		const_iterator begin() const { return &data[0]; }
		/**@}*/
		/**@name vec_ end iterators group
		 *@brief end operators for range based for loops and other fun stl-operations
		 */
		/**@{*/
		/**@brief non-const end*/
		iterator end() { return &data[size_]; }
		/**@brief const end*/
		const_iterator end() const { return &data[size_]; }
		/**@}*/


		/**@brief default constructor, sets the whole array to 0 (ZERO)
		*/
		vec_() : data{0} {}

		/**@name vec_ copy and move constructors group
		 * @{
		 */
		vec_(vec_&&) noexcept = default;
		vec_(const vec_&) = default;
		vec_& operator=(vec_&&) noexcept = default;
		vec_& operator=(const vec_&) = default;
		/**@}*/

		/**@brief constructs the vector from an array with fixed compile time size
		 * e.g.:
		 * @code
	 	 * int x[3];
		 * vec3i myVec(x);
	 	 * @endcode (Note that the size of x is constant in this example).
		 * @param [in] l the array to be used as the vectors base data
		 */
		vec_(const T(&l)[S])
		{
			for (size_t i = 0; i < size_; i++) { data[i] = l[i]; }
		}
		vec_(const T& t) : data{t} {}


		/**@brief constructs the vector from an std::initializer_list with fixed compile time size
		 * e.g.: @code vec3i myVec = {1,2,3}; @endcode.
		 * @param [in] list the list to be used as the vectors base data
		 */
		template <typename Indicies = std::make_index_sequence<size_>>
		vec_(std::initializer_list<T>&& list) : vec_(std::forward<std::initializer_list<T>>(list), Indicies{})
		{}

	private:
		template <std::size_t... I>
		vec_(std::initializer_list<T>&& list, std::index_sequence<I...>) : vec_(std::forward<std::initializer_list<T>>(list),
			std::forward<typename std::initializer_list<T>::const_iterator>(list.begin() + I)...)
		{}

		template<class... Values>
		vec_(std::initializer_list<T>&& list, Values&&...v) :
		data{ list.size() == size_ ? *(v) :  //wth is going on here ?
			throw std::logic_error("LINT_ASM:ERR_ You tried to initialize the vector with an non matching amount of arguments in the initializer list!!")... }
		{}
	public:

		/**@brief reassigns the vector from an array with fixed compile time size
		 * e.g.:
		 * @code
		 * int x[3];
		 * vec3i myVec(x);
		 * @endcode (Note that the size of x is constant in this example).
		 * @param [in] l the array to be used as the vectors base data
		 */
		vec_& operator=(const T(&l)[S])
		{
			for (size_t i = 0; i < size_; i++) { data[i] = l[i]; }
			return *this;
		}

		/**@brief allows you to get an element of the vector with an index from 0..S with no runtime overhead
		 * @tparam N the index of the element you want to fetch
		 * @returns a copy of the value at N
		 */
		template <std::size_t N>
		type get() const noexcept
		{
			static_assert(N <= size_, "error cannot get<N>() when N exceedes maximum elements in array!");
			return data[N];
		}

		/**@brief allows you to get an non-const-reference to an element of the vector with an index from 0..S with no runtime overhead
		 * @tparam N the index of the element you want to fetch
		 * @returns a reference to the value at N
		 */
		template <std::size_t N>
		ref get() noexcept
		{
			static_assert(N <= size_, "error cannot get<N>() when N exceedes maximum elements in array!");
			return data[N];
		}

		template <std::size_t N>
		void set(type&& t ) noexcept
		{
			static_assert(N <= size_, "error cannot get<N>() when N exceedes maximum elements in array!");
			data[N] = t;
		}

		template <std::size_t N>
		void set(const type& t) noexcept
		{
			static_assert(N <= size_, "error cannot get<N>() when N exceedes maximum elements in array!");
			data[N] = t;
		}

		/**@brief allows you to get an element of the vector with an index from 0..S
		 * @param [in] N the index of the element you want to fetch
		 * @returns a copy of the value at N
		 */
		type operator[](std::size_t N) const
		{
			return data[N];
		}

		/**@brief allows you to get an non-const-reference to an element of the vector with an index from 0..S
		 * @param [in] N the index of the element you want to fetch
		 * @returns a reference to the value at N
		 */
		ref operator[](std::size_t N)
		{
			return data[N];
		}

		/**@brief applies a method on every element of the vector and returns the resulting vector
		 * (this is intended for lambdas with auto parameters) see @ref operator+() for examples
		 * @param [in] lhs the vector to apply the transformation on
		 * @param [in] method the method that gets applied to all elements
		 * @returns the resulting vector (Note: make sure that your function returns)
		 */
		friend vec_ transform_all(const vec_&lhs,std::function<type(const type&)> method)
		{
			vec_ res;
			for (size_t i = 0; i < S; i++)
			{
				res.data[i] = method(lhs.data[i]);
			}
			return res;
		}

		/**@brief applies a method on every element of the vector and stores it in the vector
		 * (this is intended for lambdas with auto parameters) see @ref operator+=() for examples
		 * @param [in] lhs the vector to apply the transformation on
		 * @param [in] method the method that gets applied to all elements
		 * @returns a reference to the modified vector
		 */
		friend vec_& apply_all(vec_& lhs, std::function<void(type&)> method)
		{
			for (size_t i = 0; i < S; i++)
			{
				method(lhs.data[i]);
			}
			return lhs;
		}

		/**@brief applies a method on every element of the vector using the elements of another vector and returns the resulting vector
		 * (this is intended for lambdas with auto parameters) see @ref operator+() for examples
		 * @param [in] lhs the vector to apply the transformation on
		 * @param [in] rhs the vector to fetch values from
		 * @param [in] method the method that gets applied to all elements
		 * @returns the resulting vector (Note: make sure that your function returns)
		 */
		friend vec_ transform_2(const vec_& lhs, const vec_& rhs, std::function<type(const type&, const type&)> method)
		{
			vec_ res;
			for (size_t i = 0; i < S; i++)
			{
				res.data[i] = method(lhs.data[i], rhs.data[i]);
			}
			return res;
		}

		/**@brief applies a method on every element of the vector using the elements of another vector and stores it in the vector
		 * (this is intended for lambdas with auto parameters) see @ref operator+=() for examples
		 * @param [in] lhs the vector to apply the transformation on
	 	 * @param [in] rhs the vector to fetch values from
		 * @param [in] method the method that gets applied to all elements
		 * @returns a reference to the modified vector
		 */
		friend vec_& apply_2(vec_& lhs, const vec_& rhs, std::function<void(type&, const type&)> method)
		{
			for (size_t i = 0; i < S; i++)
			{
				method(lhs.data[i], rhs.data[i]);
			}
			return lhs;
		}
		/**@brief gives you the length (or magnitude) of the vector @f[ \lvert v\rvert = \sqrt{v_0^2 \dots v_S^2}@f]
		*/
		type length() const
		{

			vec_ tmp;

			//create interim square storage
			std::transform(begin(), end(), tmp.begin(), [](const type& t) -> type { return pow(t, 2); });

			//get length
			return sqrt(std::accumulate(tmp.begin(), tmp.end(),type(0)));
		}

		/**@brief gives you the unit vector of this vector @f[ \frac{v}{\lvert v\rvert} @f]
		 */
		vec_ to_unit() const
		{
			return transform_all(*this,[&](auto t)
			{
				return t / length();
			});
		}

		/**@brief gives you the scalar product this vector @f[ lhs * rhs = lhs_0 * rhs_0 + \dots + lhs_S*rhs_S  @f]
		 * @param [in] lhs the left hand side operand
		 * @param [in] rhs the right hand side operand
		 */
		friend type scalarp(const vec_& lhs, const vec_& rhs)
		{
			vec_ tmp = lhs * rhs;
			return std::accumulate(tmp.begin(), tmp.end(),type(0));
		}

		friend type dotp(const vec_& lhs,const vec_& rhs)
		{
			return scalarp(lhs,rhs);
		}

		/**@brief gives you the angle between two vectors @f[ \alpha = cos^{-1}\left(\frac{lhs * rhs}{\lvert lhs\rvert * \lvert rhs\rvert}\right)  @f]
		* @param [in] lhs the left hand side operand
		* @param [in] rhs the right hand side operand
		*/
		pixl::math::angle<T> angle(const vec_& lhs,const vec_& rhs)
		{
			return pixl::math::angle<T>::acos(scalarp(lhs, rhs) / lhs.length()*rhs.length());
		}

		/**@name vec_ numerical operators
		 * @brief all the usual computational stuff like + & -
		 * @{*/
		friend vec_ operator+(const vec_& lhs, const vec_& rhs)
		{
			return transform_2(lhs,rhs,[](auto left, auto right)
			{
				return left + right;
			});
		}
		friend vec_ operator-(const vec_& lhs, const vec_& rhs)
		{
			return transform_2(lhs, rhs, [](auto left,auto right)
			{
				return left - right;
			});
		}

		friend vec_ operator-(const vec_& v)
		{
			return transform_all(v,[](auto value)
			{
				return -value;
			});
		}

		friend vec_ operator/(const vec_& lhs, const vec_& rhs)
		{
			return transform_2(lhs, rhs, [](auto left, auto right)
			{
				return left / right;
			});
		}
		friend vec_ operator*(const vec_& lhs, const vec_& rhs)
		{
			return transform_2(lhs, rhs, [](auto left, auto right)
			{
				return left * right;
			});
		}
		friend vec_ operator+(const vec_& lhs, const type& rhs)
		{
			return transform_all(lhs, [&](auto left)
			{
				return left + rhs;
			});
		}
		friend vec_ operator-(const vec_& lhs, const type& rhs)
		{
			return transform_all(lhs, [&](auto left)
			{
				return left - rhs;
			});
		}
		friend vec_ operator/(const vec_& lhs, const type& rhs)
		{
			return transform_all(lhs, [&](auto left)
			{
				return left / rhs;
			});
		}
		friend vec_ operator*(const vec_& lhs, const type& rhs)
		{
			return transform_all(lhs, [&](auto left)
			{
				return left * rhs;
			});
		}

		friend vec_& operator+=(vec_& lhs, const vec_& rhs)
		{
			return apply_2(lhs,rhs,[](auto& left,const auto& right)
			{
				left += right;
			});
		}
		friend vec_& operator-=(vec_& lhs, const vec_& rhs)
		{
			return apply_2(lhs, rhs, [](auto& left, const auto& right)
			{
				left -= right;
			});
		}
		friend vec_& operator/=(vec_& lhs, const vec_& rhs)
		{
			return apply_2(lhs, rhs, [](auto& left, const auto& right)
			{
				left /= right;
			});
		}
		friend vec_& operator*=(vec_& lhs, const vec_& rhs)
		{
			return apply_all(lhs, rhs, [](auto& left, const auto& right)
			{
				left *= right;
			});
		}

		friend vec_& operator+=(vec_& lhs, const type& rhs)
		{
			return apply_all(lhs, [=](auto& left)
			{
				left += rhs;
			});
		}
		friend vec_& operator-=(vec_& lhs, const type& rhs)
		{
			return apply_all(lhs, [=](auto& left)
			{
				left -= rhs;
			});
		}
		friend vec_& operator/=(vec_& lhs, const type& rhs)
		{
			return apply_all(lhs,[=](auto& left)
			{
				left /= rhs;
			});
		}
		friend vec_& operator*=(vec_& lhs, const type& rhs)
		{
			return apply_all(lhs, [=](auto& left)
			{
				left *= rhs;
			});
		}
    /**@}*/

		/**@name vec_ boolean operators group
		 * @brief boolean equality operators
		 * @{
		 */

		friend bool operator<(const vec_& lhs, const vec_& rhs)
		{
			return lhs.length() < rhs.length();
		}

		friend bool operator<=(const vec_& lhs, const vec_& rhs)
		{
			return !(rhs < lhs);
		}

		friend bool operator>(const vec_& lhs, const vec_& rhs)
		{
			return rhs < lhs;
		}

		friend bool operator>=(const vec_& lhs, const vec_& rhs)
		{
			return !(lhs < rhs);
		}

		friend bool operator==(const vec_& lhs, const vec_& rhs)
		{
			//are they literally the same?
			if (&lhs == &rhs) return true;

			for(size_t i = 0;i<size_;i++)
			{
				if(*(lhs.begin()+i) != *(lhs.begin()+i))
				{
					return false;
				}
			}
			return true;
		}

		friend bool operator!=(const vec_& lhs, const vec_& rhs)
		{
			return !(lhs == rhs);
		}
		/**@}*/

	};

#define P_VEC_ASSIGNABLE(SIZE) using vec_<T, SIZE>::vec_;\
		using vec_<T, SIZE>::operator=;\
		using vec_<T, SIZE>::length;\
		using vec_<T, SIZE>::to_unit;\
		vec(vec_<T, SIZE>&& v) : vec_<T, SIZE>(v){}\
		~vec()=default;\
		vec() : vec_<T, SIZE>(){}\
		vec(vec&&) noexcept = default;\
		vec(const vec&) = default;\
		vec& operator=(const vec& other)\
		{\
			this->vec_<T, SIZE>::operator=((const vec_<T, SIZE>&)other);\
			return *this;\
		}\
		vec& operator=(vec&& other)\
		{\
			this->vec_<T, SIZE>::operator=((vec_<T, SIZE>&&)other);\
			return *this;\
		}

	template <class T>
	struct vec<T,2> : vec_<T,2>
	{

		P_VEC_ASSIGNABLE(2)

		vec(T x,T y) : vec_<T,2>()
		{
			this->data[0] = x;
			this->data[1] = y;
		}
		typename vec_<T, 2>::ref x = this->vec_<T, 2>::template get<0>();
		typename vec_<T, 2>::ref y = this->vec_<T, 2>::template get<1>();
		typename vec_<T, 2>::ref w = this->vec_<T, 2>::template get<0>();
		typename vec_<T, 2>::ref h = this->vec_<T, 2>::template get<1>();

		typedef vec<T,2> self_t;

		auto normal() const -> vec<T, 2> { return { -y,x }; }


		auto unit_normal() const -> vec<T, 2>
		{
			return normal().to_unit();
		}

		static auto unit_from_angle(pixl::math::angle<T> a) -> vec<T, 2>
		{
			return self_t{ a.cos(),a.sin() };
		}

		auto with_angle(pixl::math::angle<T> a) const -> vec<T, 2>
		{
			auto u = unit_from_angle(a);
			T len = length();
			return u * len;
		}
		auto enclosed_angle() const -> pixl::math::angle<T>
		{
			return pixl::math::angle<T>::atan2(y,x) + pixl::math::angle<T>::deg(180);
		}

		auto rotate(pixl::math::angle<T> a) const -> vec<T, 2>
		{
			return self_t{ x * a.cos() - y * a.sin() , x * a.sin() + y * a.cos()};
		}

		auto rotate_around(pixl::math::angle<T> a , vec point) const -> vec<T, 2>
		{
			auto frame = *this - point;
			frame = frame.rotate(a);
			frame += point;
			return frame;
		}

		static constexpr auto i_hat () -> vec<T, 2> { return self_t{1,0}; };
		static constexpr auto j_hat () -> vec<T, 2> { return self_t{0,1}; };

		auto co_variant (vec<T, 2> ihat = i_hat(), vec<T, 2> jhat = j_hat()) const -> vec<T, 2> { return { dotp(*this,ihat),dotp(*this,jhat) }; }
		/**/
	};

  /**@brief predefined vector with 2 elements
	 * @tparam T the type of the vector
	 */
	template <class T>
	using vec2 = vec<T, 2>;
	/**@brief predefined vector with 2 elements and type float
	 */
	using vec2f = vec<float, 2>;
	/**@brief predefined vector with 2 elements and type double
	 */
	using vec2d = vec<double, 2>;
	/**@brief predefined vector with 2 elements and type int
	 */
  using vec2i = vec<int,2>;

	template <class T>
	struct vec<T, 3> : vec_<T, 3>
	{
		P_VEC_ASSIGNABLE(3)

		vec(T x, T y, T z) : vec_<T,3>()
		{
			this->data[0] = x;
			this->data[1] = y;
			this->data[2] = z;
		}

		//const static inline vec up{ 0, 1, 0 };


		typename vec_<T, 3>::ref x = this->vec_<T, 3>::template get<0>();
		typename vec_<T, 3>::ref y = this->vec_<T, 3>::template get<1>();
		typename vec_<T, 3>::ref z = this->vec_<T, 3>::template get<2>();
		typename vec_<T, 3>::ref r = this->vec_<T, 3>::template get<0>();
		typename vec_<T, 3>::ref g = this->vec_<T, 3>::template get<1>();
		typename vec_<T, 3>::ref b = this->vec_<T, 3>::template get<2>();
	};
	/**@brief predefined vector with 3 elements
	 * @tparam T the type of the vector
	 */
	template <class T>
	using vec3 = vec<T, 3>;
	/**@brief predefined vector with 3 elements and type float
	 */
	using vec3f = vec<float, 3>;
	/**@brief predefined vector with 3 elements and type double
	 */
	using vec3d = vec<double, 3>;
	/**@brief predefined vector with 3 elements and type int
	 */
  using vec3i = vec<int,3>;

	template <class T>
	struct vec<T, 4> : vec_<T, 4>
	{
		P_VEC_ASSIGNABLE(4)
		vec(T x, T y, T z,T w) : vec_<T,4>()
		{
			this->data[0] = x;
			this->data[1] = y;
			this->data[2] = z;
			this->data[3] = w;
		}
		typename vec_<T, 4>::ref x = this->vec_<T, 4>::template get<0>();
		typename vec_<T, 4>::ref y = this->vec_<T, 4>::template get<1>();
		typename vec_<T, 4>::ref z = this->vec_<T, 4>::template get<2>();
		typename vec_<T, 4>::ref w = this->vec_<T, 4>::template get<3>();
	};
	/**@brief predefined vector with 3 elements
	 * @tparam T the type of the vector
	 */
	template <class T>
	using vec4 = vec<T, 4>;
	/**@brief predefined vector with 3 elements and type float
	 */
	using vec4f = vec<float, 4>;
	/**@brief predefined vector with 3 elements and type double
	 */
	using vec4d = vec<double, 4>;
	/**@brief predefined vector with 3 elements and type int
	 */
	using vec4i = vec<int,4>;

	template <class T, std::size_t S>
	void swap(vec_<T,S>& lhs, vec_<T,S>& rhs) noexcept
	{
		using std::swap;
		swap(lhs.data, rhs.data);
	}

	/**@brief allow you to pretty print a vector to an std::ostream like cout or cerr
	 * @tparam T the type of the vector
	 * @tparam S the size of the vector
	 * @param os the ostream you want to write to
	 * @param obj the vector you want to print to the ostream
	 * @returns the ostream you wrote to
	 */
	template <class T, std::size_t S>
	std::ostream& operator<<(std::ostream& os, const vec_<T,S>& obj)
	{
		os << '[';
		for (auto i = obj.begin(); i != obj.end(); ++i)
		{
			os << *i;
			if (!(i + 1 == obj.end())) os << ',';
		}
		os << ']';
		return os;
	}

	/**@brief the crossproduct of the first 3 elements of two vectors with size 4 @f[ res = lhs \times rhs   @f]
	 * @tparam T the type of the vector
	 * @param lhs the left hand side operand
	 * @param rhs the right hand side operand
	 * @returns the crossproduct of the two vectors with an undefined w component
	 */
	template <class T>
	vec4<T> crossp(vec_<T,4> lhs,vec_<T,4> rhs)
	{
		const __m128 a = _mm_loadr_ps(lhs.ptr());
		const __m128 b = _mm_loadr_ps(rhs.ptr());

		const auto crossproduct = _mm_sub_ps(
		    _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 0, 2, 1)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 1, 0, 2))),
		    _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 1, 0, 2)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 0, 2, 1)))
		);

		vec4<T> result;
		_mm_store_ps(result.ptr(),crossproduct);
		return result;
	}
	template <class T>
	auto crossp(vec4<T> lhs, vec4<T> rhs) -> decltype(auto)
	{
		return crossp(static_cast<vec_<T, 4>>(lhs), static_cast<vec_<T, 4>>(rhs));
	}

	/**@brief the crossproduct of two vectors with size 3 @f[ res = lhs \times rhs   @f]
	 * @tparam T the type of the vector
	 * @param lhs the left hand side operand
	 * @param rhs the right hand side operand
	 * @returns the crossproduct of the two vectors
	 */
	template <class T>
	vec3<T> crossp(vec_<T,3> lhs,vec_<T,3> rhs)
	{
		const __m128 a = _mm_setr_ps(lhs.data[0],lhs.data[1],lhs.data[2],0);
		const __m128 b = _mm_setr_ps(rhs.data[0],rhs.data[1],rhs.data[2],0);

		const auto crossproduct = _mm_sub_ps(
		    _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 0, 2, 1)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 1, 0, 2))),
		    _mm_mul_ps(_mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 1, 0, 2)), _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 0, 2, 1)))
		);

		vec3<T> result;
        result.data[0] = _mm_cvtss_f32(_mm_shuffle_ps(crossproduct,crossproduct,0));
        result.data[1] = _mm_cvtss_f32(_mm_shuffle_ps(crossproduct,crossproduct,1));
        result.data[2] = _mm_cvtss_f32(_mm_shuffle_ps(crossproduct,crossproduct,2));
		return result;
	}
	template <class T>
	auto crossp(vec3<T> lhs, vec3<T> rhs) -> decltype(auto)
	{
		return crossp(static_cast<vec_<T, 3>>(lhs), static_cast<vec_<T, 3>>(rhs));
	}
	template <class T, template <class, size_t> class Vector>
	struct vcast_helper
	{
		template<class X,size_t S, size_t... Vals>
		static Vector<T,S> cast(const Vector<X,S>& x,std::index_sequence<Vals...>)
		{
			return Vector<T,S>{
				static_cast<T>(x.template get<Vals>())...
			};
		}
	};

	template <class T,class X,size_t S, template <class,size_t> class Vector>
	Vector<T,S> vector_cast(const Vector<X,S>& other)
	{
		auto sequence = std::make_index_sequence<S>();
		return vcast_helper<T, Vector>::cast(other, sequence);
	}
}
/**@} end of Math Group*/

namespace std
{
	///@brief helpers for c++17s based auto& [x,y,z] syntax
	///@{
	template<class T,std::size_t S>
	struct tuple_size <pixl::math::vec<T, S>>
		: std::integral_constant<std::size_t, S> {};

	template<std::size_t N,class T,std::size_t S>
	struct tuple_element<N,pixl::math::vec<T,S>>
	{
		using type = typename pixl::math::vec<T, S>::type;
	};
	///@}

}
