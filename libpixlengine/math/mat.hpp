#pragma once

#include <cstddef>
#include <stdexcept>
#include <array>
#include <ostream>
#include <string>
#include "common.hpp"
#ifdef _MSC_VER
#include <immintrin.h>
#else
#include <x86intrin.h>
#endif
#include "core/instruction_set.hpp"
#include "math/vector.hpp"
#include "core/debugging.hpp"


#if defined(__GNUC__)
#define __forceinline inline
#endif


///@brief Namespace for all math related matters
namespace pixl::math {
	using namespace std::literals;


	/**
	 *  @addtogroup Math
	 *  @{
	 */


	 /**@struct mat
	  * @brief represents a generic matrix with size N x M
	  * @tparam T the underlying type of the matrix (i.e.: double or int)
	  * @tparam N the number of rows of the matrix
	  * @tparam M the number of columns of the matrix
	  */
	template <class T, std::size_t N, std::size_t M>
	struct mat
	{
		typedef T type;

		typedef T& ref;
		typedef T* iterator;
		typedef const T* const_iterator;
		static std::size_t constexpr size_ = N * M;
		static std::size_t constexpr size() { return size_; }
		static std::pair<const std::size_t, const std::size_t> constexpr dimensions_ = { N,M };
		static std::pair<const std::size_t, const std::size_t> constexpr dimensions() { return dimensions_; }
		static bool constexpr square() { return N == M; }
		using is_square_t = std::bool_constant<M == N>;

		/**@struct Column
		 * @brief a single column with multiple rows
		 */
		struct Column
		{
			std::array<type, dimensions_.second> row; ///< the individual rows containing all the data
		};

		/**@union
		 * @brief holds the data of the matrix in different formats for ease of use
		 */
		union
		{
			type data[size_]; ///< the entire data represented in one continuous array
			std::array<std::array<type, dimensions_.second>, dimensions_.first> data_md; ///< the data structured into a multidimensional array
			Column columns[dimensions_.first]; ///< the data structured into structs (called columns) with a single member rows

		};

		PIXL_NODISCARD const_iterator begin() const
		{
			return data;
		}
		PIXL_NODISCARD const_iterator end() const
		{
			return data + size();
		}

		iterator begin()
		{
			return data;
		}

		iterator end()
		{
			return data + size();
		}

		/**@brief the default constructor for the matrix
		 * fills the entire data with 0 (ZERO)
		 */
		mat() : data{ 0 }
		{
		}

		~mat() = default;

		/**@brief this constructor takes exactly as many values as the matrix needs to be fully defined. e.g.:
		 * @code
		 * mat2f m0
		 * (
		 *	2.0f,1.0f,
		 *	3.0f,2.0f
		 * );
		 * @endcode
		 */
		template <class... Values>
		mat(Values... v) : data{ v... }
		{
			static_assert(sizeof...(v) == size_, "mismatch in size");
		}

		/**@brief this constructor takes exactly as many values as the matrix needs to be fully defined via an initializer list. e.g.:
		 * @code
		 * mat2f m0=
		 * {
		 *	2.0f,1.0f,
		 *	3.0f,2.0f
		 * };
		 * @endcode
		 */
		template <typename Indices = std::make_index_sequence<size_>>
		mat(std::initializer_list<T>&& list) : mat(std::forward<std::initializer_list<T>>(list), Indices{})
		{}

		/// support constructors for @code mat(std::initializer_list<T>&& list) @endcode
	private:
		template <std::size_t... I>
		mat(std::initializer_list<T>&& list, std::index_sequence<I...>) : mat(std::forward<std::initializer_list<T>>(list),
			std::forward<typename std::initializer_list<T>::const_iterator>(list.begin() + I)...)
		{}
		template<class... Values>
		mat(std::initializer_list<T>&& list, Values&&...v) :
			data{ list.size() == size_ ? *(v) :  //wth is going on here ?
			/*if you can configure your linter to look for the string LINT_ASM:ERR_ in your binary output as this will quite literally compile an exception into an array 
			 * hence this string will only  appear in your binary if this static comparision failed! additionally you can read the last two segments to also get the whereabouts of your error
			 * (static_assert( list.size == N ) is broken)
			 */
				throw std::logic_error("LINT_ASM:ERR_ You tried to initialize the vector with an non matching amount of arguments in the initializer list!! "s + __FILE__ + std::to_string(__LINE__))... }
		{}
	public:


		/**@brief this constructor takes exactly as many values as the matrix needs to be fully defined via nested initializer lists. e.g.:
		 * @code
		 * mat2f m0=
		 * {
		 *	{ 2.0f,1.0f },
		 *	{ 3.0f,2.0f }
		 * };
		 * @endcode
		 */
		template <typename Indices = std::make_index_sequence<dimensions_.first>>
		mat(std::initializer_list<std::array<T, dimensions_.second>>&& list) : mat(std::forward<std::initializer_list<std::array<T, dimensions_.second>>>(list), Indices{})
		{}

		/// support constructors for @code mat(std::initializer_list<std::array<T, dimensions_.second>>&& list) @endcode
	private:
		template<std::size_t... I>
		mat(std::initializer_list<std::array<T, dimensions_.second>>&& list, std::index_sequence<I...>) :
			mat(
				std::forward<std::initializer_list<std::array<T, dimensions_.second>>>(list),
				std::forward<typename std::initializer_list<std::array<T, dimensions_.second>>::const_iterator>(list.begin() + I)...
			)
		{}

		template<typename... Values>
		mat(std::initializer_list<std::array<T, dimensions_.second>>&& list, Values... values) :
			data_md{ list.size() == dimensions_.first ? *values : throw std::logic_error("LINT_ASM:ERR_ cannot create matrix from too little arguments! "s + __FILE__ + std::to_string(__LINE__))... }
		{}
	public:

		/**@brief overload for the stream operator to make pretty printing via an std::ostream (like cout or cerr) possible
		 */
		friend std::ostream& operator<<(std::ostream& os, const mat& obj)
		{

			//these are bracket parts according to IBM's weird code-page
			static 	std::array<char, 5> bracket_parts = {
				char(179), //middle-piece
				char(191), //top-right
				char(217), //bottom-right
				char(218), //top-left
				char(192)  //bottom-left
			};


			std::size_t spacing = 1;
			std::string strs[dimensions_.first][dimensions_.second];

			//check for the longest string
			for (std::size_t i = 0; i < dimensions_.first; i++)
			{
				for (std::size_t j = 0; j < dimensions_.second; j++)
				{
					strs[i][j] = std::to_string(round(obj.data_md[i][j] * T(10000)) / T(10000));

					//get rid of some precision (6 digits is just too much)
					strs[i][j] = strs[i][j].substr(0, strs[i][j].find('.') + 4);

					//save the spacing of this string
							  //max - macro expanded to save us from weird macro hell
					spacing = (((strs[i][j].length()) > (spacing)) ? (strs[i][j].length()) : (spacing));
				}
			}

			for (std::size_t i = 0; i < dimensions_.first; i++)
			{

				//print the right bracket part
				if (i == 0) os << bracket_parts[3];
				else if (i == dimensions_.first - 1) os << bracket_parts[4];
				else os << bracket_parts[0];

				// little bit of spacing
				os << ' ';

				for (std::size_t j = 0; j < dimensions_.second; j++)
				{
					if (j != 0) os << ", ";

					//enough padding to make the matrix exactly rectangular
					for (std::size_t spacer = 0; spacer < spacing - strs[i][j].length(); spacer++)
					{
						os << ' ';
					}

					//the actual data
					os << strs[i][j];
				}

				//more spacing
				os << ' ';

				//the right closing bracket part
				if (i == 0) os << bracket_parts[1];
				else if (i == dimensions_.first - 1) os << bracket_parts[2];
				else os << bracket_parts[0];

				//a new line for each line of the matrix
				os << pixl::newl;
			}
			return os;
		}

		PIXL_NODISCARD mat inv() const
		{
			mat inv;

			T det_ = T(1.0) / det();

			for (std::size_t j = 0; j < static_cast<std::size_t>(N); j++)
			{
				for (std::size_t i = 0; i < static_cast<std::size_t>(M); i++)
				{
					inv.data_md[i][j] = det_ * minor(j, i).det();
					if ((i + j) % 2 == 1) {
						inv.data_md[i][j] *= -1;
					}
				}
			}
			return inv;
		}

		PIXL_NODISCARD mat<T, N - 1, M - 1> minor(std::size_t row, std::size_t col) const
		{
			//can not create a minor if the row or col param is out of range!
			assert(col < N && row < M);
			mat<T, N - 1, M - 1> destination;

			std::size_t colc;
			std::size_t rowc = 0;
			for (auto& rowd : data_md) {
				colc = 0;
				for (auto& cold : rowd) {
					if (row != rowc && col != colc) {

						destination.data_md[col < colc ? colc - 1 : colc][row < rowc ? rowc - 1 : rowc] = cold;
					}
					colc++;
				}
				rowc++;
			}
			return destination;
		}

		template <std::size_t S, std::size_t order>
		struct det_impl_s;
			   
		template <typename _can_only_det_square_matrices = typename std::enable_if<is_square_t::value, int>::type, std::size_t order = M - 1>
		PIXL_NODISCARD T det(_can_only_det_square_matrices = 0) const
		{
			T det{};

			det += det_impl_s<order, order>::compute(this);
			return det;
		}

		//Note(algo-ryth-mix): im almost certain this could be sped up with sse ... haven't quite figured out how tho 
		//for now static compile-time optimization must suffice (i.e.: no loops)
		template <std::size_t S, std::size_t order>
		struct det_impl_s
		{
			static T compute(const mat* self)
			{
				if constexpr (order == 0) return T(self->data_md[0][0]);
				else {
					auto minor_ = self->minor(0, S);
					if constexpr (S != 0) {
						return (S % 2 == 1 ? -T(1.0) : T(1.0)) * self->data_md[0][S] * minor_.det() + det_impl_s<S - 1, order>::compute(self);
					}
					else {
						return  self->data_md[0][0] * minor_.det();
					}
				}
			}
		};

		template <typename X = typename std::enable_if<is_square_t::value, int>::type>
		PIXL_NODISCARD static mat identity(X = 0)
		{
			mat builder;
			for (int i = 0; i < N; i++) for (int j = 0; j < M; j++) {
				if (i == j) builder.data_md[i][j] = 1;
				else builder.data_md[i][j] = 0;
			}
			return builder;
		}

		PIXL_NODISCARD static mat zero()
		{
			mat builder;
			memcpy(builder.data, N*M * sizeof(T), 0);
			return builder;
		}

	};





	/**@name mat predefined mat types group
	 * @brief  all the predefined matrix types from mat2(x2) to mat4(x4) for all common types (int,float,double)
	 */
	 /**@{*/
	template <class T>
	using mat4x4 = mat<T, 4, 4>;

	template <class T>
	using mat3x4 = mat<T, 3, 4>;

	template <class T>
	using mat2x4 = mat<T, 2, 4>;

	template <class T>
	using mat4x3 = mat<T, 4, 3>;

	template <class T>
	using mat4x2 = mat<T, 4, 2>;

	template <class T>
	using mat4 = mat4x4<T>;

	using mat4f = mat4<float>;
	using mat4d = mat4<double>;
	using mat4i = mat4<int>;

	template <class T>
	using mat3x3 = mat<T, 3, 3>;

	template <class T>
	using mat2x3 = mat<T, 2, 3>;

	template <class T>
	using mat3x2 = mat<T, 3, 2>;

	template <class T>
	using mat3 = mat3x3<T>;

	using mat3f = mat3<float>;
	using mat3d = mat3<double>;
	using mat3i = mat3<int>;

	template <class T>
	using mat2x2 = mat<T, 2, 2>;

	template <class T>
	using mat2 = mat2x2<T>;

	using mat2f = mat2<float>;
	using mat2d = mat2<double>;
	using mat2i = mat2<int>;
	/**@}*/

#ifdef PIXL_HAS_SSESIMPLE
	PIXL_NODISCARD __forceinline static mat<float, 4, 4> fast_mul4by4(const mat<float, 4, 4>& lhs, const mat<float, 4, 4>& rhs);
	PIXL_NODISCARD __forceinline static mat2f fast_mul2by2(mat2f lhs, mat2f rhs);
#endif

	/**@brief matrix multiplication as defined by the following formula:
	 *
	 * @f[
		\left[\begin{matrix}T_{00}&\cdots&T_{0B_N}\\\vdots&\ddots&\vdots\\T_{A_N0}&\cdots&T_{A_NB_N}\\\end{matrix}\right]\ =lhs{\colon=}\left[\begin{matrix}T_{00}&\cdots&T_{0C_N}\\\vdots&\ddots&\vdots\\T_{A_N0}&\cdots&T_{A_NC_N}\\\end{matrix}\right]\ *\ rhs{\colon=}\left[\begin{matrix}T_{00}&\cdots&T_{0B_N}\\\vdots&\ddots&\vdots\\T_{C_N0}&\cdots&T_{C_NB_N}\\\end{matrix}\right]\
	 * @f]
	 * @param [in] lhs the left hand side operand matrix with size A_NxC_N
	 * @param [in] rhs the right hand side operand matrix with size C_NxB_N
	 * @return the resulting multiplied matrix with size A_NxB_N
	 */
	template <class T, std::size_t A_N, std::size_t B_N, std::size_t C_N>
	mat<T, A_N, B_N> operator*(const mat<T, A_N, C_N>& lhs, const mat<T, C_N, B_N>& rhs)
	{
		#ifdef PIXL_HAS_SSESIMPLE
		if constexpr(A_N == 4 && B_N == 4 && C_N == 4 && std::is_same<T,float>::value){
			// Note I left this check out as every PC after 1999 supports SSE1 and I think its ok to assume that this engine will only run on PC's built after that date
			// Excluding this check will significantly speed up the binary as this makes the function basically two parted (with an SSE only part)
			// ARM is not affected as this check is also guarded by a macro and arm needs a recompile anyways
			/*if(InstructionSet::SSE()){*/ 
				return fast_mul4by4(lhs,rhs);
			/*}*/
		}
		else if constexpr (A_N == 2 && B_N == 2 && C_N == 2 && std::is_same<T,float>::value){
			/*if(InstructionSet::SSE()){*/ 
			return fast_mul2by2(lhs,rhs);
			/*}*/
		}
		#endif

		mat<T, A_N, B_N> m;

		for (std::size_t i = 0; i < B_N; ++i) {
			for (std::size_t j = 0; j < A_N; ++j) {
				for (std::size_t k = 0; k < C_N; ++k)
				{
					m.data_md[i][j] += lhs.data_md[i][k] * rhs.data_md[k][j];
				}
			}
		}
		return m;
	}

	/**@brief converts a 1-column matrix to a vector
	 * @tparam T the type of both (must be the same)
	 * @tparam N the size of both (must be the same)
	 * @param [in] m the matrix you want to convert
	 * @return a vec<T,N> with the data of the passed matrix
	 */
	template <class T, std::size_t N>
	vec<T, N> mat_to_vec(const mat<T, N, 1>& m)
	{
		vec<T, N> v;
		memcpy(v.data, m.data, N * sizeof(T));
		return v;
	}

	/**@brief converts a 1-row matrix to a vector
	 * @tparam T the type of both (must be the same)
	 * @tparam N the size of both (must be the same)
	 * @param [in] m the matrix you want to convert
	 * @return a vec<T,N> with the data of the passed matrix
	 */
	template <class T, std::size_t N>
	vec<T, N> mat_to_vec_alt(const mat<T, 1, N>& m)
	{
		vec<T, N> v;
		memcpy(v.data, m.data, N * sizeof(T));
		return v;
	}

	/**@brief converts a vector to a 1-column matrix
	 * @tparam T the type of both (must be the same)
	 * @tparam N the size of both (must be the same)
	 * @param [in] v the vector you want to convert
	 * @return a mat<T,N,1> with the data of the passed vector
	 */
	template <class T, std::size_t N>
	mat<T, N, 1> vec_to_mat(const vec<T, N>& v)
	{
		mat<T, N, 1> d;
		memcpy(d.data, v.data, N * sizeof(T));
		return d;
	}

	/**@brief converts a vector to a 1-row matrix
	 * @tparam T the type of both (must be the same)
	 * @tparam N the size of both (must be the same)
	 * @param [in] v the vector you want to convert
	 * @return a mat<T,1,N> with the data of the passed vector
	 */
	template <class T, std::size_t N>
	mat<T, 1, N> vec_to_mat_alt(const vec<T, N>& v)
	{
		mat<T, 1, N> d;
		memcpy(d.data, v.data, N * sizeof(T));
		return d;
	}

	#ifdef PIXL_HAS_SSESIMPLE
	PIXL_NODISCARD __forceinline static vec4f fast_mul4by_vec(const mat4f&lhs,const vec4f& rhs);
	#endif
	/**@brief matrix-vector multiplication as defined by the following formula:
	 * @f[
		\left(\begin{matrix}T_0\\\vdots\\T_{B_N}\\\end{matrix}\right)=lhs{\colon=}\left[\begin{matrix}T_{00}&\cdots&T_{0B_N}\\\vdots&\ddots&\vdots\\T_{A_N0}&\cdots&T_{A_NB_N}\\\end{matrix}\right]\ *\ rhs{\colon=}\left(\begin{matrix}T_0\\\vdots\\T_{A_N}\\\end{matrix}\right)
	 * @f]
	 * @param [in] lhs the left hand side operand matrix with size A_NxB_N
	 * @param [in] rhs the right hand side operand vector with size A_N
	 * @return the resulting multiplied vector with size B_N
	 */
	template <class T, std::size_t A_N, std::size_t B_N>
	vec<T, B_N> operator*(const mat<T, A_N, B_N>& lhs, const vec<T, A_N>& rhs)
	{
		#ifdef PIXL_HAS_SSESIMPLE
		if constexpr (A_N == 4 && B_N == 4 && std::is_same<T,float>::value) {
			//See operator*(mat,mat)
			/*if(InstructionSet::SSE()){*/ 
			return fast_mul4by_vec(lhs,rhs);
			/*}*/
		}
		#endif


		vec<T, B_N> v;
		for (std::size_t i = 0; i < A_N; ++i) {
			for (std::size_t j = 0; j < B_N; ++j)
			{
				v.data[i] += lhs.data_md[i][j] * rhs.data[j];
			}
		}
		return v;
	}

#if defined(_MSC_VER)

	// _mm_mul_ps instead of * ? really who came up with this ?
	PIXL_NODISCARD inline static __m128 operator*(const __m128& lhs,const __m128& rhs)
	{
		return _mm_mul_ps(lhs,rhs);
	}
	// just why ?
	PIXL_NODISCARD inline static __m128 operator+(const __m128& lhs,const __m128& rhs)
	{
		return _mm_add_ps(lhs,rhs);
	}
#endif

	//when general statements fail you, you start to specialize 
	#ifdef PIXL_HAS_SSESIMPLE

	#define MakeShuffleMask(x,y,z,w)           (x | (y<<2) | (z<<4) | (w<<6))

	// vec(0, 1, 2, 3) -> (vec[x], vec[y], vec[z], vec[w])
	#define VecSwizzleMask(vec, mask)          _mm_castsi128_ps(_mm_shuffle_epi32(_mm_castps_si128(vec), mask))
	#define VecSwizzle(vec, x, y, z, w)        VecSwizzleMask(vec, MakeShuffleMask(x,y,z,w))
	#define VecSwizzle1(vec, x)                VecSwizzleMask(vec, MakeShuffleMask(x,x,x,x))
	// special swizzle
	#define VecSwizzle_0022(vec)               _mm_moveldup_ps(vec)
	#define VecSwizzle_1133(vec)               _mm_movehdup_ps(vec)

	// return (vec1[x], vec1[y], vec2[z], vec2[w])
	#define VecShuffle(vec1, vec2, x,y,z,w)    _mm_shuffle_ps(vec1, vec2, MakeShuffleMask(x,y,z,w))
	// special shuffle
	#define VecShuffle_0101(vec1, vec2)        _mm_movelh_ps(vec1, vec2)
	#define VecShuffle_2323(vec1, vec2)        _mm_movehl_ps(vec2, vec1)


	/**@brief multiplies two mat4f's 
	 * Note: in a pretty fast way (maybe not the fastest, but that's ok)
	 * Note: this function is called automatically by operator* so there is really no reason to call it manually
	 * @param [in] lhs the left-hand side operand
	 * @param [in] rhs the right-hand side operand
	 *
	 * @return the multiplied matrix 
	 */
	PIXL_NODISCARD __forceinline static mat<float, 4, 4> fast_mul4by4(const mat<float, 4, 4>& lhs, const mat<float, 4, 4>& rhs)
	{
		const __m128 BCx = _mm_load_ps(rhs.data);
		const __m128 BCy = _mm_load_ps(rhs.data + 0x4);
		const __m128 BCz = _mm_load_ps(rhs.data + 0x8);
		const __m128 BCw = _mm_load_ps(rhs.data + 0xC);

		mat<float, 4, 4> res;

		const float* lhs_row_ptr = lhs.data;
		float * res_row_ptr = res.data;

		for (size_t i = 0; i < 4; ++i, lhs_row_ptr += 4, res_row_ptr += 4) {
			__m128 ARx = _mm_set1_ps(lhs_row_ptr[0]);
			__m128 ARy = _mm_set1_ps(lhs_row_ptr[1]);
			__m128 ARz = _mm_set1_ps(lhs_row_ptr[2]);
			__m128 ARw = _mm_set1_ps(lhs_row_ptr[3]);

			//multiply whole rows
			__m128 X = ARx * BCx;
			__m128 Y = ARy * BCy;
			__m128 Z = ARz * BCz;
			__m128 W = ARw * BCw;

			__m128 R = X + Y + Z + W;
			_mm_store_ps(res_row_ptr,R);
		}
		return res;
	}
	

	PIXL_NODISCARD __forceinline  static vec4f fast_mul4by_vec(const mat4f&lhs,const vec4f& rhs)
	{

		// rhs.data is already packed to 4 bytes, this generates no assembly
		const __m128 vector = _mm_load_ps(rhs.data);
		vec4f result;


		//funnily enough the compiler will probably vectorize this for loop if possible and convert the below code to AVX512 if possible 
		// (clang++ did, as did gnu, I for the love of god can not read the output of msvc tho)
		for(size_t i = 0; i<4; i++) {
			// multiply entire row
			__m128 matrix_row = vector * _mm_load_ps(lhs.data + (4 * i));

			//add the row together

			// note this should compile to a single jmp (sadly no constexpr as cpuid %%eax can not be queried at compile time, also that wouldn't make much sense anyways) 
			// Im thinking about a jit-re-compiler that creates a static loadable resource which every pc can compile for itself 
			// alternatively this check could be removed, however this limits compatibility to CPU's built after 2004 :/ not sure on that one
			if (core::isa::InstructionSet::SSE3()) {

				// "fast" way
				matrix_row = _mm_hadd_ps(matrix_row,matrix_row);
				matrix_row = _mm_hadd_ps(matrix_row,matrix_row);
			} else {
				// "compatible" way
				matrix_row = _mm_add_ps(matrix_row, _mm_movehl_ps(matrix_row, matrix_row));
				matrix_row = _mm_add_ss(matrix_row, _mm_shuffle_ps(matrix_row, matrix_row, 1));
			}

			// this should compile to a single vcvttss2si eax,xmmN (intel) if not set PIXL_PLATFORM_BAD_MMSTOREPS
			_mm_store_ss(&result.data[i],matrix_row);
		}

		return result;
	}

	PIXL_NODISCARD __forceinline static vec4d fast_mul4by_vec(const mat4d&lhs,const vec4d &rhs)
	{
		vec4d result;
		if (core::isa::InstructionSet::AVX())
		{
			const __m256d vector = _mm256_load_pd(rhs.data);
		
			for (size_t i = 0; i < 4; i++)
			{
				__m256d matrix_row = _mm256_mul_pd(vector, _mm256_load_pd(lhs.data + (4 * i)));


				matrix_row = _mm256_hadd_pd(matrix_row, matrix_row);
				matrix_row = _mm256_hadd_pd(matrix_row, matrix_row);
				_mm256_store_pd(&result.data[i], matrix_row);

			}
		}
		return result;
	}

	
	//indeed very fast
	PIXL_NODISCARD __forceinline static __m128 fast_mul2by2sse(__m128 lhs,__m128 rhs)
	{
		return _mm_add_ps(_mm_mul_ps(lhs,VecSwizzle(rhs,0,3,0,3)),
						  _mm_mul_ps(VecSwizzle(lhs,1,0,3,2),VecSwizzle(rhs,2,1,2,1)));
	}
	PIXL_NODISCARD __forceinline static mat2f fast_mul2by2(mat2f lhs, mat2f rhs)
	{
		mat2f x;
		_mm_store_ps(x.data,fast_mul2by2sse(_mm_load_ps(lhs.data),_mm_load_ps(rhs.data)));
		return x;
	}

	/*
	 * void matmulSSE(int mat1[N][N], int mat2[N][N], int result[N][N]) {

		  for(int i = 0; i < N; ++i) {
		    for(int j = 0; j < N; j+=4) {   // vectorize over this loop
		        __m128i vR = _mm_setzero_si128();
		        for(int k = 0; k < N; k++) {   // not this loop
		            //result[i][j] += mat1[i][k] * mat2[k][j];
		            __m128i vA = _mm_set1_epi32(mat1[i][k]);  // load+broadcast is much cheaper than MOVD + 3 inserts (or especially 4x insert, which your new code is doing)
		            __m128i vB = _mm_loadu_si128((__m128i*)&mat2[k][j]);  // mat2[k][j+0..3]
		            vR = _mm_add_epi32(vR, _mm_mullo_epi32(vA, vB));
		        }
		        _mm_storeu_si128((__m128i*)&result[i][j], vR));
		    }
		  }
		}
	 */

	#endif
	#ifdef PIXL_AVX
	/* TODO:(algo-ryth-mix) __mm256_shuffle_hiepi32 && __mm256_shuffle_loepi32
	PIXL_NODISCARD __forceinline static __m256 fast_mul2by2avxd(__m256 lhs,__m256 rhs)
	{
		return _mm256_add_ps(__mm256_mul_ps(lhs,_mm256_castsi256_ps(_mm256_shuffle_epi64(_mm256_castsi256_ps(lhs),MakeSwizzleMask(0,3,0,3))))
							__mm256_mul_ps())
	}
	*/
	#endif

	/** @} End of Doxygen Groups*/
}

#if defined(__GNUC__)
#undef __forceinline
#endif
