#pragma once
namespace  pixl::math
{

	/**
	 *  @addtogroup Math
	 *  @{
	 */

	/**@brief PI in any numerical assignable type @f[ \pi @f]
	 * @tparam T the type you need your PI in.
	 */
	template <class T>
	constexpr T pi() { return static_cast<T>(3.1415926535897932384626433832795); }

	template <class T>
	constexpr T half_pi() { return pi<T>() * T(0.5);}

	/**@brief 2 * PI @f[ 2 * \pi @f]
	 * @tparam T the type you need your 2 * PI in.
	 */
	template <class T>
	constexpr T two_pi() { return pi<T>() * T(2); }

	/**@brief 2 * PI also known as tau @f[ \tau @f]
	 * @tparam T the type you need your TAU in.
	 */
	template <class T>
	constexpr T tau() { return pi<T>() * T(2); }

	/**@brief 1.5 PI ... because XKCD @f[ 1.5 * \pi @f]
	 * Note: https://xkcd.com/1292/
	 * @tparam T the type you need your 1.5 PI (PAU) in.
	 */
	template <class T>
	constexpr T pau() { return pi<T>() * T(1.5); }

	/**@brief gives the numerical constant to convert from a 2pi unit circle to degrees  @f[ \frac{\pi}{180} @f]
	 * @tparam T the type you need your conversion in.
	 */
	template <class T>
	constexpr T deg_to_rad() { return pi<T>() / T(180); }

	/**@brief gives the numerical constant to convert from degrees unit circle to 2pi unit circle @f[ \frac{180}{\pi}  @f]
	 * @tparam T the type you need your conversion in.
	 */
	template <class T>
	constexpr T rad_to_deg() { return T(180) / pi<T>(); }

	/** @} End of Doxygen Groups*/
}
