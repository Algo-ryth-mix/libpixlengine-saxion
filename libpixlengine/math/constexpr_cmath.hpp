#pragma once
#include "math/mathematical_consts.hpp"
#include <cstdint>
#include <cstddef>

namespace  pixl::math::cexp {

	#define MID ((lo + hi + 1) / 2)

	template <typename T, std::size_t MaxRec = 800>
	constexpr T sqrt_helper(T x, T lo, T hi)
	{
		if constexpr(MaxRec != 0)
			return lo == hi ? lo : ((x / MID < MID) ? sqrt_helper<T,MaxRec-1>(x, lo, MID - 1) : sqrt_helper<T,MaxRec-1>(x, MID, hi));
		return lo;
	}
	#undef MID

	template <typename T>
	constexpr T ct_sqrt(T x)
	{
	  return sqrt_helper<T>(x, 0, x / 2 + 1);
	}

	
	template <class T>
	T constexpr reduce_pi_upper(T value)
	{
		return value > math::pi<T>() ? reduce_pi_upper(value - math::two_pi<T>()) : value;
	}

	template <class T>
	T constexpr reduce_pi_lower(T value)
	{
		return value < -math::pi<T>() ? reduce_pi_lower(value + math::two_pi<T>()) : value;
	}

	template <class T>
	T constexpr reduce_pi(T value)
	{
		return value > math::pi<T>() ? reduce_pi_upper(value) : value < -math::pi<T>() ? reduce_pi_lower(value) : value;
	}

	template <class T>
	T constexpr fact(T value)
	{
		return value > 1 ?  value * fact(value -1) : 1;
	}

	template <class T>
	T constexpr pow_positive(T value,long Z)
	{
		return Z > 0 ? value * pow_positive(value,Z-1) :T(1);
	}

	template <class T>
	T constexpr cbc(T value)
	{
		return fact(2 * value) / (fact(value) * fact(value));
	}

	template <class T,std::size_t Prec>
	T constexpr taylor_sin(T value)
	{
		if constexpr (Prec != -1)
		{
			return taylor_sin<T,Prec-1>(value) + pow_positive(T(-1),Prec) * pow_positive(value,2 * Prec + 1) / fact(2 * Prec + 1);
		} 
		return T(0);
	}

	template <class T,std::size_t Prec>
	T constexpr maclaurin_arcsin(T value)
	{
		if constexpr (Prec != -1)
		{
			return maclaurin_arcsin<T,Prec-1>(value) + (T(1) / pow_positive(T(2),Prec * 2)) * cbc(T(Prec)) * pow_positive(value,2 * Prec + 1) / T(2 * Prec + 1);
		}
		return T(0);
	}

	template <class T,std::size_t Prec = 8>
	T constexpr sin (T value)
	{
		return taylor_sin<T,Prec>(reduce_pi(value));
	}

	template <class T,std::size_t Prec = 8>
	T constexpr cos(T value)
	{
		return sin<T,Prec>( value - half_pi<T>() );
	}

	template <class T,std::size_t Prec = 8>
	T constexpr tan(T value)
	{
		return sin<T,Prec>(value) / cos<T,Prec>(value);
	}

	template <class T,std::size_t Prec = 8>
	T constexpr asin(T value)
	{
		return maclaurin_arcsin<T,Prec>(value);
	}

	template <class T,std::size_t Prec = 8>
	T constexpr acos(T value)
	{
		return math::half_pi<T>() - asin(value);
	}

	template<class T,std::size_t Prec = 8>
	T constexpr atan(T value)
	{
		return asin(value/ct_sqrt(T(1 + value * value)));
	}

	template<class T, std::size_t Prec = 8>
	T constexpr atan2(T x,T y)
	{

		return 
		x >  T(0) ?					atan(y/x) :
		x <  T(0) && y >= T(0) ?	atan(y/x) + pi<T>() :
		x <  T(0) && y <  T(0) ?	atan(y/x) - pi<T>() :
		x == T(0) && y >  T(0) ?	+half_pi<T>() :
		x == T(0) && y <  T(0) ?	-half_pi<T>() :
		std::numeric_limits<T>::quiet_NaN();

	}
}