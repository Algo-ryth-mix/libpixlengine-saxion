#pragma once
#include "math/solve.hpp"

namespace pixl::math {
	/**
	 *  @addtogroup Math
	 *  @{
	 */

	/**@brief linear-interpolation: @f[ (1-t)* v_0 + t *v_1 @f]
	 * @tparam T the type you need your linear-interpolation in.
	 * @param [in] v0 point 0
	 * @param [in] v1 point 1
	 * @param [in] t the amount of mix between v0 and v1 from 0..1
	 */
	template <class T>
	constexpr T lerp(T v0,T v1,T t)
	{
		return (1 - t) * v0 + t * v1;
	}

	/**@brief bilinear-interpolation: @f[ lerp(lerp(c_{00},c_{11},t_x),lerp(c_{01},c_{11},t_x),t_y) @f]
	 * @tparam T the type you need your bilinear-interpolation in. also see
 	 * @ref lerp
	 * @param [in] c00 point x0 y0
	 * @param [in] c10 point x1 y0
	 * @param [in] c01 point x0 y1
 	 * @param [in] c11 point x1 y1
	 * @param [in] tx the amount of mix between c00 and c11 in the x axis from 0..1
	 * @param [in] ty the amount of mix between c00 and c11 in the y axis from 0..1
	 */
	template <class T>
	constexpr T blerp(T c00, T c10, T c01, T c11, T tx, T ty)
	{
		return lerp(lerp(c00,c10,tx),lerp(c01,c11,tx),ty);
	}
	

	template <class T>
	struct linear_continuous_solver
	{
		linear_continuous_solver(T&& _k, T&&_d): k(_k),d(_d){}

		linear_continuous_solver solve(T&& x)
		{
			y = linear(k,x,d);
			return *this;
		}
		T y;
		T k;
		T d;

	};

	PIXL_NODISCARD __forceinline float fast_inv_sqrt(float num)
	{
		return _mm_cvtss_f32(_mm_rsqrt_ss(_mm_set_ss(num)));
	}
	
	PIXL_NODISCARD __forceinline float fast_inv_sqrtQ3(float num)
	{
		const auto temp =(0x5f3759df - (*reinterpret_cast<int*>(&num) >> 1)); // divide exponent and mantissa by 2 (i>>1) and then get the mantissa back to "normal" with 0x magic
		return *reinterpret_cast<const float*>(&temp) * (1.5F - 0.5F * num * *reinterpret_cast<const float*>(&temp) * *reinterpret_cast<const float*>(&temp)); // get some precision back
	}

	/** @} End of Doxygen Groups*/

}
