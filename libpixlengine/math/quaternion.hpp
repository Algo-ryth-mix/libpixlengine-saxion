#pragma once

#include <ostream>
#include <utility>
#include "math/vector.hpp"
#include "math/mathematical_consts.hpp"
#include "math/mat.hpp"

#include <cmath>

namespace pixl::math
{
   /**
    *  @addtogroup Math
    *  @{
    */
	 template <class T>
	 struct quaternion
	 {
		 typedef T scalar_type;

		 scalar_type scalar = T(1);
		 vec3<scalar_type> vector{0.0,0.0,0.0};

		 quaternion() = default;
		 quaternion(const scalar_type& s, vec3<scalar_type> v) : scalar(s),vector(std::move(v)){}
		 quaternion(scalar_type&& s,vec3<scalar_type> v) noexcept : scalar(s),vector(std::move(v)){}
		 quaternion(const quaternion& other) = default;
		 quaternion(quaternion&& other) noexcept = default;
		 quaternion& operator=(const quaternion& other) = default;
		 quaternion& operator=(quaternion&& other) noexcept = default;

		 friend quaternion operator+(const quaternion& lhs,const quaternion& rhs)
		 {
			 return quaternion(lhs.scalar + rhs.scalar, lhs.vector + rhs.vector);
		 }
		 friend quaternion operator-(const quaternion& lhs, const quaternion& rhs)
		 {
			 return quaternion(lhs.scalar - rhs.scalar, lhs.vector - rhs.vector);
		 }

		 friend bool operator==(const quaternion& lhs, const quaternion& rhs)
		 {
			 return lhs.scalar == rhs.scalar
				 && lhs.vector == rhs.vector;
		 }

		 friend bool operator!=(const quaternion& lhs, const quaternion& rhs)
		 {
			 return !(lhs == rhs);
		 }

		 friend  quaternion& operator+=(quaternion& lhs, const quaternion& rhs)
		 {
			 lhs.scalar += rhs.scalar;
			 lhs.vector += rhs.vector;
			 return lhs;
		 }

		 friend  quaternion& operator-=(quaternion& lhs,const quaternion& rhs)
		 {
			 lhs.scalar -= rhs.scalar;
			 lhs.vector -= rhs.vector;
			 return lhs;
		 }

     /**@brief multiplication of two quaternion defined as such:
      * @f[ q = \left\{ s = s_{l} * s_{r} * (v_{l} * v_{r}); v = v_{r} * s_{l} + v_{l} * s_{r} + v_{l} \times v_{r} \right\} @f]
      */
		 friend quaternion operator*(const quaternion& lhs, const quaternion& rhs)
		 {
			 T w = lhs.scalar * rhs.scalar - lhs.vector.x * rhs.vector.x - lhs.vector.y * rhs.vector.y - lhs.vector.z * rhs.vector.z;
			 T x = lhs.scalar * rhs.vector.x + lhs.vector.x * rhs.scalar + lhs.vector.y * rhs.vector.z - lhs.vector.z * rhs.vector.y;
			 T y = lhs.scalar * rhs.vector.y - lhs.vector.x * rhs.vector.z + lhs.vector.y * rhs.scalar + lhs.vector.z * rhs.vector.x;
			 T z = lhs.scalar * rhs.vector.z + lhs.vector.x * rhs.vector.y - lhs.vector.y * rhs.vector.x + lhs.vector.z * rhs.scalar;
			 return quaternion(w, { x,y,z });
		 }

     /**@brief multiplication of two quaternion defined as such:
      * @f[ q = \left\{ s = s_{l} * s_{r} * (v_{l} * v_{r}); v = v_{r} * s_{l} + v_{l} * s_{r} + v_{l} \times v_{r} \right\} @f]
      */
		 quaternion multiply(const quaternion& other) const
		 {
			 T w = scalar * other.scalar -    vector.x * other.vector.x -  vector.y * other.vector.y -  vector.z * other.vector.z;
			 T x = scalar * other.vector.x +  vector.x * other.scalar   +  vector.y * other.vector.z -  vector.z * other.vector.y;
			 T y = scalar * other.vector.y -  vector.x * other.vector.z +  vector.y * other.scalar   +  vector.z * other.vector.x;
			 T z = scalar * other.vector.z +  vector.x * other.vector.y -  vector.y * other.vector.x +  vector.z * other.scalar;
			 return quaternion(w, { x,y,z });
		 }

     /**@brief multiplication of two quaternion defined as such:
      * @f[ q = \left\{ s = s_{l} * s_{r} * (v_{l} * v_{r}); v = v_{r} * s_{l} + v_{l} * s_{r} + v_{l} \times v_{r} \right\} @f]
      */

		 friend quaternion& operator*=(quaternion& lhs, const quaternion& rhs)
		 {
			 T w = lhs.scalar * rhs.scalar - lhs.vector.x * rhs.vector.x - lhs.vector.y * rhs.vector.y - lhs.vector.z * rhs.vector.z;
			 T x = lhs.scalar * rhs.vector.x + lhs.vector.x * rhs.scalar + lhs.vector.y * rhs.vector.z - lhs.vector.z * rhs.vector.y;
			 T y = lhs.scalar * rhs.vector.y - lhs.vector.x * rhs.vector.z + lhs.vector.y * rhs.scalar + lhs.vector.z * rhs.vector.x;
			 T z = lhs.scalar * rhs.vector.z + lhs.vector.x * rhs.vector.y - lhs.vector.y * rhs.vector.x + lhs.vector.z * rhs.scalar;
			 
			 lhs.scalar = w;
			 lhs.vector.x = x;
			 lhs.vector.y = y;
			 lhs.vector.z = z;
		 	return lhs;
		 }

     /**@brief the magnitude of the quaternion
      * @f[ \lvert q\rvert = \sqrt{s^2 + v^2} @f]
      */
		 scalar_type length() const
		 {
			 return sqrt(scalar*scalar + vector.x*vector.x + vector.y*vector.y + vector.z*vector.z);
		 }

     /**@brief the unit quaternion of the original one
      * @f[ q_u = \frac{q}{\lvert q\rvert} @f]
      */
		 quaternion to_unit() const
		 {
			 scalar_type n = length();
			 return quaternion(scalar / n, vector / n);
		 }

     /**@brief alternative unit quaternion of the original one
     * @f[ q_u = \left\{ s=cos\left(\frac{s*\frac{180}{\pi}}{2}\right); v=\frac{v}{\lvert v\rvert} * sin\left(\frac{s*\frac{180}{\pi}}{2}\right) \right\} @f]
      */
		 quaternion to_specunit() const
		 {
			scalar_type angle = scalar * deg_to_rad<scalar_type>() / scalar_type(2);

			return quaternion(std::cos(angle), vector.to_unit()*std::sin(angle));
		 }

     /**@brief the complex-conjugated form of the quaternion (note that) @f[ q == q^{*^*} @f]
     * @f[ q = \{ s = s;v = -v \} @f]
      */
		 quaternion conjugate() const
		 {
			 return quaternion(scalar, vector * -1);
		 }
     
     /**@brief the inverse form of the quaternion (note that) @f[ q == q^{-1^{-1}} @f]
     * @f[ q = \left\{  q^* * \frac{1}{\lvert q\rvert^2} \right\}  @f]
      */
		 quaternion inverse() const
		 {
			 scalar_type absolute = length();
			 absolute = 1 / (absolute * absolute);

			 quaternion conj = conjugate();
			 return { conj.scalar * absolute,conj.vector*absolute };
		 }

		 vec3<T> rotate_vec3(vec3<T> v)
		 {
			 quaternion p(0, v);
			 quaternion q(scalar, vector.to_unit());
			 q = q.to_specunit();
			 p = q * p * q.inverse();
			 return p.vector;
		 }

		 void rotate_axis(angle<T> theta,vec3<T> axis)
		 {
			 axis = axis.to_unit();

			 *this = this->to_unit();
			 T fact = sin(theta / T(2));



			 quaternion q;

			 q.vector. x = axis.x * fact;
			 q.vector. y = axis.y * fact;
			 q.vector. z = axis.z * fact;


			 q.scalar = cos(theta / T(2));





			 operator*=(*this,q);
			// *this = quaternion(w, { x,y,z }).to_unit() * (*this);
		 }

		 /**
		  * Returns a 3D rotation matrix.
		  * This is the "homogeneous" expression to convert to a rotation matrix,
		  * which works if the Quaternion is not a unit Quaternion.
		  */
		 inline math::mat4<T> to_rotation_matrix() {
			 // 21 operations?
			 T a2 = scalar * scalar, b2 = vector.x * vector.x, c2 = vector.y * vector.y, d2 = vector.z * vector.z;
			 T ab = scalar * vector.x, ac = scalar * vector.y, ad = scalar * vector.z;
			 T bc = vector.x * vector.y, bd = vector.x * vector.z;
			 T cd = vector.y * vector.z;

			return {
				 {a2 + b2 - c2 - d2, 2 * (bc - ad), 2 * (bd + ac),0},
				 {2 * (bc + ad), a2 - b2 + c2 - d2, 2 * (cd - ab),0},
				 {2 * (bd - ac), 2 * (cd + ab), a2 - b2 - c2 + d2,0},
				 {0,0,0,1}
			 };
		

		 }


		 friend std::ostream& operator<<(std::ostream& os, const quaternion& obj)
		 {
			 return os
				 << "{ " << obj.scalar
				 << " , " << obj.vector<< " }";
		 }
	 };
   /**@} end of group math*/
 }
