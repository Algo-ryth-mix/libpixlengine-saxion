#pragma once
#include <utility>
#include <cmath>
#include "math/mat.hpp"


namespace pixl::math{

	template <class T>
	inline bool constexpr is_quadratic_solvable_for_zero(T p, T q)
	{
		return std::pow(p, 2) / T(4) - q > 0;
	}

	template <class T>
	inline bool constexpr is_quadratic_solvable_for_zero(T a, T b, T c)
	{
		return std::pow(b, 2) - (4 * a * c) > 0;
	}


	template <class T>
	inline std::pair<T, T> constexpr solve_quadratic_for_zero(T p, T q)
	{
		return std::make_pair(
			(-p / T(2)) + std::sqrt(std::pow(p / T(2), 2) - q),
			(-p / T(2)) - std::sqrt(std::pow(p / T(2), 2) - q)
		);
	}

	template <class T>
	inline std::pair<T, T> constexpr solve_quadratic_for_zero(T a, T b, T c)
	{
		return std::make_pair(
			(-b + std::sqrt(std::pow(b, 2) - (4 * a * c))) / 2 * a,
			(-b - std::sqrt(std::pow(b, 2) - (4 * a * c))) / 2 * a
		);
	}

	template <class T>
	inline T constexpr linear(T k ,T x, T d)
	{
		//y = kx + d
		return k * x + d;
	}


	template <class T,std::size_t N>
	inline T constexpr solve_linear(mat<T,N,N -2> equations)
	{
		for(auto column : equations.columns ) {
			
		}
	}
/*	template<typename T,std::size_t N,std::size_t M>
	 void rref(mat<T,N,M>& A)
	{
	  typedef typename mat<T,N,M>::iterator index_type;
	 
	  index_type lead = A.begin();
	 
	  for (index_type row = A.begin(); row <= A.end(); ++row)
	  {
	    if (lead > A.columns.begin() + M -1)
	      return;
	    index_type i = row;
	    while (A[i][lead] == 0)
	    {
	      ++i;
	      if (i > A.end())
	      {
	        i = row;
	        ++lead;
	        if (lead >  A.columns.begin() + M -1)
	          return;
	      }
	    }
	    swap_rows(A, i, row);
	    divide_row(A, row, mt.element(A, row, lead));
	    for (i = mt.min_row(A); i <= mt.max_row(A); ++i)
	    {
	      if (i != row)
	        add_multiple_row(A, i, row, -mt.element(A, i, lead));
	    }
	  }
	}

*/
}

