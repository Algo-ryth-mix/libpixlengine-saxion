#pragma once
#include <memory>
#include <string_view>
#include <utility>
#include "core/resource.hpp"

namespace pixl::io {

	class DiskResource;

	class DiskView
	{


	public:
		DiskView() = default;
		DiskView(std::string path) : m_path(std::move(path)) {}


		PIXL_NODISCARD DiskResource read() const;
		void write(DiskResource&& res) const;
		void write(const DiskResource& res) const;

		PIXL_NODISCARD std::string extension() const;
		PIXL_NODISCARD std::string filename() const;
		PIXL_NODISCARD std::string relative_path() const;
		PIXL_NODISCARD std::string absolute_path() const;

		PIXL_NODISCARD bool exists() const;
		
	private:
		std::string m_path;

	};
	

	
	namespace literals {
		inline DiskView operator""_dv(const char * path,std::size_t size)
		{
			return DiskView(std::string(path,size));
		}
		
	}

}
