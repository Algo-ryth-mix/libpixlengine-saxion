#include "io/disk_view.hpp"
#include <string>
#include <regex>
#include "common.hpp"
#include <experimental/filesystem>
#include "core/file_core.hpp"
#include "io/disk_resource.hpp"


namespace pixl::io {
	using std::experimental::filesystem::path;

	std::string match_single_regex(const std::regex& r,const std::string & str,size_t required_match = 0)
	{
		std::smatch match;
		
		if(regex_search(str,match,r) && match.size() == required_match + 1) {
			return match[required_match];
		}

		return str::empty;
	}


	DiskResource DiskView::read() const
	{
		return DiskResource(core::io::read_file(m_path),*this);	
	}

	void DiskView::write(DiskResource&& res) const
	{
		core::io::write_file(m_path,res.data());
	}

	void DiskView::write(const DiskResource& res) const
	{
		core::io::write_file(m_path,res.data());
	}

	std::string DiskView::extension() const
	{
		const static std::regex expression(R"(\.[^.\\\/:*?"<>|\r\n]+$)");
		return match_single_regex(expression,filename());

	}

	std::string DiskView::filename() const
	{
		const static std::regex expression(R"(^(?:[A-z]\:)?[\/\\]?(?:.+[\\\/])*([^\\\/\s]+)$)");
		return match_single_regex(expression,m_path,1);
	}

	std::string DiskView::relative_path() const
	{
		return m_path;
	}
	std::string DiskView::absolute_path() const
	{
		return absolute(path(m_path)).string();
	}

	bool DiskView::exists() const
	{
		FILE* fp = fopen(absolute_path().c_str(), "rb");
		if(fp)
		{
			fclose(fp);
			return true;
		}
		return false;
	}
}
