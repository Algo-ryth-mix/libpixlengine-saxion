#pragma once
#ifndef __linux__
#define    _7ZIP_ST
#include <LZMA/Lzma2Enc.h> //Lzma2Enc...
#include <LZMA/Lzma2Dec.h>//Lzma2Dec...
#include <LZMA/Alloc.h> //BigAlloc, BigFree


#include <cstddef> //std::size_t
#include <cstdlib> //std::malloc, std::free
#include <cassert> //assert
#include <stdexcept> //std::runtime_error
#include <minwindef.h> // TRUE
#include "common.hpp" // byte_vec

//TODO: make compile on __linux__
//TODO: fix the magic header bug
//TODO: also fix the documentation for this file ... it's awful

namespace pixl::io {

	const std::size_t MAGIC_HEADER_SIZE = 5;


	inline static ISzAlloc MemFuncs{
		[](ISzAllocPtr,std::size_t size) -> void *	{return std::malloc(size);},
		[](ISzAllocPtr,void * addr)		 -> void	{std::free(addr);}
	};
	inline static ISzAlloc MemFuncsBig{
		[](ISzAllocPtr,std::size_t size) -> void *	{return BigAlloc(size);},
		[](ISzAllocPtr,void * addr)		 -> void	{return BigFree(addr);}
	};

	class LZMA2Encoder
	{
	public :
		LZMA2Encoder()
		{
			m_encoder = Lzma2Enc_Create(&MemFuncs,&MemFuncsBig);
			if(!m_encoder) throw std::runtime_error("Could not create Encoder!");

			Lzma2EncProps_Init(&m_props);
			m_props.lzmaProps.writeEndMark = TRUE;
			m_props.lzmaProps.level = 9;
			m_props.lzmaProps.numThreads = 2;


			const SRes sp_res = Lzma2Enc_SetProps(m_encoder,&m_props);
			if(sp_res != SZ_OK) throw std::runtime_error("Could not set Encoder Properties");

		}

	

		byte_vec encode_data(const byte_vec& in) const
		{

			//In the worst case (when decoding very little data) the buffer might be bigger then the input
			byte_vec result_vector(in.size() + MAGIC_HEADER_SIZE + 16);

			const pixl::byte wp_res = Lzma2Enc_WriteProperties(m_encoder);

			std::size_t available_size = result_vector.size() - MAGIC_HEADER_SIZE;

			const auto enc_result = Lzma2Enc_Encode2(m_encoder,NULL,result_vector.data()+MAGIC_HEADER_SIZE,&available_size,NULL,in.data(),in.size(),NULL);
			
			if(enc_result != SZ_OK) throw std::runtime_error("Could not encode data!");
			result_vector.resize(available_size+MAGIC_HEADER_SIZE);
			result_vector[0] = wp_res;

			return result_vector;
		}

		~LZMA2Encoder()
		{
			Lzma2Enc_Destroy(m_encoder);
		}


	private:



		CLzma2EncProps m_props;
		CLzma2EncHandle m_encoder;

	};

	class LZMA2Decoder
	{
	public:
		LZMA2Decoder()
		{
			Lzma2Dec_Construct(&m_decoder);
		}
		~LZMA2Decoder()
		{
			Lzma2Dec_Free(&m_decoder,&MemFuncsBig);
		}

		byte_vec decode_data(const byte_vec& in) const
		{
			byte_vec result_vector(10240);

			const byte* magic_header = in.data();

			SRes res = Lzma2Dec_Allocate(&m_decoder, magic_header[0], &MemFuncsBig);
			Lzma2Dec_Init(&m_decoder);

			ELzmaStatus status = LZMA_STATUS_NOT_FINISHED;

			std::size_t pos_in_resvector= 0;
			std::size_t pos_in_srcvector= 0;
			while(status == LZMA_STATUS_NOT_FINISHED) {
				
				const auto available_size = result_vector.size() + 1024;
				result_vector.resize(available_size);

				std::size_t writable_size = available_size-pos_in_resvector;

				std::size_t size_to_write = in.size() - MAGIC_HEADER_SIZE - pos_in_srcvector;

				const auto dec_res = Lzma2Dec_DecodeToBuf(&m_decoder,result_vector.data()+pos_in_resvector,&writable_size,in.data() + MAGIC_HEADER_SIZE + pos_in_srcvector,&size_to_write,LZMA_FINISH_ANY,&status);
				if(dec_res != SZ_OK) throw std::runtime_error("Could not decode data-chunk");

				pos_in_resvector += writable_size;
				pos_in_srcvector += size_to_write;

			}

			return result_vector;
		}
	private:
		mutable CLzma2Dec m_decoder;

	};

}
#endif //__linux__