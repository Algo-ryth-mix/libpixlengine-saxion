#pragma once
#include <utility>
#include "common.hpp"
#include "core/resource.hpp"
#include "io/disk_view.hpp"

namespace pixl::io {

class DiskResource : public core::resource
{
public:
	DiskResource(const DiskResource& other) = default;
	DiskResource(DiskResource&& other) noexcept = default;
	DiskResource& operator=(const DiskResource& other) = default;
	DiskResource& operator=(DiskResource&& other) noexcept = default;
	using core::resource::operator=;
	using core::resource::resource;


	DiskResource(const container_type& c, DiskView v) : core::resource(c) , m_view(std::move(v))
	{
	}

	DiskResource(container_type&& c, DiskView v) : core::resource(c) , m_view(std::move(v))
	{
	}


	std::string debug_info() override
	{
		return fmt::format("@{} disk-{}", m_view.absolute_path(), core::resource::debug_info());
	}
	void write() const	{ m_view.write(*this); }
	void read()			{ operator=(m_view.read()); }

	DiskView get_view() const { return m_view;}
	DiskView& modify_view() { return m_view; }

	private:
	DiskView m_view;
};
}
