#pragma once
#if defined(_MSC_VER)
	#define NOMINMAX
	#ifdef MFC_VER
		#define VC_EXTRALEAN
	#else
		#define WIN32_LEAN_AND_MEAN
	#endif
	#ifndef _CRT_SECURE_NO_WARNINGS
		#define _CRT_SECURE_NO_WARNINGS
	#endif
	#include <windows.h>
#endif

#include <vector>
#include <string>
namespace pixl
{
	namespace str{
		static std::string empty("");
	}

	static constexpr char newl = '\n';
	using byte = unsigned char;
	using byte_vec = std::vector<pixl::byte>;
}

#define PIXL_CPP17V 201703L

#include "git_rev.h"

#define PIXL_VERSION_MAJOR 0
#define PIXL_VERSION_MINOR 2
#define PIXL_VERSION_PATCH PIXL_GIT_VERSION_REF // git commit

#define STRINGIFY(X) STRINGIFY_INTERNAL(X)
#define STRINGIFY_INTERNAL(X) #X

#define PIXL_VERSION_STR "libpixl V: " STRINGIFY(PIXL_VERSION_MAJOR) "." STRINGIFY(PIXL_VERSION_MINOR) " patch:" STRINGIFY(PIXL_VERSION_PATCH)

#ifdef __has_cpp_attribute
#  define PHASCPPATTRIB(x) __has_cpp_attribute(x)
#else
#  define PHASCPPATTRIB(x) 0
#endif



#if __cplusplus >= PIXL_CPP17V || PHASCPPATTRIB(nodiscard)
#define PIXL_NODISCARD [[nodiscard]]
#else
#define PIXL_NODISCARD
#endif

#if (__cplusplus >= PIXL_CPP17V || PHASCPPATTRIB(fallthrough)) && !defined(_MSC_VER)
#define PIXL_FALLTHROUGH [[fallthrough]]
#else
#define PIXL_FALLTHROUGH
#endif

//! @brief the virtual base-class does not require this to be overriden and does not provide any functionality per default
#define PIXL_NOVIRTIMPLEMENT {} 

//! @brief the virtual base-class does require this to be overriden
#define PIXL_PUREVIRTUAL = 0

#define PIXL_NOIMPL

#ifdef __SSE__
	#define PIXL_HAS_SSESIMPLE
	#ifdef __SSE2__
		#define PIXL_HAS_SSE2
		#ifdef __SSE3__
			#define PIXL_HAS_SSEFULL
		#endif
	#endif
#endif

#ifdef __AVX__
	#define PIXL_HAS_AVXSIMPLE
	#ifdef __AVX2__
		#define PIXL_HAS_AVX2
		#ifdef __AVX512__
			#define PIXL_HAS_AVX512
		#endif
	#endif
#endif

#ifdef _M_IX86_FP
	#if _M_IX86_FP == 1 
		#define PIXL_HAS_SSESIMPLE
	#elif _M_IX86_FP == 2
		#define PIXL_HAS_SSESIMPLE
		#define PIXL_HAS_SSE2
	#endif
#elif defined(_M_AMD64) || defined(_M_X64)
	#define PIXL_HAS_SSESIMPLE
	#define PIXL_HAS_SSE2
	
#endif

#ifdef _MSC_VER
#define ptrap __debugbreak
#else
#include <signal.h>
#ifdef SIGTRAP
#define ptrap() raise(SIGTRAP)
#else
#define ptrap() raise(SIGABRT)
#endif
#endif


#if defined( _MSC_VER )
#define PIXL_EXPORT extern "C" __cdecl __declspec(dllexport)
#define PIXL_IMPORT extern "C" __cdecl __declspec(dllimport)
#else
#define PIXL_EXPORT __attribute__((visibility( default ))) extern "C" __cdecl
#define PIXL_IMPORT extern "C" extern __cdecl
#endif



//SECTOR FOR HEADER_ONLY LIBRARY IMPLEMENTATIONS

#if !defined( HEADER_ONLY_DEFINED )
#define HEADER_ONLY_DEFINED

#define FMT_HEADER_ONLY
#include <FMT/format.h>

#endif
