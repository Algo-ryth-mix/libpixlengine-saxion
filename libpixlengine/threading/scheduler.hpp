#pragma once
#include "core/fast_delegate.hpp"

#include <thread>
#include <atomic>
#include <deque>
#include <vector>

namespace pixl::threading {
	/**
	 * @addtogroup Concurrency
	 * @{
	 */

	/**@class Scheduler
	 * @brief this static class provides the basic interface for the 4 thread-types one can use in the Engine
	 * it is fully static as it does'nt make a whole lot of sense to have more then one set running per Application
	 * 
	 */
	class Scheduler
	{
	public:

		/**@brief Describes the type that every run-once task has to have to be accepted as such */
		using runnable = sa::delegate<void()>;

		/**@brief Describes the type that every continuous-run task has to have to be accepted as such */
		using runner = sa::delegate<void()>;

		/**@brief Starts Up the Task Scheduler, creates all the task including the optimal amount of pool-runners
		 * @param consume_main [in] when this parameter is set to true the Scheduler will run the Task assigned to manage on the 
		 *			main thread when it finished the creation of all other tasks, therefore it won't return
		 *			otherwise this function will return after it is done, also note that this affects join behaviour,
		 *			when the thread is set to consume main it will join automatically if not you have to join with @ref joinAll() manual
		 * @param core_offs [in] this parameter determines how many unmanaged threads you need for your own purpose and scale down the Pool respectively
		 *			please note that the Scheduler cannot go below a certain threshold: it will always reserve
		 *			one thread for draw, one for update, and at least one for the pool
		 */
		static void startScheduler(bool consume_main = true, std::size_t core_offs = 0);

		/**@brief schedules a task on the Manage thread that will be executed once
		 * this thread is meant as a hyper-visor for other threads, generally you don't want to schedule anything here,
		 * however this task is idle most of the time so in a high performance setting you can make use of this
		 * @param task [in] the task you want to execute once
		 */
		static void scheduleManageThread		(const runnable& task);

		/**@brief schedules a task on the Update thread that will be executed once
		 * this thread is meant for physic-simulation and game-logic, schedule a task here if you want to perform a game-event
		 * @param task [in] the task you want to execute once
		 */
		static void scheduleUpdateThread		(const runnable& task);

		/**@brief schedules a task on the Draw thread that will be executed once
		 * this thread is specialized in setting up draw parameters,
		 * scheduling a task here should only be use if you want to draw something and should not exceed a certain execution time.
		 * @param task [in] the task you want to execute once
		 */
		static void scheduleDrawThread			(const runnable& task);

		/**@brief schedules a task on the the next available Pool thread that will be executed once
		 *	this threads are general-purpose and can be use for calculations/tasks that are too heavy for other thread but can be done in the background
		 *	(e.g.: download something, unpack-data, etc...)
		 * @param task [in] the task you want to execute once
		 */
		static void scheduleOnNextAvailable		(const runnable& task); // run on next available idle thread 

		/**@brief checks if there are any tasks scheduled for the update thread
		 * You can use this to precisely sync two tasks or await context connections
		 */
		static bool updateTasksEmpty();

		/**@brief checks if there are any tasks scheduled for the draw thread
		 * You can use this to precisely sync two tasks or await context connections
		 */
		static bool drawTasksEmpty();

		/**@brief checks if there are any tasks scheduled for the manage thread
		 * You can use this to precisely sync two tasks or await context connections
		 */
		static bool manageTasksEmpty();

		/**@brief checks if there are any tasks scheduled for the pool
		 */
		static bool poolTasksEmpty();


		/**@brief sets the continuous runner for the update thread (think screen->update())
		 * @param updater [in] the runner you want to be called over and over again on the update thread
		 */
		static void setUpdateRunner		(const runner& updater);

		/**@brief sets the continuous runner for the draw thread (think screen->draw())
		 * @param drawer [in] the runner you want to be called over and over again on the update thread
		 */
		static void setDrawRunner		(const runner& drawer);

		/**@brief sets the continuous runner for the manager thread (think application->update())
		 * this is where management logic should go, aka how to change screens, how to end the application etc... 
		 * this is not available if the main was not consumed with startScheduler
		 * @param manager [in] the runner you want to be called over and over again on the update thread
		 */
		static void setManageRunner		(const runner& manager);


		/**@brief this will send a hit man after every thread and brutally murder them.
		 * Jokes aside, this will shutdown the Scheduler and probably your application.
		 * this only works if you application consumed main
		 */
		static void killApplication();


		/**@name individual thread stopper 
		 * @brief these will stop an individual thread (respectively with their name)
		 * or the pool
		 */
		/** @{ */
		//bunch o' hit man
		static void killUpdate();
		static void killDraw();
		static void killPool();
		/** @} */

		/** @brief this will join the threads once they have been ended.
		 * this function should only be called when you application DID NOT consume main
		 */
		static void joinAll();

		static bool isSchedulerStarted();

	private:
		//there are 3 distinct "reserved threads" one of witch is the main thread
		static std::thread m_updateThread;
		static std::thread m_drawThread;
		//there is no manage_thread, since that is the main thread anyways

		//this represents a pool of all-purpose threads
		static std::vector<std::thread> m_pool;

		//these are the task that can be scheduled on the manage-thread, the update-thread,
		// the draw-thread or the pool
		static std::deque<runnable> m_drawTasks;
		static std::deque<runnable> m_updateTasks;
		static std::deque<runnable> m_manageTasks;
		static std::deque<runnable> m_poolTasks;
		
		//this determine the runtime behaviour of the application
		static std::atomic<bool> m_keepDrawAlive;
		static std::atomic<bool> m_keepUpdateAlive;
		static std::atomic<bool> m_keepPoolAlive;
		static std::atomic<bool> m_keepApplicationAlive;

		//the runners
		static void schedulerDraw();
		static void schedulerUpdate();
		static void schedulerManage();
		static void schedulerPoolRunner();


	};
	/**@} end of Concurrency Group*/	
}
