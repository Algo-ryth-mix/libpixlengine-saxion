#pragma once
#include "threading/dispatcher.hpp"
#include "threading/scheduler.hpp"
#include <map>
#include <numeric>
#include <algorithm>
namespace pixl::threading {

	enum class AsyncDispatchMode
	{
		POOL,
		DRAW,
		UPDATE,
		MANAGE
	};

	struct SelfDestructCallee
	{
		sa::delegate<void()> func;
		bool* done_ptr;
		void call()
		{
			func();
			*done_ptr = true;
			delete this;
		}
	};


	template <AsyncDispatchMode Mode = AsyncDispatchMode::POOL>
	class AsyncDispatcher : public Dispatcher
	{
	public:
		void dispatch(sa::delegate<void()>, tag_t) override;
		bool await_all(tag_t) override;

	private:
		std::multimap<tag_t,bool> finishedTasks;

	};

	template <AsyncDispatchMode Mode>
	void AsyncDispatcher<Mode>::dispatch(sa::delegate<void()> fun, tag_t tag)
	{
		//prepare task
		auto i = finishedTasks.insert({tag,false});

		auto* callee = new SelfDestructCallee;
		callee->done_ptr = &i->second;
		callee->func = fun;

		//define a little macro to keep code dry
		#define P_SCHEDULEMODE(MODE,FUNCTION) \
			if constexpr(Mode == (MODE)) /*check the mode we are in*/ \
				{\
				Scheduler::FUNCTION(Scheduler::runnable::create<SelfDestructCallee,&SelfDestructCallee::call>(callee));\
				return;\
			}

		//generate branches for all enum types
		P_SCHEDULEMODE(AsyncDispatchMode::POOL	,scheduleOnNextAvailable)
		P_SCHEDULEMODE(AsyncDispatchMode::DRAW	,scheduleDrawThread)
		P_SCHEDULEMODE(AsyncDispatchMode::UPDATE,scheduleUpdateThread)
		P_SCHEDULEMODE(AsyncDispatchMode::MANAGE,scheduleManageThread)

		#undef P_SCHEDULEMODE
	}
	template <AsyncDispatchMode Mode>
	bool AsyncDispatcher<Mode>::await_all(tag_t tag)
	{
		//sanity check
		if(!Scheduler::isSchedulerStarted()) throw std::runtime_error("awaiting Scheduler that has not been started!");

		//get the range of task we are interested in
		auto [start,end] = finishedTasks.equal_range(tag);

		//accumulate them & check if they are all true
		const bool b = std::all_of(start, end, [](auto b) {return b.second; });
		if (!b) return false;

		//kill the finished ones
		finishedTasks.erase(start, end);

		
		return true;

	}
}
