#pragma once
#include <cstdint>
#include "core/fast_delegate.hpp"

namespace pixl::threading {
	class Dispatcher
	{
	public:

		Dispatcher() = default;
		Dispatcher(const Dispatcher& other) = default;
		Dispatcher(Dispatcher&& other) noexcept = default;
		Dispatcher& operator=(const Dispatcher& other) = default;
		Dispatcher& operator=(Dispatcher&& other) noexcept = default;
		virtual ~Dispatcher() = default;

		using tag_t = std::uint8_t;

		virtual void dispatch(sa::delegate<void()>,tag_t) = 0;
		virtual bool await_all(tag_t) = 0;
	};
}
