#pragma once

#include "core/fast_delegate_mc.hpp"
#include "threading/dispatcher.hpp"
#include <deque>
#include <map>

namespace pixl::threading {
	class QueuedDispatcher :public Dispatcher
	{
	public:

		void dispatch(sa::delegate<void()>, tag_t) override;
		bool await_all(tag_t) override;

	private:

		std::map<tag_t,sa::multicast_delegate<void()>> m_queue;
	};
}


inline void pixl::threading::QueuedDispatcher::dispatch(sa::delegate<void()> fun, tag_t tag)
{
	m_queue[tag] += fun;
}
inline bool pixl::threading::QueuedDispatcher::await_all(tag_t tag)
{
	if(m_queue.find(tag) == m_queue.end()) return true;
	m_queue[tag]();
	return true;
}
