#pragma once

#include "threading/dispatcher.hpp"

namespace  pixl::threading {
	class DefaultDispatcher : public Dispatcher
	{
		void dispatch(sa::delegate<void()>, tag_t) override;
		bool await_all(tag_t) override;
	};
}

inline void pixl::threading::DefaultDispatcher::dispatch(sa::delegate<void()> function, tag_t)
{
	function();
}
inline bool pixl::threading::DefaultDispatcher::await_all(tag_t)
{
	return true;
}
