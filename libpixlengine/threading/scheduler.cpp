#include "threading/scheduler.hpp"

#include <mutex>
#include <deque>
#include <chrono>
#include <iostream>

const int SCHEDULE_LIMIT = 255;
const int SCHEDULE_RELIEVE = 20;

namespace pixl::threading{
	//instatiate all the static fields
	std::deque<Scheduler::runnable> Scheduler::m_drawTasks;
	std::deque<Scheduler::runnable> Scheduler::m_updateTasks;
	std::deque<Scheduler::runnable> Scheduler::m_manageTasks;
	std::deque<Scheduler::runnable> Scheduler::m_poolTasks;

	std::atomic<bool> Scheduler::m_keepDrawAlive = true;
	std::atomic<bool> Scheduler::m_keepUpdateAlive = true;
	std::atomic<bool> Scheduler::m_keepPoolAlive = true;
	std::atomic<bool> Scheduler::m_keepApplicationAlive = true;

	std::thread Scheduler::m_drawThread;
	std::thread Scheduler::m_updateThread;
	std::vector<std::thread> Scheduler::m_pool;

	//nuli expose mutexi meos
	std::recursive_mutex drawTaskMutex;
	std::recursive_mutex updateTaskMutex;
	std::recursive_mutex manageTaskMutex;
	std::recursive_mutex poolTaskMutex;

	std::mutex drawRunnerMutex;
	std::mutex updateRunnerMutex;
	std::mutex manageRunnerMutex;

	bool scheduler_started = false;

	//the mental equivalent of `nop`
	void no_task()
	{
		NULL;
	}

	std::thread::id main_id;


	//runner objects
	Scheduler::runner drawRunner = Scheduler::runner::create<&no_task>();
	Scheduler::runner updateRunner =  Scheduler::runner::create<&no_task>();
	Scheduler::runner manageRunner =  Scheduler::runner::create<&no_task>();

	//this is like a constructor ... only that the class is fully static so not really
	void Scheduler::startScheduler(bool consume_main, std::size_t core_offs)
	{
		main_id = std::this_thread::get_id();
		if(scheduler_started) return;
		scheduler_started = true;

		//create the two stars of the show
		m_drawThread = std::thread(&Scheduler::schedulerDraw);
		m_updateThread = std::thread(&Scheduler::schedulerUpdate);


		//get the number of available cores
		auto coreCount = static_cast<long long>(std::thread::hardware_concurrency());
		
		//reserve the cores the user requested first
		coreCount -= core_offs;

		//Reserve one core for the OS
		coreCount -= 1;

		//Reserve one core for manage update and draw
		coreCount -= 3;


		//we can't have negative cores if we don't have enough hardware, just reserve one only
		if(coreCount < 0) coreCount = 1;
		for(long long i = 0;i < coreCount;++i) {
			m_pool.emplace_back(&Scheduler::schedulerPoolRunner);
		}

		if(consume_main) schedulerManage();
	}

	//Draw runner
	void Scheduler::schedulerDraw()
	{
		while(m_keepDrawAlive) {

			//universal draw Task ( think of a draw() function that gets run every cycle )
			if(drawRunnerMutex.try_lock())
			{
				drawRunner.execute();
				drawRunnerMutex.unlock();
			}
			//pre check if there is a task
			while(!m_drawTasks.empty()) {
				//lock the pool
				std::unique_lock<std::recursive_mutex> lock(drawTaskMutex);
				m_drawTasks.back()(); //execute task
				m_drawTasks.pop_back(); //delete task
			}
		}
	}

	//Update Runner
	void Scheduler::schedulerUpdate()
	{
		while(m_keepUpdateAlive) {
			
			//universal update Task ( think of a update() function that gets run every cycle )
			if(updateRunnerMutex.try_lock())
			{
				updateRunner.execute();
				updateRunnerMutex.unlock();
			}

			//pre check if there is a task
			while(!m_updateTasks.empty()) {
				//lock the pool
				std::unique_lock<std::recursive_mutex> lock(updateTaskMutex);
				m_updateTasks.back()(); //execute task
				m_updateTasks.pop_back(); //delete task
			}

		}
	}

	//Manage Runner
	void Scheduler::schedulerManage()
	{
		while(m_keepApplicationAlive) {

			//universal manage Task 
			if(manageRunnerMutex.try_lock())
			{
				manageRunner.execute();
				manageRunnerMutex.unlock();
			}
			//pre check if there is a task
			while(!m_manageTasks.empty()) {
				//lock the pool
				std::unique_lock<std::recursive_mutex> lock(manageTaskMutex);
				m_manageTasks.back()(); //execute task
				m_manageTasks.pop_back(); //delete task
			}
		}

		//Bloody Murder
		killUpdate();
		killDraw();
		killPool();

		//and join up when you are done
		joinAll();
	}

	//Pool Runner
	void Scheduler::schedulerPoolRunner()
	{
		using namespace std::literals;

		while(m_keepPoolAlive) {
			//pre check if there is any work
			if(!m_poolTasks.empty())
			{
				//prime the task with 0 work
				runnable task =  runnable::create<&no_task>();
				{
					std::unique_lock<std::recursive_mutex> lock(poolTaskMutex);

					//check again (maybe a different runner already snacked the task)
					if(!m_poolTasks.empty())
					{
						//run the task in the back
						task = m_poolTasks.back();
						m_poolTasks.pop_back();
					} // else continue;// (not needed as we prime with 0-work)
				}
				//execute the task
				task();
			} else {
				//if there isn't, sleep for a bit
			std::this_thread::sleep_for(1ms);
			}
		}
	}

	//expose all these interfaces to the class on the other side
	void Scheduler::scheduleManageThread(const runnable& task)
	{
		
		if (m_manageTasks.size() > SCHEDULE_LIMIT && std::this_thread::get_id() != main_id)
		{
			do
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				
			} while (m_manageTasks.size() > SCHEDULE_LIMIT - SCHEDULE_RELIEVE);
		}
		std::unique_lock<std::recursive_mutex> lock(manageTaskMutex);
		m_manageTasks.push_front(task);
	}

	void Scheduler::scheduleUpdateThread(const runnable& task)
	{
		if (m_updateTasks.size() > SCHEDULE_LIMIT && std::this_thread::get_id() != m_updateThread.get_id())
		{
			do
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(1));

			} while (m_updateTasks.size() > SCHEDULE_LIMIT - SCHEDULE_RELIEVE);
		}
		std::unique_lock<std::recursive_mutex> lock(updateTaskMutex);
		m_updateTasks.push_front(task);
	}

	void Scheduler::scheduleDrawThread(const runnable& task)
	{
		if (m_drawTasks.size() > SCHEDULE_LIMIT && std::this_thread::get_id() != m_drawThread.get_id())
		{
			do
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(1));

			} while (m_drawTasks.size() > SCHEDULE_LIMIT - SCHEDULE_RELIEVE);
		}
		std::unique_lock<std::recursive_mutex> lock(drawTaskMutex);
		m_drawTasks.push_front(task);
	}

	void Scheduler::scheduleOnNextAvailable(const runnable& task)
	{
		std::unique_lock<std::recursive_mutex> lock(poolTaskMutex);
		m_poolTasks.push_front(task);
	}

	bool Scheduler::updateTasksEmpty()
	{
		return m_updateTasks.empty();
	}

	bool Scheduler::drawTasksEmpty()
	{
		return m_drawTasks.empty();
	}

	bool Scheduler::manageTasksEmpty()
	{
		return m_manageTasks.empty();
	}

	bool Scheduler::poolTasksEmpty()
	{
		return m_poolTasks.empty();
	}

	void Scheduler::setUpdateRunner(const runner& updater)
	{
		std::unique_lock<std::mutex> lock(updateRunnerMutex);
		updateRunner = updater;
	}

	void Scheduler::setDrawRunner(const runner& drawer)
	{
		std::unique_lock<std::mutex> lock(drawRunnerMutex);
		drawRunner = drawer;
	}

	void Scheduler::setManageRunner(const runner& manager)
	{
		std::unique_lock<std::mutex> lock(manageRunnerMutex);
		manageRunner = manager;
	}

	void Scheduler::killUpdate()		{ m_keepUpdateAlive = false;}
	void Scheduler::killDraw()			{ m_keepDrawAlive  = false;}
	void Scheduler::killPool()			{ m_keepPoolAlive = false;}
	void Scheduler::killApplication()	{ m_keepApplicationAlive = false;}

	void Scheduler::joinAll()
	{
		for (std::thread& thread : m_pool) {
			thread.join();
		}
		m_updateThread.join();
		m_drawThread.join();
	}

	bool Scheduler::isSchedulerStarted()
	{
		return scheduler_started;
	}

}
