#pragma once
#include <limits>
#include <atomic>

#include "ecs/entity.hpp"
#include "core/subject.hpp"
#ifndef GAME_DETAIL_PROPERLY_INCLUDED
#define GAME_DETAIL_PROPERLY_INCLUDED
#endif
#include "game/detail.hpp"
#include "graphics/master_renderer.hpp"
#include "timing.hpp"
#include "input_manager.hpp"

namespace pixl::game
{
	/**@brief represents a Screen of your Game\n
	 * A Screen might be: \n
	 * - A Level of your game \n
	 * - A Menu-Screen \n
	 * - A Loading-Screen \n
	 * Or anything that can be seen as it's own little "sub-application"
	 * Every screen is also an Entity that can be seen as your "root" entity and it inherits all functionality of a traditional entity
	 */
	class ScreenBase : public ecs::Entity, virtual public core::subject_base , virtual public graphics::MasterRenderer
	{
	public:

		ScreenBase(const std::string &screen_name) : Entity("screen_"+screen_name){}
		virtual ~ScreenBase()=default;

		//these signify that the Screen want's to provoke an event in the MainGame Application of your Game
		//Do not use this standalone!
		//functions are provided for you (look below)
		enum screen_notification_t : event_t
		{
			EXIT_SCREEN = 101, // this means that the Screen would like to switch to a different screen
			EXIT_APPLICATION = 100 //this means that the Screen would like to exit the Application
		};


		/**@brief this gets the id of your screen
		 */
		screen_id_t get_id() const { return id; }

		float getSimulationDelta() const noexcept override
		{
			return m_simulationDelta;
		}

		float getGraphicsDelta() const noexcept override
		{
			return m_graphicsDelta;
		}

	protected:
		/**@brief these are the events that the MainGame will call on you
		 * @note The difference between the update and draw thread is that the draw thread may hold a special context \n
		 * Think of an OpenGL context for instance. They are not synced up and the draw thread may run significantly faster then the update thread
		 * Usually you should try to run as many tasks on Update as possible to keep a steady frame-rate
		 *@{
		 */

		/**@brief The MainGame will call this exactly once to make sure that the Screen has all its resources available */
		virtual void onCreate()				PIXL_NOVIRTIMPLEMENT;

		/**@brief The MainGame will call this every time the context switches to this screen 
		 * it will be called on the "Update" Thread of the application  */
		virtual void onEnterLogicEarly()	PIXL_NOVIRTIMPLEMENT;
		/**@brief The MainGame will call this every time the context switches to this screen
		 * it will be called on the "Draw" Thread of the application  */
		virtual void onEnterGraphicEarly()	PIXL_NOVIRTIMPLEMENT;

		/**@brief The MainGame will call this every time the context switches to this screen
		 * it will be called on the "Update" Thread of the application and after the Early Enters are done (both of them)  */
		virtual void onEnterLogic()			PIXL_NOVIRTIMPLEMENT;
		/**@brief The MainGame will call this every time the context switches to this screen
		 * it will be called on the "Draw" Thread of the application and after the Early Enters are done (both of them)  */
		virtual void onEnterGraphic()		PIXL_NOVIRTIMPLEMENT;

		/**@brief The Scheduler will call this every frame
		 * it will be called on the "Update" Thread of the application 
		 * @param [in] dt the deltaTime of the frame
		 */
		virtual void onUpdate(float dt)		PIXL_NOVIRTIMPLEMENT;

		/**@brief The Scheduler will call this every frame
		 * it will be called on the "Draw" Thread of the application
		 * @param [in] dt the deltaTime of the Simulation Frame
		 */
		virtual void onDraw(float dt)		PIXL_NOVIRTIMPLEMENT;

		/**@brief The MainGame will call this every time before the context leaves this screen
		 * it will be called on the "Update" Thread of the application  */
		virtual void onExitLogicEarly()		PIXL_NOVIRTIMPLEMENT;
		/**@brief The MainGame will call this every time before the context leaves this screen
		 * it will be called on the "Draw" Thread of the application  */
		virtual void onExitGraphicEarly()	PIXL_NOVIRTIMPLEMENT;

		/**@brief The MainGame will call this every time before the context leaves this screen
		 * it will be called on the "Update" Thread of the application  & after the Early Exits*/
		virtual void onExitLogic()			PIXL_NOVIRTIMPLEMENT;
		/**@brief The MainGame will call this every time before the context leaves this screen
		 * it will be called on the "Draw" Thread of the application  & after the Early Exits*/
		virtual void onExitGraphic()		PIXL_NOVIRTIMPLEMENT;

		/**@brief The MainGame will call this exactly once on the end of the Application, you may collect all your resources here and free them
		 */
		virtual void onDestroy()			PIXL_NOVIRTIMPLEMENT;

		/** @} */

		/**@brief call this from anywhere in your screen to leave this screen to a destination screen
		 * @param [in] whereto the screen you want to switch to
		 */
		void exit(screen_id_t whereto);

		/**@brief call this from anywhere to exit the application */
		void exitApplication() { notify(EXIT_APPLICATION); halt_execution = true; }

		MainGame* getApplication() const { return m_application; }
			   
	private:
		//We reserve a bit of storage to signify different events to the maingame
		enum FLAGS : std::uint8_t
		{
			CREATED = 0b00000001
		};
		float m_simulationDelta = 0.0f;
		float m_graphicsDelta = 0.0f;


		std::atomic<std::uint8_t> flags = 0;

		screen_id_t id = SID_BUILTIN_ERROR;
		friend class MainGame;

		MainGame* m_application = nullptr;

		void update_screen();
		void draw_screen();

		std::atomic<bool> halt_execution = false;

		screen_id_t m_yieldingScreen = SID_BUILTIN_ERROR;
	};

	inline void ScreenBase::update_screen()
	{
		if (halt_execution) return;
		const auto start = Time::acquisitionFpsTarget();
		m_simulationDelta = Time::getDelta();
		onUpdate(m_simulationDelta);
		Entity::update(m_simulationDelta);
		InputManager::update();
		Time::waitFpsTarget(start);
	}

	inline void ScreenBase::draw_screen()
	{
		if(halt_execution) return;
		const auto start = Time::acquisitionFpsTarget();
		m_graphicsDelta = Time::getDelta();
		onDraw(m_graphicsDelta);
		Entity::draw(m_graphicsDelta);
		Time::waitFpsTarget(start);
	}

	inline void ScreenBase::exit(screen_id_t whereto)
	{
		m_yieldingScreen = whereto;
		notify(EXIT_SCREEN);
		halt_execution = true;
	}
}
