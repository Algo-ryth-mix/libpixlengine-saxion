#pragma once
#ifndef GAME_DETAIL_PROPERLY_INCLUDED // the file generates unguarded forward-declarations guard them by making this file not include-able
#error "this file should not be included directly!" 
#endif


#include <limits>
#include <cstdint>
#include <cstddef> // std::uint32_t on *nix for whatever reason
#include <utility>

#include "core/fast_delegate.hpp"
#include "common.hpp"

namespace pixl::game
{

	//add forward declarations
	class ScreenBase;
	class MainGame;

	typedef std::uint32_t screen_id_t;



	//Honestly ... fvck windows
	#ifdef max
	#undef max
	#endif


	/**@brief this id signifies that you do not want to take control over the distribution of Screens and rather have the computer choose for you*/
	const screen_id_t SID_BUILTIN_ANY = std::numeric_limits<screen_id_t>::max() - 1;

	/**@brief this id signifies that there was an error assigning whatever id you wanted to give */
	const screen_id_t SID_BUILTIN_ERROR = std::numeric_limits<screen_id_t>::max();


	/**@brief this type is a surrogate for a constructor for a ScreenBase type object. you may use this if your custom class is not trivially constructable (or Singleton)*/
	typedef sa::delegate<std::shared_ptr<ScreenBase>()> screen_surrogate_ctor_t;

	namespace no_conflict
	{
		const static auto anti_deleter = [](ScreenBase*) -> void {};
	}

	typedef std::shared_ptr<ScreenBase> screen_smrt_t;

	/**@brief this type ensures that you do not access a lazily initialized screen, but still have the possibility of getting it's id.
	 * Note that if @code check_screen(proxy) == false @endcode and @code proxy.get_lazy_id() == SID_BUILTIN_ERROR @endcode that the engine was unable to create the screen for you
	 */
	struct screen_ptr_proxy
	{
		screen_ptr_proxy(screen_smrt_t ptr): data{std::move(ptr) }{}
		screen_ptr_proxy(screen_smrt_t ptr,screen_id_t id)  : data{std::move(ptr)},lazy_id(id){}
		operator screen_smrt_t() const { return data; }

		PIXL_NODISCARD screen_smrt_t screen() const { return data; }
		PIXL_NODISCARD screen_id_t get_lazy_id() const { return lazy_id; }

		/**@brief attempts to convert the Screen Proxy to a different type of screen
		 * @tparam T the ScreenType you want to have
		 * @returns a pointer to a screen if succeeded and nullptr otherwise
		 */
		template <class T>
		PIXL_NODISCARD T* as() noexcept
		{
			return dynamic_cast<T*>(data.get());
		}

		/**@brief checks if a Screen Proxy contains a certain type
		 * @tparam T the ScreenType you want to check for
		 * @returns true if it is and false otherwise
		 */
		template <class T>
		PIXL_NODISCARD bool is() const noexcept
		{
			return dynamic_cast<const T*>(data.get()) != nullptr;
		}
		

		screen_smrt_t data;
		screen_id_t lazy_id = SID_BUILTIN_ERROR;
	};

	//These should not be deleted as they have never been created in the first place ... they point to bogus addresses

	/** @brief signifies an invalid screen meaning that engine was unable to create your screen*/
	const static screen_smrt_t INVALID_SCREEN										(nullptr,							 no_conflict::anti_deleter);

	/** @brief signifies that the engine was unable to find your screen in the registry (it might have a cached ctor)
	 * if this is returned after creating a screen than the screen will always have a cached ctor and was/is going to be lazily initialized	 */
	const static screen_smrt_t SCREEN_NOT_IN_REGISTRY								(reinterpret_cast<ScreenBase*>(0x1), no_conflict::anti_deleter);

	/** @brief signifies that the engine was unable to find your screen at all */
	const static screen_smrt_t SCREEN_NOT_IN_REGISTRY_AND_CTOR_NOT_CACHED			(reinterpret_cast<ScreenBase*>(0x2), no_conflict::anti_deleter);

	/** @brief checks if a given screen is for whatever reason invalid 
	 *  @param [in] screen the screen to check */
	inline bool check_screen(const screen_smrt_t& screen)
	{
		return !(screen == INVALID_SCREEN || screen == SCREEN_NOT_IN_REGISTRY || screen == SCREEN_NOT_IN_REGISTRY_AND_CTOR_NOT_CACHED);
	}


}
