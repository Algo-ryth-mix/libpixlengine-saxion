#include "main_game.hpp"
#include "threading/scheduler.hpp"
#include "threading/asynchronous_dispatcher.hpp"
#include "game/screen.hpp"
#include <iostream>
#include "core/file_core.hpp"

void nop() {}

namespace pixl::game
{

	void MainGame::manage()
	{
		using namespace threading;
		using namespace std::literals;

		switch (m_state)
		{
		case NOT_INITIALIZED: throw std::logic_error("you did not run `run()` to get here, how did you do that ?");

		case SWITCHING_SCREENS:
		{

			//stop execution of current screen
			Scheduler::setUpdateRunner(Scheduler::runner::create<&nop>());
			Scheduler::setDrawRunner(Scheduler::runner::create<&nop>());


			//exit step 1
			Scheduler::scheduleDrawThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitGraphicEarly>(m_activeScreen.get()));
			Scheduler::scheduleUpdateThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitLogicEarly>(m_activeScreen.get()));

			//await exit step 1
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}

			//exit step 2
			Scheduler::scheduleDrawThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitGraphic>(m_activeScreen.get()));
			Scheduler::scheduleUpdateThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitLogic>(m_activeScreen.get()));

			//await exit step 2
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}

			{
				//query next screen 
				std::unique_lock<std::mutex> lock(m_screenMutex);

				//get the yield screen
				m_activeScreen = get_screen(nextScreen, true);


				//sanity check ... this might be bad if you return a screen that is very invalid
				if (m_activeScreen == INVALID_SCREEN || m_activeScreen == SCREEN_NOT_IN_REGISTRY || m_activeScreen == SCREEN_NOT_IN_REGISTRY_AND_CTOR_NOT_CACHED)
				{
					throw std::runtime_error("your screen cannot exit towards INVALID_SCREEN!");
				}
			}
		}
		PIXL_FALLTHROUGH case INITIALIZED:
		{

			//wait for all the steps that the initializer brought upon us before continuing
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}


			//check if the screen has been created before
			if (!(m_activeScreen->flags & ScreenBase::CREATED))
			{
				m_activeScreen->m_application = this;
				m_activeScreen->onCreate();
				m_activeScreen->initialize();
				m_activeScreen->flags |= ScreenBase::CREATED;
				subscribe(*m_activeScreen);
				m_activeScreen->halt_execution = false;
			}

			//set the dispatcher of the screen 
			m_activeScreen->setDispatchers<
				AsyncDispatcher<AsyncDispatchMode::POOL>,
				AsyncDispatcher<AsyncDispatchMode::DRAW>
			>();

			//init step 1
			Scheduler::scheduleDrawThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onEnterGraphicEarly>(m_activeScreen.get()));
			Scheduler::scheduleUpdateThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onEnterLogicEarly>(m_activeScreen.get()));


			//await init step 1
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}


			//init step 2
			Scheduler::scheduleDrawThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onEnterGraphic>(m_activeScreen.get()));
			Scheduler::scheduleUpdateThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onEnterLogic>(m_activeScreen.get()));


			//await init step 2
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}


			//set the runner
			Scheduler::setUpdateRunner(Scheduler::runner::create<ScreenBase, &ScreenBase::update_screen>(m_activeScreen.get()));
			Scheduler::setDrawRunner(Scheduler::runner::create<ScreenBase, &ScreenBase::draw_screen>(m_activeScreen.get()));

			//mark the application as "active"
			m_state = APPLICATION_RUNNING;
		}
		PIXL_FALLTHROUGH case APPLICATION_RUNNING:
		{
			//call the lil callback and otherwise sit tight
			onAppUpdate();
			if(m_window.pollEvents())
			{
				break;
			} 
		}
		PIXL_FALLTHROUGH case ENDING:
		{

			m_activeScreen->halt_execution = true;

			//kill the runners
			Scheduler::setUpdateRunner(Scheduler::runner::create<&nop>());
			Scheduler::setDrawRunner(Scheduler::runner::create<&nop>());


			// exit step 1
			Scheduler::scheduleDrawThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitGraphicEarly>(m_activeScreen.get()));
			Scheduler::scheduleUpdateThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitLogicEarly>(m_activeScreen.get()));

			// await exit step 1
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}

			//exit step 2
			Scheduler::scheduleDrawThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitGraphic>(m_activeScreen.get()));
			Scheduler::scheduleUpdateThread(Scheduler::runnable::create<ScreenBase, &ScreenBase::onExitLogic>(m_activeScreen.get()));

			//await exit step 2
			while (!Scheduler::drawTasksEmpty() || !Scheduler::updateTasksEmpty())
			{
				std::this_thread::sleep_for(1ms);
			}

			//final exit for all active screens
			for (auto &[id, screen] : m_screens)
			{
				screen->onDestroy();
				screen->flags &= ~ScreenBase::CREATED;
			}

			//park application in ended state
			m_state = ENDED;
		}
		PIXL_FALLTHROUGH case ENDED:

			//terminate
			Scheduler::killApplication();
			break;

		}
	}

	void set_console_title(const char* str)
	{
#ifdef _MSC_VER
		SetConsoleTitleA(str);
#else
		std::cout << "\033]0;" << str << "\007";
#endif
	}

	void splash_screen(bool cool)
	{
		using namespace std::literals;

		auto file = core::io::read_file("SPLASH_SCREEN.TXT");

		for (auto& i : file)
		{
			std::cout << i;
			if (cool && i != ' ')
				std::this_thread::sleep_for(100ns * (std::rand() % (100 - 50) + 50));
		}


		std::cout << PIXL_VERSION_STR << std::endl;

#if defined(DEBUG) || defined(_DEBUG)
		core::_fg_red();
		std::cout << "WARNING: DEBUG-BUILD, expect performance degradation, debug-symbols available" << std::endl;
		core::_fg_default();
#endif

	}

	int MainGame::run(int argc, char** argv, math::vec2<size_t> size)
	{
		if (argc)
		{
			assert(argv);// you forgot to also pass argv!
			m_args = std::vector<char*>(argv ,argv + argc);

		}

		set_console_title("libpixlengine console!");
		splash_screen(false);

		//application initializer
		onAppInit();
		using namespace threading;

		m_window.init(size , "Game Window");
		subscribe(m_window);

		//register the screens
		makeScreens();
		if (m_activeScreen == INVALID_SCREEN) throw std::logic_error("You need to mark a screen active in `makeScreens`!");


		//set the executor
		Scheduler::setManageRunner(Scheduler::runner::create<MainGame, &MainGame::manage>(this));

		m_window.callback<core::KEY>(InputManager::make_key_callback());

		m_state = INITIALIZED;

		//give up execution control
		Scheduler::startScheduler();


		//application finalizer
		onAppExit();
		return 0;
	}




	void MainGame::onNotify(core::subject_base* subject_base, core::subject_base::event_t event)
	{
		if (event == ScreenBase::EXIT_SCREEN)
		{
			assert(m_activeScreen.get() == subject_base); // you have two engines running ... or something else that is quite weird
			std::unique_lock<std::mutex> lock(m_screenMutex);

			//save the id with us instead, MainGame is just a bit more secure
			nextScreen = m_activeScreen->m_yieldingScreen;

			//change the state of the application 
			m_state = SWITCHING_SCREENS;
		}
		if (event == ScreenBase::EXIT_APPLICATION)
		{
			//memento mor
			m_state = ENDING;
		}
	}



	screen_ptr_proxy MainGame::addScreen(const screen_surrogate_ctor_t& surrogate, screen_id_t id, bool lazy)
	{
		assert(m_screens.find(id) == m_screens.end()); // screen already exists!
		assert(m_preparedScreens.find(id) == m_preparedScreens.end()); // constructor for screen already exists!
		if (id == SID_BUILTIN_ANY)
		{
			id = find_lowest_available_key(m_screens);
		}

		if (lazy)
		{
			m_preparedScreens.emplace(id, surrogate);
			return screen_ptr_proxy(SCREEN_NOT_IN_REGISTRY, id);
		}
		auto s = m_screens.emplace(id, std::invoke(surrogate)).first->second;
		s->id = id;
		return s;
	}

	screen_id_t MainGame::addScreen(std::shared_ptr<ScreenBase> screen, screen_id_t id)
	{
		assert(m_screens.find(id) == m_screens.end()); // screen already exists!
		assert(m_preparedScreens.find(id) == m_preparedScreens.end()); // constructor for screen already exists!
		if (id == SID_BUILTIN_ANY)
		{
			id = find_lowest_available_key(m_screens);
		}
		screen->id = id;
		m_screens.emplace(id, screen);
		return id;
	}

	void MainGame::makeScreenActive(screen_id_t id)
	{
		auto screen = get_screen(id, true);

		if (screen == SCREEN_NOT_IN_REGISTRY_AND_CTOR_NOT_CACHED) throw std::logic_error("The screen you tried to make active does not exist!");
		m_activeScreen = screen;

	}


	void MainGame::makeScreenActive(screen_ptr_proxy proxy)
	{
		if (check_screen(proxy))
		{
			m_activeScreen = proxy;
		}
		else if (proxy.get_lazy_id() == SID_BUILTIN_ERROR) throw std::logic_error("The screen you tried to make active is not valid");
		else makeScreenActive(proxy.get_lazy_id());
	}

	void MainGame::makeScreenActive(screen_smrt_t ptr)
	{
		if (check_screen(ptr))
		{
			m_activeScreen = ptr;
		}
		else throw std::logic_error("The screen you tried to make active is not valid");
	}

	std::shared_ptr<ScreenBase> MainGame::get_screen(screen_id_t id, bool lazy_init)
	{
		//prepare the iterator where will store our result
		decltype(m_screens)::iterator iter;

		//check if it already exists
		if ((iter = m_screens.find(id)) == m_screens.end())
		{

			//bail out early if we don't allow lazy init's
			if (!lazy_init) return SCREEN_NOT_IN_REGISTRY;

			//c++17 if(<init>,<stmt>) syntax! FUCK YEAH!!! xD
			if (decltype(m_preparedScreens)::iterator prepared_iter; (prepared_iter = m_preparedScreens.find(id)) == m_preparedScreens.end())
			{
				//there was no option to create the screen either, bail!
				return SCREEN_NOT_IN_REGISTRY_AND_CTOR_NOT_CACHED;
			}

			//slightly awkward conversion function... it really only calls the surrogate_ctor from the second map
			else iter = m_screens.emplace(prepared_iter->first, std::invoke(prepared_iter->second)).first;

			iter->second->id = iter->first;
		}

		//we already know the id, return the actual pointer
		return iter->second;

	}

	std::string MainGame::get_arg(std::size_t which) const noexcept
	{
		if(which > m_args.size())
		{
			return str::empty;
		}
		return std::string(m_args[which]);
	}
}
