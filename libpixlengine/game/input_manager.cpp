#include "game/input_manager.hpp"
namespace pixl::game
{
	std::map<int32_t, bool>* InputManager::m_now_pressed_keys = new std::map<int32_t, bool>;
	std::map<int32_t, bool>* InputManager::m_previously_pressed_keys = new std::map<int32_t, bool>;
	std::map<int32_t, bool>* InputManager::m_now_released_keys = new std::map<int32_t, bool>;
	std::map<int32_t, int>*  InputManager::m_sticky_pressed = new std::map<int32_t, int>;
	std::map<int32_t, int>*  InputManager::m_sticky_released = new std::map<int32_t, int>;

	std::mutex InputManager::key_map_lock;

}