#pragma once
#include <cstdint>
#include <limits>
#include <memory>
#include <map>
#include <cassert>

#include "core/fast_delegate.hpp"
#include "core/subscriber.hpp"

#ifndef GAME_DETAIL_PROPERLY_INCLUDED
#define GAME_DETAIL_PROPERLY_INCLUDED
#endif
#include "game/detail.hpp"
#include <mutex>
#include "core/window.hpp"
#include "math/quaternion.hpp"


#define PIXL_RUN_APPLICATION(APP)\
int main()\
{\
	APP app;\
	return app.run();\
}
/**\cond INTERNAL*/
//forward declaration
namespace pixl::threading { class Scheduler; }
/**\endcond*/

namespace pixl::game
{


	class MainGame : public core::subscriber_base
	{
	public:
		/**@brief runs the application
		 * This will consume the main-thread
		 * @param argc the application argc
		 * @param argv the application argv
		 * @param size the size of the window
		 * @return the return code for main
		 */
		int run(int argc = 0, char** argv = nullptr, math::vec2<size_t> size = {1280, 800});

		void onNotify(core::subject_base* subject_base, core::subject_base::event_t event) override;

		core::Window& getWindow()
		{
			return m_window;
		}

	protected:

		//called before the application initializes
		virtual void onAppInit() PIXL_NOVIRTIMPLEMENT;

		//called after the application initialized, but before the threads are initialized you must register your screens here and set one as active
		virtual void makeScreens() PIXL_PUREVIRTUAL;

		//called every cycle on the manage thread 
		virtual void onAppUpdate() PIXL_NOVIRTIMPLEMENT;

		//called after the threads finished
		virtual void onAppExit() PIXL_NOVIRTIMPLEMENT;


		/**@brief you may received application arguments from this. if the return is str::empty then that argument does not exist
		 * @param [in] which the position of the parameter you want to get
		 * @return the value of that parameter
		 */
		std::string get_arg(std::size_t which) const noexcept;


		/**@brief adds a Screen to the Application, traditionally you would call this in your make_screens() function but in theory nothing keeps you from creating more screens afterwards.\n
		 * this is not thread-safe! you may call it from everywhere, but you need to find your own method of preventing bad insertions
		 * @tparam T the type of the Screen you want to add, keep in mind that this must be of base ScreenBase and must be trivially constructable (there are other functions available for Screens that are not)
		 * @param [in] id the id of the screen you want to create if you use SID_BUILTIN_ANY that id will be assigned for you. note that it is discouraged to mix SID_BUILTIN_ANY with custom SID's
		 * @note SID_BUILTIN_ANY will always use the next available slot! 
		 * @param [in] lazy set this to true if you want your screen to be lazily initialized, the screen will stay unallocated until it is finally needed (this may cut down on initial load-times but increase the load-times during the game)
		 * @return if lazy was true: returns a proxy with the value SCREEN_NOT_IN_REGISTRY, if your id was SID_BUILTIN_ANY you can get the id with @code proxy.get_lazy_id() @endcode \n
		 *			if lazy was false: returns a proxy with the value of your screen, if your id was SID_BUILTIN_ANY you can get the id with @code proxy.screen.get_id() @endcode \n
		 *			if the Engine was unable to create your screen the return value will be INVALID_SCREEN 
		 * @note in DEBUG this function will `assert()` if you try to add a screen that was already added, in RELEASE it will emit undefined behaviour
		 */
		template<class T>
		screen_ptr_proxy addScreen(std::enable_if_t<std::is_base_of_v<ScreenBase, T>, screen_id_t> id = SID_BUILTIN_ANY, bool lazy = false)
		{
			if (id == SID_BUILTIN_ANY)
			{
				id = find_lowest_available_key(m_screens);
			}

			if (id == SID_BUILTIN_ERROR)
				return INVALID_SCREEN;

			if (lazy)
			{
				prepare_screen_instance<T>(id);
				return screen_ptr_proxy(SCREEN_NOT_IN_REGISTRY,id);
			}
			make_screen_instance<T>(id);
			m_screens.at(id)->id = id;
			return m_screens.at(id);
		}

		/**@brief adds a Screen to the Application, traditionally you would call this in your make_screens() function but in theory nothing keeps you from creating more screens afterwards. \n
		 * this is not thread-safe! you may call it from everywhere, but you need to find your own method of preventing bad insertions
		 * @param [in] surrogate a replacement for the constructor of a screen, this can be a lambda, a function, or a functor, note that it must return a std::shared_ptr<ScreenBase> and must have no arguments
		 * @param [in] id the id of the screen you want to create if you use SID_BUILTIN_ANY that id will be assigned for you. note that it is discouraged to mix SID_BUILTIN_ANY with custom SID's
		 * @note SID_BUILTIN_ANY will always use the next available slot!
		 * @param [in] lazy set this to true if you want your screen to be lazily initialized, the screen will stay unallocated until it is finally needed (this may cut down on initial load-times but increase the load-times during the game)
		 * @return if lazy was true: returns a proxy with the value SCREEN_NOT_IN_REGISTRY, if your id was SID_BUILTIN_ANY you can get the id with @code proxy.get_lazy_id() @endcode \n
		 *			if lazy was false: returns a proxy with the value of your screen, if your id was SID_BUILTIN_ANY you can get the id with @code proxy.screen.get_id() @endcode \n
		 *			if the Engine was unable to create your screen the return value will be INVALID_SCREEN
		 * @note in DEBUG this function will `assert()` if you try to add a screen that was already added, in RELEASE it will emit undefined behaviour
		 */
		screen_ptr_proxy addScreen(const screen_surrogate_ctor_t& surrogate, screen_id_t id = SID_BUILTIN_ANY, bool lazy = false);

		/**@brief adds a Screen to the Application, traditionally you would call this in your make_screens() function but in theory nothing keeps you from creating more screens afterwards. \n
		 * this is not thread-safe! you may call it from everywhere, but you need to find your own method of preventing bad insertions
		 * @param [in] screen a std::shared_ptr<ScreenBase> for your screen
		 * @param [in] id the id of the screen you want to create if you use SID_BUILTIN_ANY that id will be assigned for you. note that it is discouraged to mix SID_BUILTIN_ANY with custom SID's
		 * @return if the Engine was unable to create your screen this will return SID_BUILTIN_ERROR otherwise this will return the id of your screen
		 * @note it is impossible to lazily create screens like this as you already created them!
		 * @note SID_BUILTIN_ANY will always use the next available slot!
		 * @note in DEBUG this function will `assert()` if you try to add a screen that was already added, in RELEASE it will emit undefined behaviour
		 */
		screen_id_t addScreen(std::shared_ptr<ScreenBase> screen, screen_id_t id = SID_BUILTIN_ANY);

		/**@brief this marks a screen active
		 * You may pass a screen, a screen-id, or a screen-proxy to this function
		 * @note only call this function on one screen! To transition between screens use the exit() function of your screen.
		 * @{ */
		void makeScreenActive(screen_ptr_proxy);
		void makeScreenActive(screen_smrt_t);
		void makeScreenActive(screen_id_t);
		/**@}*/



	private:

		enum INTERNAL_STATE
		{
			NOT_INITIALIZED,
			INITIALIZED,
			APPLICATION_RUNNING,
			SWITCHING_SCREENS,
			ENDING,
			ENDED
		} m_state = NOT_INITIALIZED;

		screen_id_t nextScreen = SID_BUILTIN_ERROR;

		friend class threading::Scheduler;

		void manage();

		template <class T>
		screen_id_t find_lowest_available_key(const std::map<uint32_t,T>& map) const;


		//Immediate Loading
		template <class T>
		void make_screen_instance(screen_id_t);

		//Lazy Loading
		template <class T>
		void prepare_screen_instance(screen_id_t);

		std::map<screen_id_t, std::shared_ptr<ScreenBase>> m_screens;
		std::map<screen_id_t, screen_surrogate_ctor_t> m_preparedScreens;

		std::shared_ptr<ScreenBase> get_screen(screen_id_t,bool lazy_init = true);

		std::shared_ptr<ScreenBase> m_activeScreen = INVALID_SCREEN;

		mutable std::mutex m_screenMutex;

		std::vector<char*> m_args;

		//system members

		pixl::core::Window m_window;

	};

	template <class T>
	screen_id_t MainGame::find_lowest_available_key(const std::map<uint32_t, T>& map) const
	{
		uint32_t i = 0;
		for (auto it = map.cbegin(), end = map.cend();
			it != end && i == it->first; ++it, ++i)
		{		
		}
		return i;

	}

	template <class T>
	void MainGame::make_screen_instance(screen_id_t screen_id)
	{
		assert(m_screens.find(screen_id) == m_screens.end()); // screen already exists!
		assert(m_preparedScreens.find(screen_id) == m_preparedScreens.end()); // constructor for screen already exists!
		m_screens.emplace(screen_id, std::make_shared<T>());
	}

	template <class T>
	void MainGame::prepare_screen_instance(screen_id_t screen_id)
	{
		assert(m_screens.find(screen_id) == m_screens.end()); // screen already exists!
		assert(m_preparedScreens.find(screen_id) == m_preparedScreens.end()); // constructor for screen already exists!
		m_preparedScreens.emplace(
			screen_id,
			screen_surrogate_ctor_t::create([] //aliasing away all our problems
			{
				return std::make_shared<T>();
			})
		);
	}
	
}
