#pragma once
#include <common.hpp>
#include <map>
#include <GLFW/glfw3.h>
#include <mutex>
#include "core/window.hpp"
#include <set>

namespace pixl::game
{

	namespace keys {
		//old fashioned way it is then
		enum EKeys : int {
			UNKNOWN = GLFW_KEY_UNKNOWN,
			SPACE = GLFW_KEY_SPACE,
			APOSTOPHE = GLFW_KEY_APOSTROPHE,
			COMMA = GLFW_KEY_COMMA,
			MINUS = GLFW_KEY_MINUS,
			PERIOD = GLFW_KEY_PERIOD,
			SLASH = GLFW_KEY_SLASH,
			N0 = GLFW_KEY_0,
			N1 = GLFW_KEY_1,
			N2 = GLFW_KEY_2,
			N3 = GLFW_KEY_3,
			N4 = GLFW_KEY_4,
			N5 = GLFW_KEY_5,
			N6 = GLFW_KEY_6,
			N7 = GLFW_KEY_7,
			N8 = GLFW_KEY_8,
			N9 = GLFW_KEY_9,
			SEMICOLON = GLFW_KEY_SEMICOLON,
			EQUAL = GLFW_KEY_EQUAL,
			A = GLFW_KEY_A,
			B = GLFW_KEY_B,
			C = GLFW_KEY_C,
			D = GLFW_KEY_D,
			E = GLFW_KEY_E,
			F = GLFW_KEY_F,
			G = GLFW_KEY_G,
			H = GLFW_KEY_H,
			I = GLFW_KEY_I,
			J = GLFW_KEY_J,
			K = GLFW_KEY_K,
			L = GLFW_KEY_L,
			M = GLFW_KEY_M,
			N = GLFW_KEY_N,
			O = GLFW_KEY_O,
			P = GLFW_KEY_P,
			Q = GLFW_KEY_Q,
			R = GLFW_KEY_R,
			S = GLFW_KEY_S,
			T = GLFW_KEY_T,
			U = GLFW_KEY_U,
			V = GLFW_KEY_V,
			W = GLFW_KEY_W,
			X = GLFW_KEY_X,
			Y = GLFW_KEY_Y,
			Z = GLFW_KEY_Z,
			LEFT_BRACKET = GLFW_KEY_LEFT_BRACKET,
			BACKSLASH = GLFW_KEY_BACKSLASH,
			RIGHT_BRACKET = GLFW_KEY_RIGHT_BRACKET,
			GRAVE_ACCENT = GLFW_KEY_GRAVE_ACCENT,
			WORLD1 = GLFW_KEY_WORLD_1,
			WORLD2 = GLFW_KEY_WORLD_2,
			ESCAPE = GLFW_KEY_ESCAPE,
			ENTER = GLFW_KEY_ENTER,
			TAB = GLFW_KEY_TAB,
			BACKSPACE = GLFW_KEY_BACKSPACE,
			INSERT = GLFW_KEY_INSERT,
			KDELETE = GLFW_KEY_DELETE, //NOTE:(algo-ryth-mix) DELETE is defined by winnt.h can't circumvent defined as we need DELETE to be accessible afterwards as well ... meh

			RIGHT = GLFW_KEY_RIGHT,
			LEFT = GLFW_KEY_LEFT,
			DOWN = GLFW_KEY_DOWN,
			UP = GLFW_KEY_UP,
			PAGE_UP = GLFW_KEY_PAGE_UP,
			PAGE_DOWN = GLFW_KEY_PAGE_DOWN,
			HOME = GLFW_KEY_HOME, // HOME ?
			END = GLFW_KEY_END,
			CAPS_LOCK = GLFW_KEY_CAPS_LOCK,
			SCROLL_LOCK = GLFW_KEY_SCROLL_LOCK,
			PRINT_SCREEN = GLFW_KEY_PRINT_SCREEN,
			PAUSE = GLFW_KEY_PAUSE,
			F1 = GLFW_KEY_F1,
			F2 = GLFW_KEY_F2,
			F3 = GLFW_KEY_F3,
			F4 = GLFW_KEY_F4,
			F5 = GLFW_KEY_F5,
			F6 = GLFW_KEY_F6,
			F7 = GLFW_KEY_F7,
			F8 = GLFW_KEY_F8,
			F9 = GLFW_KEY_F9,
			F10 = GLFW_KEY_F10,
			F11 = GLFW_KEY_F11,
			F12 = GLFW_KEY_F12,
			F13 = GLFW_KEY_F13,
			F14 = GLFW_KEY_F14,
			F15 = GLFW_KEY_F15,
			F16 = GLFW_KEY_F16,
			F17 = GLFW_KEY_F17,
			F18 = GLFW_KEY_F18,
			F19 = GLFW_KEY_F19,
			F20 = GLFW_KEY_F20,
			F21 = GLFW_KEY_F21,
			F22 = GLFW_KEY_F22,
			F23 = GLFW_KEY_F23,
			F24 = GLFW_KEY_F24,
			F25 = GLFW_KEY_F25,
			NUMPAD0 = GLFW_KEY_KP_0,
			NUMPAD1 = GLFW_KEY_KP_1,
			NUMPAD2 = GLFW_KEY_KP_2,
			NUMPAD3 = GLFW_KEY_KP_3,
			NUMPAD4 = GLFW_KEY_KP_4,
			NUMPAD5 = GLFW_KEY_KP_5,
			NUMPAD6 = GLFW_KEY_KP_6,
			NUMPAD7 = GLFW_KEY_KP_7,
			NUMPAD8 = GLFW_KEY_KP_8,
			NUMPAD9 = GLFW_KEY_KP_9,
			NUMPAD_DECIMAL = GLFW_KEY_KP_DECIMAL,
			NUMPAD_DIVIDE = GLFW_KEY_KP_DIVIDE,
			NUMPAD_MULTIPLY = GLFW_KEY_KP_MULTIPLY,
			NUMPAD_SUBTRACT = GLFW_KEY_KP_SUBTRACT,
			NUMPAD_ADD = GLFW_KEY_KP_ADD,
			NUMPAD_ENTER = GLFW_KEY_KP_ENTER,
			NUMPAD_EQUAL = GLFW_KEY_KP_EQUAL,
			LEFT_SHIFT = GLFW_KEY_LEFT_SHIFT,
			LEFT_CTRL = GLFW_KEY_LEFT_CONTROL,
			LEFT_ALT = GLFW_KEY_LEFT_ALT,
			LEFT_SUPER = GLFW_KEY_LEFT_SUPER,
			RIGHT_SHIFT = GLFW_KEY_RIGHT_SHIFT,
			RIGHT_CTRL = GLFW_KEY_RIGHT_CONTROL,
			RIGHT_ALT = GLFW_KEY_RIGHT_ALT,
			RIGHT_SUPER = GLFW_KEY_RIGHT_SUPER,
			MENU = GLFW_KEY_MENU,
			LAST = MENU
		};
	}



	//TODO:(feels kinda sluggish?)
	class InputManager
	{

	public:

		static inline GLFWwindow* wnd;


		static void pressKey(int32_t keycode)
		{
			std::unique_lock<std::mutex> lock(key_map_lock);
			assert(m_now_pressed_keys);
			(*m_now_pressed_keys)[keycode] = true;
		}

		static void releaseKey(int32_t keycode)
		{
			std::unique_lock<std::mutex> lock(key_map_lock);
			assert(m_now_released_keys);
			(*m_now_released_keys)[keycode] = true;
		}

		template<class Map,class T>
		static bool key_exists_and_gt0(const Map& m,const T& key)
		{
			const auto i = m.find(key);
			if (i == m.end()) return false;
			if (i->second) return true;
			return false;
		}

		template <class Set,class T>
		static bool key_exists(const Set& s,const T& key)
		{
			return (s.find(key) != s.end());
		}

	

		static void update()
		{
			std::unique_lock<std::mutex> lock(key_map_lock);
			for (auto& [key, value] : *m_sticky_pressed)
			{
				if ((*m_sticky_pressed)[key] != 0)
				{
					(*m_sticky_pressed)[key]--;
				}
			}
			for (auto& [key, value] : *m_sticky_released)
			{
				if ((*m_sticky_released)[key] != 0)
				{
					(*m_sticky_released)[key]--;
				}
			}
			for (auto& [key, value] : (*m_now_pressed_keys))
			{
				if (value) {
					if ((m_previously_pressed_keys)->find(key) == m_previously_pressed_keys->end())
					{

						(*m_sticky_pressed)[key] = 3;
					}
				}
				else
				{
					if ((m_previously_pressed_keys)->find(key) != m_previously_pressed_keys->end())
					{

						(*m_sticky_released)[key] = 3;
					}
				}
			}
			m_previously_pressed_keys->clear();
			for (auto& [key, value] : (*m_now_pressed_keys))
			{

				std::cout << key << " " << value << std::endl;
				if (value)
					(*m_previously_pressed_keys)[key] = value;
			}


			for (auto& [key, value] : (*m_now_released_keys))
			{
				(*m_now_pressed_keys)[key] = false;
			}
			m_now_released_keys->clear();
		}

		static bool keyDown(int keycode)
		{
			
			const auto i = m_now_pressed_keys->find(keycode);
			if (i != m_now_pressed_keys->end()) return i->second;
			return false;
			
		}

		static bool keyUp(int keycode)
		{
			return !keyDown(keycode);
		}

		static bool keyPressed(int keycode)
		{
			const auto i = m_sticky_pressed->find(keycode);
			if (i == m_sticky_pressed->end()) return false;

			if (i->second == 0) return false;
			(*m_sticky_pressed)[keycode] = 0;
			return true;
		}

		static bool keyReleased(int keycode)
		{
			const auto i = m_sticky_released->find(keycode);
			if (i == m_sticky_released->end()) return false;

			if (i->second == 0) return false;
			(*m_sticky_released)[keycode] = 0;
			return true;
		}

		static core::cb_create<core::KEY>::type make_key_callback()
		{
			return core::cb_create<core::KEY>::type::create([&](int key, int scan, core::window_input::KeyButtonMode key_mode, core::window_input::ModifierKeys)
				{
					if (key_mode == core::PRESSED)
					{
						pressKey(key);
					}
					else if (key_mode == core::RELEASED)
					{
						releaseKey(key);
					}
				});
		}

		void static __internal_pixl_free_maps()
		{
			//MSVC being MSVC ...seems to be a bug in the at-exit handler of std::map for inline static
			//or maybe a thread is overly possessive
			//TODO(algo-ryth-mix): FIX-THIS! [potential_memory_leak(2 un-freed maps of (int,bool))]
			//https://developercommunity.visualstudio.com/content/problem/339685/inline-static-stdmap-member-throws-exception-in-de.html
			//fix has been issued newer versions of the compiler "should" not be affected
#if !defined(_MSC_VER) && _MSC_FULL_VER == 192030324
			delete m_now_pressed_keys;
			delete m_previously_pressed_keys;
			delete m_now_released_keys;
			delete m_sticky_pressed;
			delete m_sticky_released;
#endif
		}


	private:

		static std::map<int32_t, bool>* m_now_pressed_keys;
		static std::map<int32_t, bool>* m_previously_pressed_keys;
		static std::map<int32_t, bool>* m_now_released_keys;
		static std::map<int32_t, int>* m_sticky_pressed;
		static std::map<int32_t, int>* m_sticky_released;

		static std::mutex key_map_lock;

	};
}
//HACK:(algorythmix)
//custom at-exist destructor for maps
inline static bool atexit_control = [&]() -> bool
{
	std::atexit([]
		{
			pixl::game::InputManager::__internal_pixl_free_maps();
		});
	std::at_quick_exit([]
		{
			pixl::game::InputManager::__internal_pixl_free_maps();
		});
	return true;
}();