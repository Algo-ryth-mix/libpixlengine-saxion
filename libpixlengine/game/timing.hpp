#pragma once
#include <chrono>
#include <mutex>

namespace pixl::game
{
	class Time
	{
	public:

		/** @brief returns the deltaTime of this Frame
		 *	usually you would not want to call this yourself as the Screen already acquires the Delta for your
		 *	and calling this manually breaks the acquisition of this frame
		 *	@returns the delta time in seconds
		 */
		static float getDelta()
		{
			using namespace std::chrono;
			using p_seconds = duration<float>;
			using clock = steady_clock;

			static thread_local auto last = clock::now();
			const auto now = clock::now();

			const float delta = duration_cast<p_seconds>(now - last).count();

			last = now;
			m_sLastDelta = delta;
			return delta;
		}

		/** @brief sets the target Frames per second on this thread
		 *  @param [in] FPS the Frames per second you need 
		 */
		static void setTargetFps(float FPS)
		{
			m_sTargetFps = FPS;
		}


		/** @brief gets the actual FPS based on Delta
		 */
		static float getFps()
		{
			return 1.0f / m_sLastDelta;
		}


		/** @brief starts a new frame for wait-acquisition
		 */
		static auto acquisitionFpsTarget()
		{
			using namespace std::chrono;
			return steady_clock::now();
		}

		/** @brief finishes a frame of wait-acquisition, by waiting the required amount,
		 *  @note if your frame-time was longer than the required time, this will not limit anything
		 *  @param [in] acquisition the acquisition made with acquisitionFpsTarget 
		 *  @param [in] bias the bias in wait to remove any overhead in your measurement (typically around 0.7)
		 *  @pre acquisitionFpsTarget
		 */
		static void waitFpsTarget(std::chrono::steady_clock::time_point acquisition,double bias = 0.7)
		{
			//if the target is 0 then we do nothing ... this indicates that no Target has been set
			if (m_sTargetFps == 0) return;
			
			using namespace std::chrono;


			//acquire actual frame-time and desired frame-time
			const auto frameTime = steady_clock::now() - acquisition;
			const milliseconds frameTarget(static_cast<unsigned long>(1.0f / m_sTargetFps * 1000.0f));



			//if the current frame time is bigger then the target-frame time do NOT limit the frame rate
			// additional calls to _Thrd_sleep should always be avoided as they will ALWAYS consume frame time (even when the target is 0)
			//
			if (frameTime >= frameTarget) return;


			//Limit the current frame time
			//this will (in theory) free up the processor to do other things
			//i.e.: notify the Operating System to do other things while waiting for the rest of the duration
			//this can (in theory) drastically improve game performance by actively telling 
			//the OS when to do things we don't care about (downloading updates or whatever)
			//this needs to be slightly biased (default 0.7) to account for other things that are not captured by the acquisition
			std::this_thread::sleep_for((frameTarget - frameTime) * bias);
			//TODO(algo-ryth-mix): the above should preferably be done in either Scheduler
			//TODO(continued):     or at least the ScreenBase to avoid having a ridiculously low bias to match the frame times
		} 

	private:
		inline static thread_local float m_sTargetFps = 0;
		inline static thread_local float m_sLastDelta = 0;
	};
}
