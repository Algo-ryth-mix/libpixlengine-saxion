#pragma once
#include "common.hpp"
#include "fast_delegate.hpp"

#include <chrono>
#include <mutex>

namespace pixl::core {

#if defined(__linux__)

	//special sequences to color the tty
	const char * const _fg_red_v("\033[0;31m");
	const char * const _fg_green_v("\033[1;32m");
	const char * const _fg_yellow_v("\033[1;33m");
	const char * const _fg_cyan_v("\033[0;36m");
	const char * const _fg_magenta_v("\033[0;35m");
	const char * const _fg_default_v("\033[0m");


	//push to stdout
	inline void _fg_red()		{ printf(_fg_red_v); }
	inline void _fg_green()		{ printf(_fg_green_v); }
	inline void _fg_yellow()	{ printf(_fg_yellow_v); }
	inline void _fg_cyan()		{ printf(_fg_cyan_v); }
	inline void _fg_magenta()	{ printf(_fg_magenta_v); }
	inline void _fg_default()	{ printf(_fg_default_v); }


	//bit of a trick to mask msvcs __forceinline (__attribute__((always_inline)) gives a warning and it isn't that important that its inlined anyway)
	#define __forceinline inline


#endif
#if defined(_MSC_VER)

//_CRT_SECURE_NO_WARNINGS_ is just a nuisance
#pragma warning(disable:4996)
	namespace detail {

		//we need the defualt console
		inline HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
	}

	// color the console with some butt-ugly windows calls
	inline void _fg_red() { SetConsoleTextAttribute(detail::console,FOREGROUND_RED | FOREGROUND_INTENSITY); }
	inline void _fg_green() { SetConsoleTextAttribute(detail::console, FOREGROUND_GREEN | FOREGROUND_INTENSITY); }
	inline void _fg_yellow() { SetConsoleTextAttribute(detail::console, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY); }
	inline void _fg_cyan() { SetConsoleTextAttribute(detail::console, FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY); }
	inline void _fg_magenta() { SetConsoleTextAttribute(detail::console, FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY);; }
	inline void _fg_default() { SetConsoleTextAttribute(detail::console, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_BLUE); }

#endif

	namespace detail {
	inline static std::mutex _stdout_mtx;
	}

	//print error message
	inline void vprint_error(const char * format,fmt::format_args args)
	{

		//get time-point
		auto tp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		const tm local_tm = *localtime(&tp);
		
		//color red
		_fg_red();

		//print preamble
		fmt::print(stderr,"[ ERR  | {:02}-{:02} ] ", local_tm.tm_min, local_tm.tm_sec);
		
		//color white
		_fg_default();

		//print message
		fmt::vprint(stderr,format, args);
	}

	/**@brief prints an error message
	 * @note formatted like "[ ERR  | mm-ss ] your message"
	 * @note this function makes sure that the output to stdout is locked before printing
	 * @param [in] format the format of the string, usage like fmt
	 * @param [in] args the (optional) options to the given format string 
	 */
	template <typename ... Args>
	void print_error(const char * format, const Args& ... args)
	{
		//lock stdout
		std::unique_lock<std::mutex> lock(detail::_stdout_mtx);
		
		//print error with args
		vprint_error(format, fmt::make_format_args(args...));
	}

	/**@brief prints an error message and enters a new line
	 * @note formatted like "[ ERR  | mm-ss ] your message \n"
	 * @note this function makes sure that the output to stdout is locked before printing
	 * @param [in] format the format of the string, usage like fmt
	 * @param [in] args the (optional) options to the given format string
	 */
	template <typename ... Args>
	void println_error(const char * format, const Args& ... args)
	{
		std::unique_lock<std::mutex> lock(detail::_stdout_mtx);
		vprint_error(format, fmt::make_format_args(args...));

		//also print a newline
		fmt::print("\n");
	}


	inline void vprint_warning(const char * format, fmt::format_args args)
	{
		auto tp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		const tm local_tm = *localtime(&tp);
	
		//same as above but warning and therefore with yellow
		_fg_yellow();
		fmt::print(stderr,"[ WARN | {:02}-{:02} ] ", local_tm.tm_min, local_tm.tm_sec);
		_fg_default();
		fmt::vprint(stderr,format, args);
	}


	/**@brief prints an warning message
	 * @note formatted like "[ WARN | mm-ss ] your message"
	 * @note this function makes sure that the output to stdout is locked before printing
	 * @param [in] format the format of the string, usage like fmt
	 * @param [in] args the (optional) options to the given format string
	 */
	template <typename ... Args>
	void print_warning(const char * format, const Args& ... args)
	{
		std::unique_lock<std::mutex> lock(detail::_stdout_mtx);
		vprint_warning(format, fmt::make_format_args(args...));
	}

	/**@brief prints an warning message and enters a new line
	 * @note formatted like "[ WARN | mm-ss ] your message \n"
	 * @note this function makes sure that the output to stdout is locked before printing
	 * @param [in] format the format of the string, usage like fmt
	 * @param [in] args the (optional) options to the given format string
	 */
	template <typename ... Args>
	void println_warning(const char * format, const Args& ... args)
	{
		std::unique_lock<std::mutex> lock(detail::_stdout_mtx);
		vprint_warning(format, fmt::make_format_args(args...));
		fmt::print("\n");
	}


	inline void vprint_debug(const char * format, fmt::format_args args)
	{
		auto tp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		const tm local_tm = *localtime(&tp);
		_fg_cyan();
		fmt::print("[ INFO | {:02}-{:02} ] ", local_tm.tm_min, local_tm.tm_sec);
		_fg_default();
		fmt::vprint(format, args);
	}

	/**@brief prints an debug message
	 * @note formatted like "[ INFO | mm-ss ] your message"
	 * @note this function makes sure that the output to stdout is locked before printing
	 * @param [in] format the format of the string, usage like fmt
	 * @param [in] args the (optional) options to the given format string
	 */
	template <typename ... Args>
	void print_debug(const char * format, const Args& ... args)
	{
		std::unique_lock<std::mutex> lock(detail::_stdout_mtx);
		vprint_debug(format, fmt::make_format_args(args...));
	}

	/**@brief prints an debug message and enters a new line
	 * @note formatted like "[ INFO | mm-ss ] your message \n"
	 * @note this function makes sure that the output to stdout is locked before printing
	 * @param [in] format the format of the string, usage like fmt
	 * @param [in] args the (optional) options to the given format string
	 */
	template <typename ... Args>
	void println_debug(const char * format, const Args& ... args)
	{
		std::unique_lock<std::mutex> lock(detail::_stdout_mtx);
		vprint_debug(format, fmt::make_format_args(args...));
		fmt::print("\n");
	}


	/**@brief breaks program execution and falls into the debugger \n
	 * on msvc this will trigger __debug_break \n
	 * on linux this will try to trigger SIGTRAP and fallback to SIGABRT 
	 */
	__forceinline void trap()
	{
		ptrap();
	}


	/**@brief breaks program execution and falls into the debugger if x is true \n
	 * on msvc this will trigger __debug_break \n
	 * on linux this will try to trigger SIGTRAP and fallback to SIGABRT
	 * @param [in] x the condition
	 */
	__forceinline void trap_if(bool x)
	{
		if (x) ptrap();
	}

	/**@brief breaks program execution and falls into the debugger if f() is true \n
	 * on msvc this will trigger __debug_break \n
	 * on linux this will try to trigger SIGTRAP and fallback to SIGABRT
	 * @param [in] f the function pointer that will be evaluated
	 */
	__forceinline void trap_if(bool(*f)())
	{
		if (f()) ptrap();
	}
	/**@brief breaks program execution and falls into the debugger if f() is true \n
	 * on msvc this will trigger __debug_break \n
	 * on linux this will try to trigger SIGTRAP and fallback to SIGABRT
	 * @param [in] f the function delegate that will be evaluated
	 */
	__forceinline void trap_if(const sa::delegate<bool()>& f)
	{
		if (f()) ptrap();
	}
}
#ifdef __linux__
#undef __forceinline
#endif
