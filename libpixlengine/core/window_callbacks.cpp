#include "core/window.hpp"

using namespace pixl::core;

namespace pixl::detail {
	Window * G_wnd;

	class window_callback_impl
	{

		static void GLFWErrorFun(int code,const char * msg)
		{
			G_wnd->m_lastGLFWError = std::make_pair(code,msg);
			G_wnd->notify(Window::PGLFW_ERROR_RECEIVED);
		}

		static void GLFWWindowPosFun(GLFWwindow*,int x,int y)
		{
			G_wnd->m_windowPos.x = x;
			G_wnd->m_windowPos.y = y;
			G_wnd->notify(Window::PGLFW_WINDOW_POS_CHANGED);
		}

		static void GLFWWindowSizeFun(GLFWwindow*,int w,int h)
		{
			G_wnd->m_windowSize.w = static_cast<std::size_t>(w);
			G_wnd->m_windowSize.h = static_cast<std::size_t>(h);
			G_wnd->notify(Window::PGLFW_WINDOW_SIZE_CHANGED);
		}

		static void GLFWWindowCloseFun(GLFWwindow*)
		{
			G_wnd->notify(Window::PGLFW_WINDOW_CLOSED);
		}

		static void GLFWWindowFocusFun(GLFWwindow*,int attr)
		{
			G_wnd->m_hasFocus = attr == GLFW_TRUE;
			G_wnd->notify(Window::PGLFW_WINDOW_FOCUS);
		}
		static void GLFWWindowIconifyFun(GLFWwindow*,int attr)
		{
			G_wnd->m_iconified = attr == GLFW_TRUE;
			G_wnd->notify(Window::PGLFW_WINDOW_FOCUS);
		}
		static void GLFWFramebufferSizeFun(GLFWwindow*,int x,int y)
		{
			G_wnd->m_multicallFBSIZE({x,y});
		}

		static void GLFWMouseButtonFun(GLFWwindow*,int btn,int action,int mods)
		{
			G_wnd->m_multicallMBUTTON
			(
				window_input::KeyButtonMode(action == GLFW_PRESS),
				btn == GLFW_MOUSE_BUTTON_LEFT ? window_input::LMB : btn ==  GLFW_MOUSE_BUTTON_RIGHT ? window_input::RMB : window_input::MMB,
				window_input::ModifierKeys(mods)
			);
		}

		static void GLFWCursorPosFun(GLFWwindow*,double x,double y)
		{
			G_wnd->m_multicallMCURSOR({x,y});
		}

		static void GLFWCursorEnterFun(GLFWwindow*,int action)
		{
			G_wnd->notify(Window::PGLFW_CURSOR_ENTER_LEAVE);
			G_wnd->m_hasCursor = (action == GLFW_TRUE);
		}
		static void GLFWScrollFun(GLFWwindow* wnd,double x,double y)
		{
			G_wnd->m_multicallMSCROLL({x,y});
		}

		static void GLFWKeyFun(GLFWwindow* ,int key,int scan,int action,int mods)
		{
			G_wnd->m_multicallKEY
			(
				key,
				scan,
				window_input::KeyButtonMode(action != GLFW_RELEASE),
				window_input::ModifierKeys(mods)
			);
		}

		static void GLFWCharFun(GLFWwindow*,unsigned int code)
		{
			G_wnd->m_multicallCHAR(code);
		}

		static void GLFWCharModsFun(GLFWwindow*,unsigned int code ,int mods)
		{
	
			G_wnd->m_multicallCHARMOD(code, window_input::ModifierKeys(mods));
			
		}

		static void GLFWDropFun(GLFWwindow*,int size,const char** ptr)
		{
			G_wnd->m_droppedFiles = std::vector<std::string>(ptr,ptr+static_cast<std::size_t>(size));
			G_wnd->notify(Window::PGLFW_DROP_TARGET_EVENT);
		}

		static void GLFWJoyStickFun(int no,int action)
		{
			G_wnd->notify(Window::PGLFW_JOYSTICK_CONNECTION_EVENT);
		}
	public:
		static void assign(Window * w)
		{
			G_wnd = w;
			const auto ptr = w->m_windowPtr.get();

			//create mapping 
			glfwSetErrorCallback(				&window_callback_impl::GLFWErrorFun);
			glfwSetWindowPosCallback(ptr,		&window_callback_impl::GLFWWindowPosFun);
			glfwSetWindowSizeCallback(ptr,		&window_callback_impl::GLFWWindowSizeFun);
			glfwSetWindowCloseCallback(ptr,		&window_callback_impl::GLFWWindowCloseFun);
			glfwSetWindowFocusCallback(ptr,		&window_callback_impl::GLFWWindowFocusFun);
			glfwSetFramebufferSizeCallback(ptr, &window_callback_impl::GLFWFramebufferSizeFun);
			glfwSetMouseButtonCallback(ptr,		&window_callback_impl::GLFWMouseButtonFun);
			glfwSetCursorPosCallback(ptr,		&window_callback_impl::GLFWCursorPosFun);
			glfwSetCursorEnterCallback(ptr,		&window_callback_impl::GLFWCursorEnterFun);
			glfwSetScrollCallback(ptr,			&window_callback_impl::GLFWScrollFun);
			glfwSetKeyCallback(ptr,				&window_callback_impl::GLFWKeyFun);
			glfwSetCharCallback(ptr,			&window_callback_impl::GLFWCharFun);
			glfwSetCharModsCallback(ptr,		&window_callback_impl::GLFWCharModsFun);
			glfwSetDropCallback(ptr,			&window_callback_impl::GLFWDropFun);
			glfwSetJoystickCallback(			&window_callback_impl::GLFWJoyStickFun);
		}

	};
}

namespace pixl::core
{

	void Window::createCallbacks()
	{
		::pixl::detail::window_callback_impl::assign(this);
		notify(PGLFW_CALLBACKS_CREATED);
	}

	void Window::destroyCallbacks() const
	{
		/*compat ... this maybe really useful in the future ... I dunno I though having it symmetric might be nice*/
	}

}