#pragma once
#include <mutex>
#include <atomic>

namespace pixl::core {
	class spin_mutex
	{
		std::mutex m_mutex;
		std::atomic<long long> m_watermark;
	public:
		void lock();
		void unlock();
	};


	inline void spin_mutex::lock()
	{
		using clock_t = std::chrono::high_resolution_clock;
		auto before = clock_t::now();
		long long measured(0);

		while(!m_mutex.try_lock()) {
			measured = (clock_t::now() - before).count();
			if(measured >= m_watermark * 2) {
				m_mutex.lock();
				break;
			}
		}
		m_watermark += (measured -m_watermark) / 8;
	}

	
}
