#pragma once
#include <iostream>
#include <vector>
#include <bitset>
#include <array>
#include <string>

#ifdef __linux__ 
#include <string.h> //memcpy ,memset etc...
#endif



//msdn code ... adopted to run everywhere
#ifdef _MSC_VER
#include <intrin.h>
#else
static inline void cpuid(int *eax, int *ebx, int *ecx, int *edx)
{
#if defined(__x86_64__)
    asm("cpuid"
            : "=a" (*eax),
            "=b" (*ebx),
            "=c" (*ecx),
            "=d" (*edx)
            : "0" (*eax), "2" (*ecx));

#else
    /*on 32bit, ebx can NOT be used as PIC code*/
    asm volatile ("xchgl %%ebx, %1; cpuid; xchgl %%ebx, %1"
            : "=a" (*eax), "=r" (*ebx), "=c" (*ecx), "=d" (*edx)
            : "0" (*eax), "2" (*ecx));
#endif
}

static inline void __cpuid(int a[4], int b)
{
    a[0] = b;
    a[2] = 0;
    cpuid(&a[0], &a[1], &a[2], &a[3]);
}

static inline void __cpuidex(int a[4], int b, int c)
{
    a[0] = b;
    a[2] = c;
    cpuid(&a[0], &a[1], &a[2], &a[3]);
}
#endif

namespace pixl::core::isa {

	class InstructionSet
	{
	public:

	    // getters
	    static std::string Vendor();
	    static std::string Brand();

	    static bool SSE3();
	    static bool PCLMULQDQ();
	    static bool MONITOR();
	    static bool SSSE3();
	    static bool FMA();
	    static bool CMPXCHG16B();
	    static bool SSE41();
	    static bool SSE42();
	    static bool MOVBE();
	    static bool POPCNT();
	    static bool AES();
	    static bool XSAVE();
	    static bool OSXSAVE();
	    static bool AVX();
	    static bool F16C();
	    static bool RDRAND();

	    static bool MSR();
	    static bool CX8();
	    static bool SEP();
	    static bool CMOV();
	    static bool CLFSH();
	    static bool MMX();
	    static bool FXSR();
	    static bool SSE();
	    static bool SSE2();

	    static bool FSGSBASE();
	    static bool BMI1();
	    static bool HLE();
	    static bool AVX2();
	    static bool BMI2();
	    static bool ERMS();
	    static bool INVPCID();
	    static bool RTM();
	    static bool AVX512F();
	    static bool RDSEED();
	    static bool ADX();
	    static bool AVX512PF();
	    static bool AVX512ER();
	    static bool AVX512CD();
	    static bool SHA();

	    static bool PREFETCHWT1();

	    static bool LAHF();
	    static bool LZCNT();
	    static bool ABM();
	    static bool SSE4a();
	    static bool XOP();
	    static bool TBM();

	    static bool SYSCALL();
	    static bool MMXEXT();
	    static bool RDTSCP();
	    static bool _3DNOWEXT();
	    static bool _3DNOW();

		// Print out supported instruction set extensions
	    static void print_support();

	};
}