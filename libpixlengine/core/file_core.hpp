#pragma once
#include "common.hpp"
#include <utility>
#include <string>
#include <cassert>
#include <iostream>

#ifdef _MSC_VER
	#pragma warning(disable:4996) //get rid of that pesky _CRT_SECURE_NO_WARNINGS nonsense
#endif

namespace pixl::core::io {


	/**@brief Open File in binary mode and write to buffer
	 *
	 * @param [in] path the path of the file to open
	 * @return a vector of bytes with the contents of the file at path
	 */
	PIXL_NODISCARD inline  byte_vec read_file(std::string_view path)
	{
		std::unique_ptr<FILE,decltype(&fclose)> file(
			fopen(std::string(path).c_str(),"r+b"),
			fclose
		);

		assert(file);

		fseek(file.get(),0L,SEEK_END);
		byte_vec container(ftell(file.get()));
		fseek(file.get(),0L,SEEK_SET);

		fread(container.data(),sizeof(byte),container.size(),file.get());


		return container;
	}

	/**@brief Open file in binary mode to write the buffer to it
	 *
	 * @param [in] path the path of the file you want to write to
	 * @param [in] container the buffer you want to write to the file
	 */
	inline void write_file(std::string_view path,const byte_vec& container)
	{
		std::unique_ptr<FILE,decltype(&fclose)> file(
			fopen(std::string(path).c_str(),"wb"),
			fclose
		);

		assert(file);

		fwrite(container.data(),sizeof(byte),container.size(),file.get());

	}


}
