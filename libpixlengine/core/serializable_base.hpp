#pragma once
#include <JSON/json.hpp>
#include "common.hpp"



namespace pixl::core
{

	class serializable_base;

	class serialize_input
	{
	public:
		serialize_input(const nlohmann::json & j) : serializer(j) {}
		serialize_input() = default;

		nlohmann::json serializer;


		/**@brief call this in your deserialization object when you want to deserialize
		 * child objects that also inherit from serializable_base
		 */
		void deserializeChild (serializable_base* child) const;


	};
	class serialize_output
	{
	public:
		nlohmann::json serializer;

		/**@brief call this in your serialization object when you want to serialize
		 * child objects that also inherit from serializable_base
		 */
		void serializeChild(serializable_base* child);
	};

	/**@interface serializer_base
	 * @brief implement this interface to gain access to serialization properties of Serializable Objects
	 * @note you must implement serializeObject and deserializeObject
	 */
	class serializer_base
	{
	public:

		serializer_base() = default;
		serializer_base(const serializer_base& other) = default;
		serializer_base(serializer_base&& other) noexcept = default;
		serializer_base& operator=(const serializer_base& other) = default;
		serializer_base& operator=(serializer_base&& other) noexcept = default;
		virtual ~serializer_base() = default;

		/**@brief this should serialize the object to a given output format
		 * the output format can be whatever you want and must be class internal
		 */
		virtual void serializeObject(serializable_base * s) PIXL_PUREVIRTUAL;
		/**@brief this should deserialize the object from a given input format
		 * this input format can be whatever you want and must be class internal
		 */
		virtual void deserializeObject(serializable_base * s) PIXL_PUREVIRTUAL;

	protected:
		/**@brief use this to call the serialization function and access the serialization output from a Serializable object 
		 */
		static serialize_output s_out(serializable_base * s);
		/**@brief use this to inject serialization data into a Serializable object and call the deserialization function
		 */
		static void s_in(serializable_base * s, const serialize_input& i);
	};


	/**@class serializable_base	
	 * @brief implement this if you want your Class to be serializable
	 */
	class serializable_base
	{
	public:
		serializable_base() = default;
		serializable_base(const serializable_base& other) = default;
		serializable_base(serializable_base&& other) noexcept = default;
		serializable_base& operator=(const serializable_base& other) = default;
		serializable_base& operator=(serializable_base&& other) noexcept = default;
		virtual ~serializable_base() = default;

		PIXL_NODISCARD virtual std::string getName() const PIXL_PUREVIRTUAL;

	protected:
		/**@brief serialize all your important members in here
		 */
		virtual serialize_output serialize() = 0;
		/**@brief deserialize all your important members in here
		 */
		virtual void deserialize(const serialize_input&) PIXL_PUREVIRTUAL;

		friend class serializer_base;
		friend class serialize_input;
		friend class serialize_output;

	};


	inline void serialize_input::deserializeChild(serializable_base* child) const
	{
		child->deserialize(serialize_input(serializer[child->getName()]));
	}

	inline void serialize_output::serializeChild(serializable_base* child)
	{
		serializer[child->getName()] = child->serialize().serializer;
	}


	inline serialize_output serializer_base::s_out(serializable_base* s)
	{
		return s->serialize();
	}

	inline void serializer_base::s_in(serializable_base* s, const serialize_input& i)
	{
		return s->deserialize(i);
	}
}
