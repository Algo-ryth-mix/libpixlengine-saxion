#pragma once
#include "common.hpp"
#include <ostream>
#include <thread>

/**\cond INTERNAL */
namespace std
{
	inline std::string to_string(const ::pixl::byte_vec& _vector,std::size_t max_bytes = 0)
	{
		std::string str = ::pixl::str::empty;
		auto read_limit = max_bytes;
		for (auto && byte : _vector)
		{
			str += fmt::format("{:#04x} ", byte);
			if (max_bytes)
			{
				read_limit--;
				if (!read_limit) return str + " ...";
			}
		}
		return str;
	}
}
/**\endcond */
namespace pixl::core {

	/**@brief Encapsulates a Generic Resource type
	 * this is used all over the engine and behaves like a transparent proxy type for the most common types
	 * If you want your custom type to be convertible from a resource_base make sure to implement
	 * @code static <YourCustomDataType> resource_adapter(resource *) @endcode within your class
	 */
	template <class COT = byte_vec>
	class resource_base
	{
	public:
		using container_type = COT;

		resource_base() :m_container{} {};
		resource_base(const std::string& view);
		resource_base(const char*);
		resource_base(const container_type& c) : m_container{c} {}
		resource_base(container_type&& c) : m_container{std::move(c)} {};

		virtual ~resource_base() =default;

		resource_base(const resource_base& other) = default;
		resource_base(resource_base&& other) noexcept = default;
		resource_base& operator=(const resource_base& other) = default;
		resource_base& operator=(resource_base&& other) noexcept = default;

		resource_base& operator=(const container_type& c) { m_container = container_type{c}; return *this; }
		resource_base& operator=(container_type&& c) { m_container = container_type{std::move(c)}; return *this; }


		/**@brief save the data as a c-string within the buffer
		 * Note that this will append a null-byte to make sure your buffer is terminated properly
		 * 
		 * @param [in] view the string you want to assign to this buffer
		 * @return the resource
		 */
		resource_base& operator=(std::string_view view)
		{
			m_container = container_type{view.begin(),view.end()};
			m_container.resize(m_container.size()+1);
			m_container.at(m_container.size()-1) = NULL;
			return *this;
		}


		/**@brief transparent converter to container_type (typically std::vector<std::uint8_t>)
		 */
		operator container_type () const
		{
			return m_container;
		}

		/**@brief transparent converter to container_type (typically std::vector<std::uint8_t>)
		*/
		operator container_type& ()
		{
			return m_container;
		}


		container_type data() const
		{
			return m_container;
		}

		container_type& data()
		{
			return m_container;
		}

		/**@brief string converter
		 */
		std::string str() const
		{
			return std::string(m_container.begin(),m_container.end());
		}

		/**@brief c-string converter
		 */
		const char * c_str() const
		{
			return reinterpret_cast<const char*>(m_container.data());
		}

		operator std::string () const
		{
			return str();
		}

		explicit operator const char *() const
		{
			return c_str();
		}

		/**@brief "Universal"-converter ... everything that implements a static resource_adapter function is welcome
		 */
		template <class T>
		operator T ()
		{
			return typename T::resource_adapter(this);
		}

		virtual std::string debug_info()
		{
			return fmt::format("resource: {}", std::to_string(m_container,16));
		}

		friend std::ostream& operator<<(std::ostream& os, const resource_base& obj)
		{
			return os << "m_container: " << obj.m_container;
		}

	private:
		container_type m_container;
	};


	template <class COT>
	resource_base<COT>::resource_base(const std::string& str)
	{
		m_container.insert(m_container.begin(),str.begin(),str.end());
	}


	template <class COT>
	resource_base<COT>::resource_base(const char* cstr)
	{
		m_container.insert(m_container.begin(), cstr, cstr + strlen(cstr));
		m_container.push_back(NULL);
	}

	/**@brief convenience override*/
	using resource = resource_base<>;
}
