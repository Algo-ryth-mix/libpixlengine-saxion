#include "core/window.hpp"

namespace pixl::core
{
	Window::Window() : m_hasFocus{false},m_iconified{false},m_hasCursor{false}
	{
	}
	Window::~Window()
	{	
		destroyCallbacks();
	}
	void Window::init(math::vec2<std::size_t> size, const std::string& name)
	{
		m_windowSize = size;
		createWindow(size.w, size.h, name.c_str());
	}

	bool Window::pollEvents()
	{
		glfwPollEvents();
		return !glfwWindowShouldClose(m_windowPtr.get());
	}

	math::vec2<std::size_t> Window::getWindowSize() const
	{
		return m_windowSize;
	}

	math::vec2i Window::getWindowPos() const
	{
		return m_windowPos;
	}

	bool Window::hasFocus() const
	{
		return m_hasFocus;
	}

	bool Window::isIconified() const
	{
		return m_iconified;
	}

	bool Window::hasCursor() const
	{
		return m_hasCursor;
	}

	std::vector<std::string> Window::getDroppedFiles() const
	{
		return m_droppedFiles;
	}


	bool Window::is_fullscreen() const
	{
		//check if we are attached to a monitor
		return glfwGetWindowMonitor(m_windowPtr.get()) != nullptr;
	}

	void Window::set_fullscreen(bool fs) const
	{
		//check if we already are full-screen
		if (is_fullscreen() == fs) return;

		//variables to capture window-position and size
		static math::vec2i windowPos = {0, 0};
		static math::vec2i windowSize = {0, 0};

		if (fs)
		{
			//save the window-position and size
			glfwGetWindowPos(m_windowPtr.get(), windowPos.data, windowPos.data + 1);
			glfwGetWindowSize(m_windowPtr.get(), windowSize.data, windowSize.data + 1);

			//set fullscreen
			const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
			glfwSetWindowMonitor(m_windowPtr.get(), glfwGetPrimaryMonitor(), 0, 0, mode->width, mode->height, 0);
		}

		else
		{
			//set windowed
			glfwSetWindowMonitor(m_windowPtr.get(), nullptr, windowPos.x, windowPos.y, windowSize.w, windowSize.h, 0);
		}
	}

	void Window::createWindow(std::size_t width, std::size_t height, const char* title, GLFWmonitor* mon, GLFWwindow* share)
	{
		glfwInit();
		notify(PGLFW_API_INITIALIZED);


		m_windowPtr = std::shared_ptr<GLFWwindow>(
			glfwCreateWindow(static_cast<int>(width),static_cast<int>(height), title, mon, share),
			[this](GLFWwindow* w)
			{
				glfwDestroyWindow(w);
				notify(PGLFW_WINDOW_DESTROYED);
				glfwTerminate();
				notify(PGLFW_API_TERMINATED);
			});
		notify(PGLFW_WINDOW_CREATED);

		createCallbacks();
	}
}
