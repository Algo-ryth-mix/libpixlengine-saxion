#pragma once

#define GLFW_STATIC
#include "common.hpp"
#include <GLFW/glfw3.h>

#include <memory>
#include <functional>

#include "core/subject.hpp"
#include "math/vector.hpp"

#include "core/fast_delegate.hpp"
#include "core/fast_delegate_mc.hpp"
#include "math/quaternion.hpp"

/**\cond INTERNAL*/
//forward declaration
namespace pixl::detail{
	class window_callback_impl;
}
/**\endcond */
namespace pixl::core
{
	inline namespace window_input
	{
		/**@brief determines if a key or a button is PRESSED or RELEASED
		 * must be used for the callback<CBType::KEY> or callback<CBType::MOUSEBUTTON>
		 */
		enum KeyButtonMode : bool
		{
			PRESSED = true,
			RELEASED = false
		};

		/**@brief determines which MouseButton has been pressed
		 * must be used for the callback<CBType::MOUSEBUTTON>
		 */
		enum MouseButton : uint8_t
		{
			LMB = 0,
			MMB = 2,
			RMB = 1,
		};

		/**@brief determines which ModifierKeys has been pressed
		 * must be used for the callback<CBType::MOUSEBUTTON>, callback<CBType::KEY>, callback<CBType::CHAR_MOD>
		 */
		enum ModifierKeys : uint8_t
		{
			SHIFT = 0x1,
			CTRL = 0x2,
			ALT = 0x4,
			SUPER = 0x8
		};

		using FbSize_delegate = sa::delegate<void(math::vec2i size)>;
		using MButton_delegate = sa::delegate<void(KeyButtonMode press_mode, MouseButton mb, ModifierKeys mods)>;
		using MCursor_delegate = sa::delegate<void(math::vec2d)>;
		using MScroll_delegate = sa::delegate<void(math::vec2d)>;
		using Key_delegate = sa::delegate<void(int keycode, int scanCode, KeyButtonMode pressMode, ModifierKeys mods)>;
		using Char_delegate = sa::delegate<void(uint32_t code)>;
		using CharMod_delegate = sa::delegate<void(uint32_t code, ModifierKeys mods)>;

		/**@brief determines which callback you want to register
		 * e.g.: callback<CBType::MOUSEBUTTON> will get the function where you can register a mousebutton-callback
		 */
		enum CBType
		{
			FRAMEBUFFER,
			MOUSEBUTTON,
			MOUSECURSOR,
			MOUSESCROLL,
			KEY,
			CHAR,
			CHAR_MOD
		};

		template <CBType T>
		struct cb_create
		{
			using type = nullptr_t;
		};

		template<>
		struct cb_create<FRAMEBUFFER>
		{
			using type = FbSize_delegate;
		};
		template<>
		struct cb_create<MOUSEBUTTON>
		{
			using type = MButton_delegate;
		};
		template<>
		struct cb_create<MOUSECURSOR>
		{
			using type = MCursor_delegate;
		};
		template<>
		struct cb_create<MOUSESCROLL>
		{
			using type = MScroll_delegate;
		};
		template<>
		struct cb_create<KEY>
		{
			using type = Key_delegate;
		};
		template<>
		struct cb_create<CHAR>
		{
			using type = Char_delegate;
		};
		template<>
		struct cb_create<CHAR_MOD>
		{
			using type = CharMod_delegate;
		};

	}


	/**@class Window
	 * @brief creates a drawable window independent of platform 
	 * Implements @ref subject_base and will therefore emit events when certain things happen in the window
	 */
	class Window final : public subject_base
	{
	public:
		Window();
		~Window();
		GLFWwindow* getHandle() const { return m_windowPtr.get(); }

		/**@enum event
		 * @brief includes all the events emitted by the window-subject
		 *
		 */
		enum event : event_t
		{
			/**! this gets emitted when... */

			/**! the underlying window api is initialized (GLFW) */
			PGLFW_API_INITIALIZED,
			
			/**! the window actually gets created */
			PGLFW_WINDOW_CREATED,
			
			/**! the underlying window api is destroyed (GLFW)*/
			PGLFW_API_TERMINATED,
			
			/**! the callbacks are registered */
			PGLFW_CALLBACKS_CREATED,
			
			/**! the window is destroyed */
			PGLFW_WINDOW_DESTROYED,
			
			/**! the underlying window api has pushed an error */
			PGLFW_ERROR_RECEIVED,
			
			/**! the window is moved*/
			PGLFW_WINDOW_POS_CHANGED,
			
			/**! the window is resized*/
			PGLFW_WINDOW_SIZE_CHANGED,
			
			/**! the window is closed*/
			PGLFW_WINDOW_CLOSED,
			
			/**! the window gains or looses focus*/
			PGLFW_WINDOW_FOCUS,
			
			/**! the window is minimized*/
			PGLFW_WINDOW_ICONIFIED,
			
			/**! the cursor leaves or enters the window*/
			PGLFW_CURSOR_ENTER_LEAVE,
			
			/**! the window receives a drop event (aka files have been dropped into the window)*/
			PGLFW_DROP_TARGET_EVENT,
			
			/**! a joystick is connected or disconnected*/
			PGLFW_JOYSTICK_CONNECTION_EVENT 
		};

		/**@brief creates the window
		 * @param [in] size the size of the window;
		 * @param [in] name the title of the window;
		 */
		void init(math::vec2<std::size_t> size, const std::string& name);


		/**@brief polls events and updates the window may invokes callbacks 
		 * @returns if the window should close
		 */
		bool pollEvents();

		/**@brief gets the last error emitted from the underlying window api
		 * you can check if the error changed with @ref PGLFW_ERROR_RECEIVED
		 * @returns the error code and the error string
		 */
		std::pair<int, const char*> getLastError() const { return m_lastGLFWError; }

		/**@brief gets the window size
		 * you can check if the size changed with @ref PGLFW_WINDOW_SIZE_CHANGED
		 */
		math::vec2<std::size_t> getWindowSize() const;

		/**@brief gets the window position
		 * you can check if the size changed with @ref PGLFW_WINDOW_POS_CHANGED
		 */
		math::vec2i getWindowPos() const;

		/**@brief checks if the window has focus or not
		 * you can check if the size changed with @ref PGLFW_WINDOW_FOCUS
		 */
		bool hasFocus() const;

		/**@brief checks if the window is minimized or not
		 * you can check if the size changed with @ref PGLFW_WINDOW_ICONIFIED
		 */
		bool isIconified() const;

		/**@brief checks if the cursor is in the window or not
		 * you can check if the size changed with @ref PGLFW_CURSOR_ENTER_LEAVE
		 */
		bool hasCursor() const;

		/**@brief gets the drop-target file paths
		 * you can check if they changed with @ref PGLFW_DROP_TARGET_EVENT,
		 */
		std::vector<std::string> getDroppedFiles() const;





		/**@brief register a callback 
		 * check the bellow function_pointer typedefs and the above CBType's for callback options @ref CBType
		 * @tparam type the kind of Callback you want to register
		 * @tparam LAMBDA the param-type of the functor (LAMBDA only)
		 * @param [in] functor the function-pointer you want to register as a callback
		 */

		template <window_input::CBType type,class LAMBDA,std::enable_if_t<!sa::is_delegate<LAMBDA>::value,int> = 0>
		void callback(LAMBDA&& functor)
		{
			if constexpr (type == FRAMEBUFFER)
			{
				m_multicallFBSIZE += FbSize_delegate::create(std::forward<LAMBDA&&>(functor));
			}
			else if constexpr (type == MOUSEBUTTON)
			{
				m_multicallMBUTTON += MButton_delegate::create(std::forward<LAMBDA&&>(functor));
			}
			else if constexpr (type == MOUSECURSOR)
			{
				m_multicallMCURSOR += MCursor_delegate::create(std::forward<LAMBDA&&>(functor));
			}
			else if constexpr (type == MOUSESCROLL)
			{
				m_multicallMSCROLL += MScroll_delegate::create(std::forward<LAMBDA&&>(functor));
			}
			else if constexpr (type == KEY)
			{
				m_multicallKEY += Key_delegate::create(std::forward<LAMBDA&&>(functor));
			}
			else if constexpr (type == CHAR)
			{
				m_multicallCHAR += Char_delegate::create(std::forward<LAMBDA&&>(functor));
			}
			else if constexpr (type == CHAR_MOD)
			{
				m_multicallCHARMOD += CharMod_delegate::create(std::forward<LAMBDA&&>(functor));
			}
		}

		
		/**@brief register a callback 
		 * check the bellow function_pointer typedefs and the above CBType's for callback options @ref CBType
		 * @tparam type the kind of Callback you want to register
		 * @param [in] functor the function-pointer you want to register as a callback
		 */
		template <window_input::CBType type,class... PARAMS>
		void callback(const sa::delegate<void(PARAMS...)>& functor)
		{
			if constexpr(type == FRAMEBUFFER)
			{
				m_multicallFBSIZE += functor;
			}
			else if constexpr(type == MOUSEBUTTON)
			{
				m_multicallMBUTTON += functor;
			}
			else if constexpr(type == MOUSECURSOR)
			{
				m_multicallMCURSOR += functor;
			}
			else if constexpr(type == MOUSESCROLL)
			{
				m_multicallMSCROLL += functor;
			}
			else if constexpr(type == KEY)
			{
				m_multicallKEY += functor;
			}
			else if constexpr(type == CHAR)
			{
				m_multicallCHAR += functor;
			}
			else if constexpr(type == CHAR_MOD)
			{
				m_multicallCHARMOD += functor;
			}
		}


		/**@brief register a callback in preferred syntax
		 * check the bellow function_pointer typedefs and the above CBType's for callback options @ref CBType
		 * @param [in] TYPE the kind of Callback you want to register
		 * @param [in] FUNC the function-pointer you want to register as a callback
		 * @param [in] __VA_ARGS__ (optional) pointer to object of FUNC's member-class
		 */
		#define pwnd_make_callback(TYPE, FUNC, ...) callback< ::pixl::core::window_input:: TYPE>\
			( ::pixl::core::Window::callback_t< ::pixl::core::window_input:: TYPE>::create<& FUNC>(__VA_ARGS__))

		#define pwnd_make_membercallback(TYPE,OBJECT, FUNC, ...) callback< ::pixl::core::window_input:: TYPE>\
			( ::pixl::core::Window::callback_t< ::pixl::core::window_input:: TYPE>::create<OBJECT,& OBJECT :: FUNC>(__VA_ARGS__))
		

		template <window_input::CBType T>
		using callback_t = typename window_input::cb_create<T>::type;

		/**@brief checks if the Window is fullscreen'd
		 */
		bool is_fullscreen() const;

		/**@brief sets the screen to either fullscreen or windowed mode
		 *@param [in] fs true if you want the window in fullscreen and false if you want it in windowed mode
		 */
		void set_fullscreen(bool fs) const;


	private:

		void createWindow(std::size_t width, std::size_t height, const char* title = "Game Window", GLFWmonitor* mon = nullptr,
		                  GLFWwindow* share = nullptr);

		void createCallbacks();
		void destroyCallbacks() const;

		//glfw window pointer
		std::shared_ptr<GLFWwindow> m_windowPtr;

		//all the complex callbacks

		sa::multicast_delegate<void(math::vec<int, 2>)>								m_multicallFBSIZE;
		sa::multicast_delegate<void(KeyButtonMode, MouseButton, ModifierKeys)>		m_multicallMBUTTON;
		sa::multicast_delegate<void(math::vec<double, 2>)>							m_multicallMCURSOR;
		sa::multicast_delegate<void(math::vec<double, 2>)>							m_multicallMSCROLL;
		sa::multicast_delegate<void(int, int, KeyButtonMode, ModifierKeys)>			m_multicallKEY;
		sa::multicast_delegate<void(unsigned)>										m_multicallCHAR;
		sa::multicast_delegate<void(unsigned, ModifierKeys)>						m_multicallCHARMOD;


		friend class ::pixl::detail::window_callback_impl;
		
		//all the attributes that the callbacks can modify
		std::pair<int, const char*> m_lastGLFWError;
		math::vec2i m_windowPos;
		math::vec2<std::size_t> m_windowSize;
		bool m_hasFocus;
		bool m_iconified;
		bool m_hasCursor;
		std::vector<std::string> m_droppedFiles;

	};
}
