import argparse


def res2str(infile, outfile, mode):
    with open(infile)as contents:
        contents = contents.read()
        writer = open(outfile, 'w')

        if mode == "copy":
            writer.write('R"resource(' + contents + ')resource"')

        else:
            writer.write(
                '#pragma once\n inline static const char * const ' + mode + ' = ' + 'R"resource(' + contents + ')resource"')


if __name__ == "__main__":
    parser = argparse.ArgumentParser("A tool to convert a file to an C++ R-String")
    parser.add_argument("inf", help="the file you want to convert into a C++ String Resource")
    parser.add_argument("outf", help="the file you want to save the String resource into")
    parser.add_argument("mode", help="copy if you want the C++ R-String to be included directly (copy-mode) or a name "
                                     "if you instead want it to be wrapped into a resource name")
    args = parser.parse_args()
    res2str(infile=args.inf, outfile=args.outf, mode=args.mode)
