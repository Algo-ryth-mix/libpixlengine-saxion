import res2str
import read_version
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Pixl Prebuild Resource Compiler")
    parser.add_argument("wd", help="the working directory of the project")
    args = parser.parse_args()
    working_directory = args.wd

    with open(working_directory + "ResourceList.txt") as rl:
        rl_contents = rl.readlines()
        for rl_content in rl_contents:
            rl_content = rl_content.rstrip()
            res2str.res2str(working_directory + rl_content, working_directory + rl_content + ".hpp", "copy")

    with open(working_directory + "git_rev.h", "w") as git_rev_file:
        author = read_version.get_git_commit_author(git_path=read_version.git_path_glob)
        version = read_version.get_git_rev(git_path=read_version.git_path_glob)
        decoration = read_version.decorate_file(version, author)
        git_rev_file.write(decoration)
