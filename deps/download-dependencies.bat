@echo off
setlocal enableDelayedExpansion
set "file1=dependencies-key.txt"
set "file2=dependencies-list.txt"
set "out=install.log"

echo Please Wait Resolving Dependencies
cd ../deps
echo Working directory is %cd%

for /f %%N in ('type "%file1%"^|find /c /v ""') do set "cnt=%%N"
>%out% 9<"%file1%" <"%file2%" (
  for /l %%N in (1 1 %cnt%) do (
 
    set "ln1="
    set "ln2="
    <&9 set /p "ln1="
    set /p "ln2="
	if NOT EXIST "!ln2!_Install.bat" (
		echo Resolving dependency !ln2!
		echo Downloading...
		echo "https://www.dropbox.com/sh/!ln1!/!ln2!?dl=1"
		powershell -Command iwr -outf !ln2!.zip "https://www.dropbox.com/sh/!ln1!/!ln2!?dl=1"
		echo Download complete! Extracting...
		7za.exe x !ln2!.zip -o. -aos >nul:

		echo Extraction Complete, Cleaning artifacts
		del !ln2!.zip
	) ELSE (
		echo Skiping step for !ln2!, already installed!
	)
  )
)
for /F "tokens=*" %%A in  ( dependencies-list.txt) do  (
	%%A_Install.bat
	@echo off
	cd ../deps
)

echo on