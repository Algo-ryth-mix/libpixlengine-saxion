-- Animator and Action library

AnimationManager = {}

function AnimationManager.update(script,routineName)
  routine = script.animations[routineName]
  if coroutine.status(routine) ~="dead" then
    ok, msg = coroutine.resume(routine,script.deltaTime())
    if not ok then
      engine.warning(msg)
    end
  end
end

function AnimationManager.updateAll(script)
  for _,routine in pairs(script.animations) do
    if coroutine.status(routine) ~="dead" then
      ok, msg = coroutine.resume(routine,script.deltaTime())
      if not ok then
        engine.warning(msg)
      end
    end
  end
end


function AnimationManager.installAnimation(script,routineName,routine)
  script.animations = script.animations or {}
  script.animations[routineName] = coroutine.create(function() routine(script) end)
end

Action = { finished = false }

function Action:launch()
  while not self.finished do
    local dt = coroutine.yield()
    self:update(dt)
  end
end

function launcher(action)
  action:launch()
end


function __generate_new(self,o)
  o = o or {}
  setmetatable(o,self)
  return o
end

function Action:new(o)
  o = __generate_new(self,o)
  self.__index = self
  return o
end


local DelayAction = Action:new{
  current = 0
}

function DelayAction:new(o)
  o = __generate_new(self,o)
  self.__index = self
  return o
end

function DelayAction:update(dt)
  self.current = self.current + dt
  if self.current >= self.delay then
    self.finished = true
  end
end

function co_delay(timeout)
  launcher(DelayAction:new{
    delay = timeout
  })
end

local LerpAction = Action:new{
  steps = 0
}

function LerpAction:new(o)
  o = __generate_new(self,o)
  self.__index = self
  return o
end

function lerp(v0,v1,t)
  return (1-t) * v0 +t * v1
end

function LerpAction:update(dt)
  self.steps = self.steps + (dt * self.speed)
  arg = lerp(self.v0,self.v1,self.steps)
  if self.steps >= 1 then
    self.finished = true
  else
    self.lambda(arg)
  end
end

function co_lerp(v0,v1,speed,lambda)
  launcher(LerpAction:new{
    v0 = v0,
    v1 = v1,
    speed = speed,
    lambda = lambda
  })
end
