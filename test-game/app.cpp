#include <memory>
#include <common.hpp>

#include <graphics/gl_texture.hpp>
#include <graphics/window_glextension.hpp>
#include <graphics/renderers/gl_cube_renderer.hpp>
#include <graphics/imgui/imgui_impl_pixl.hpp>
#include <graphics/imgui/text_editor.hpp>

#include <game/main_game.hpp>
#include <game/screen.hpp>
#include <game/timing.hpp>
#include <game/input_manager.hpp>

#include <ecs/builtin_script.hpp>
#include <ecs/builtin_transform.hpp>

#include <math/vector.hpp>

#include <io/disk_view.hpp>

#include <SOL/sol.hpp>
#include <IMGUI/imgui.h>




pixl::gl::GLWindowExt gl_context;

class GameScreen : public pixl::game::ScreenBase , public pixl::core::subscriber_base
{

	bool move_forward = false;
	bool move_backward = false;


public:
	GameScreen() : ScreenBase("Game") {}

	void key_handler(int key_code, int scan_code, pixl::core::KeyButtonMode key_mode, pixl::core::ModifierKeys mods)
	{


		switch (key_code)
		{

			//this one is going to be tricky 
			case GLFW_KEY_ENTER:{
					if(!key_mode&& (mods & pixl::core::ModifierKeys:: ALT)) {
						static bool fs = false;
						getApplication()->getWindow().set_fullscreen(fs = !fs);
				}
			}
		}
	}

	void mousebutton_handler(pixl::core::KeyButtonMode press_mode, pixl::core::MouseButton mb, pixl::core::ModifierKeys mods)
	{
		if(press_mode == pixl::core::PRESSED)
		{
			ImGui::GetIO().ClearInputCharacters();
		}
	}
	


	pixl::math::vec<double, 2> ploc = { -1.0,-1.0 };

	void mouse_handler(pixl::math::vec<double, 2> loc)
	{
		static pixl::math::vec2d lastPos;

		static bool initialized = [&]
		{
			lastPos = loc;
			return true;
		}();


		//check if imgui is capturing mouse-events
		const auto&io = ImGui::GetIO();
		if (io.WantCaptureMouse) return;
		if (pixl::game::InputManager::keyDown(pixl::game::keys::SPACE)) return;
		
		const pixl::math::vec2d offs = lastPos - loc;


		actCC(camera, [&](auto& cam) { cam.rotate_event(pixl::math::vector_cast<float>(offs)); });

		lastPos = loc;
	}


protected:

	pixl::ecs::ComponentContainer<pixl::ecs::ScriptComponent> m_script = nullptr;
	pixl::ecs::ComponentContainer<pixl::objects::Cube> cube1 = nullptr;
	pixl::ecs::ComponentContainer<pixl::objects::Cube> cube2 = nullptr;
	pixl::ecs::ComponentContainer<pixl::graphics::Camera> camera = nullptr;


	pixl::ecs::TransformComponent * transform = nullptr;
	std::shared_ptr<Entity> cubeChild;
	std::shared_ptr<Entity> cameraChild;

	void onCreate() override
	{

		using namespace pixl::ecs;

		installRenderer<pixl::gl::CubeRenderer>();

		cubeChild = addChild("CubeChild");
		cameraChild = addChild("CameraChild");

		
		cube2 = addChild("TestChild")->addComponent<pixl::objects::Cube>();

		cube1 = cubeChild->addComponent<pixl::objects::Cube>();

		camera = cameraChild->addComponent<pixl::graphics::Camera>();

		subscribe(getApplication()->getWindow());


		ScriptComponent::prebuildEvents += TransformComponent::lua_registration;

		transform = cubeChild->addComponent("Transform").cast<TransformComponent>().lock().get();

		//add the script component
		m_script = cubeChild->addComponent("ScriptComponent16").cast<ScriptComponent>();

		actCC(m_script, [](auto& script) {
			script.loadScript(pixl::io::DiskView("test.lua"));
		});

		pixl::gl::pImgui::init(&getApplication()->getWindow());

	}

	TextEditor editor;
	TextEditor::LanguageDefinition editor_lang = TextEditor::LanguageDefinition::Lua();

	pixl::gl::Texture cubeTexture = nullptr; 
	
	void onEnterGraphic() override
	{
		cubeTexture = pixl::gl::Texture(pixl::io::DiskView("wall.jpg").read(), true);
		cubeTexture.createTexture();

		auto cube_bind_texture = [&](pixl::objects::Cube& cube) {

			cube.texture(&cubeTexture);
		};

		editor.SetLanguageDefinition(editor_lang);

		const auto test_lua = pixl::io::DiskView("test.lua").read();
		editor.SetText(test_lua.str());


		actCC(cube1, cube_bind_texture);
		actCC(cube2, cube_bind_texture);
	}

	void onEnterLogic() override
	{
		pixl::game::Time::setTargetFps(100.0f);
	}

	void handleKeys()
	{
		using pixl::game::InputManager;
		using namespace pixl::game;

		
		if(InputManager::keyPressed(keys::R))
		{
			actCC(m_script, [](pixl::ecs::ScriptComponent& s)
			{
				s.loadScript(pixl::io::DiskView("test.lua"));
				s.reload();
			});
		}

		if(InputManager::keyPressed(keys::ESCAPE))
			exitApplication();

		if(InputManager::keyPressed(keys::V))
		{
			static bool vs = true;
			gl_context.vsync(vs = !vs);
		}

		if(InputManager::keyReleased(keys::C))
		{
			static bool cursor = true;
			gl_context.cursor_visibility(cursor = !cursor);
		}

		if(InputManager::keyDown(keys::W))
		{
			move_forward = true;
		}
		if(InputManager::keyUp(keys::W))
		{
			move_forward = false;
		}

		if (InputManager::keyDown(keys::S))
		{
			move_backward = true;
		}
		if (InputManager::keyUp(keys::S))
		{
			move_backward = false;
		}
	}
	

	void onUpdate(float dt) override
	{

		using namespace pixl::io::literals;


		static auto& io = ImGui::GetIO();
		if (!(io.WantCaptureKeyboard))
		{
			handleKeys();
		}
		
		static size_t counter = 0;

		static float samples;
		samples += pixl::game::Time::getFps();

		if(++counter >= 100)
		{
			upsStorage.store(samples / static_cast<float>(counter));

			counter = 0;
			samples = 0;
		}
	
		

		actCC(camera,[&](pixl::graphics::Camera& cam){
			if (move_forward) cam.move_event(pixl::graphics::Camera::FWD,dt);
			if (move_backward)cam.move_event(pixl::graphics::Camera::BWD,dt);
		});


		if(pixl::game::InputManager::keyPressed(GLFW_KEY_L))
		{
			pixl::core::println_debug("L Key was pressed!");
		}

	}
	
	char* c = new char[256]{};

	void onDestroy() override
	{
		delete[] c;
	}
	void imgui_create_entity_graph(Entity* e) const
	{
		if(ImGui::TreeNode(e->getName().c_str())){

			for(auto& child: e->getChildren())
			{
				imgui_create_entity_graph(child.get());
			}
			
			if (e->hasParent() && ImGui::Button("Remove"))
			{
				e->detach();
			}
			if(ImGui::Button("Add Child"))
			{
				e->addChild(c);
			}
			ImGui::TreePop();
		}
	}

	
	pixl::math::vec2f screenSize = { 1280.0f,720.0f };

	std::atomic<float> upsStorage;

	void onDraw(float dt) override
	{

		pixl::gl::pImgui::new_frame();
		ImGui::NewFrame();

		ImGui::ShowDemoWindow();

		ImGui::Begin("Hello World");
		editor.Render("test.lua");

		if(ImGui::Button("Reload Script"))
		{

			pixl::io::DiskResource dr(editor.GetText());
			pixl::io::DiskView("test.lua").write(dr);

			actCC(m_script,[&](pixl::ecs::ScriptComponent& script)
			{
					script.loadScript(dr);
					script.reload();
			});
		}
		ImGui::End();
		ImGui::Begin("Lua Console");
		static char* luacon_input = new char[255]{0};

		static std::string res = "";
		if(ImGui::InputText("Lua-Input",luacon_input,255,ImGuiInputTextFlags_EnterReturnsTrue))
		{
			
			actCC(m_script, [&](pixl::ecs::ScriptComponent& script) {
				res = script.script(luacon_input);
			});
			
		}
		ImGui::Text(res.c_str());
		

		ImGui::End();
		ImGui::Begin("Entity Overview");
		{
			
			ImGui::InputText("Child Name", c, 256);

			imgui_create_entity_graph(this);
			
		}
		ImGui::End();

		
		ImGui::SetNextWindowBgAlpha(0.35f);

		const float DISTANCE = 10.0f;
		static int corner = 0;
		ImGuiIO& io = ImGui::GetIO();
		if (corner != -1)
		{
			ImVec2 window_pos = ImVec2((corner & 1) ? io.DisplaySize.x - DISTANCE : DISTANCE, (corner & 2) ? io.DisplaySize.y - DISTANCE : DISTANCE);
			ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
			ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
		}




		static size_t counter = 0;

		static float samples;
		samples += pixl::game::Time::getFps();
		static float fps = 0.0f;
		if (++counter >= 60)
		{
			fps = samples / static_cast<float>(counter);
			counter = 0;
			samples = 0;
		}



		if (ImGui::Begin("Frame Times and Mouse Pos", nullptr, (corner != -1 ? ImGuiWindowFlags_NoMove : 0) | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav))
		{
			ImGui::Text("FPS: %.1f", fps);
			if(ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::Text("FPS or Frames Per Second describes how many frames the engine is currently rendering per second! this is averaged over 60 samples");
				ImGui::EndTooltip();
			}

			
			ImGui::Text("UPS: %.1f", upsStorage.load());
			if(ImGui::IsItemHovered())
			{
				ImGui::BeginTooltip();
				ImGui::Text("UPS or Updates Per Second on the other hand describes how many simulation steps the engine is doing per second, this is averaged over 100 samples");
				ImGui::EndTooltip();

			}
			
			ImGui::Separator();

			
		

			if (ImGui::IsMousePosValid())
				ImGui::Text("Mouse Position: (%.1f,%.1f)", io.MousePos.x, io.MousePos.y);
			else
				ImGui::Text("Mouse Position: <invalid>");
			if (ImGui::BeginPopupContextWindow())
			{
				if (ImGui::MenuItem("Custom", NULL, corner == -1)) corner = -1;
				if (ImGui::MenuItem("Top-left", NULL, corner == 0)) corner = 0;
				if (ImGui::MenuItem("Top-right", NULL, corner == 1)) corner = 1;
				if (ImGui::MenuItem("Bottom-left", NULL, corner == 2)) corner = 2;
				if (ImGui::MenuItem("Bottom-right", NULL, corner == 3)) corner = 3;
				ImGui::EndPopup();
			}
		}
		ImGui::End();
		renderSingle(*cube2);
		renderSingle(*cube1);

		auto cam = camera.lock();
		render(screenSize,cam.get());
		camera.unlock();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		gl_context.draw();
	}

	virtual void onNotify(subject_base* subject_base, event_t event) override
	{
		static pixl::core::Window* w = nullptr;
		if(subject_base == w || (w == nullptr && (w = dynamic_cast<pixl::core::Window*>(subject_base))))
			switch(event){
				case pixl::core::Window::PGLFW_WINDOW_SIZE_CHANGED:{
					screenSize = pixl::math::vector_cast<float>(w->getWindowSize());
					break;
				}
				default:break;
			}
	}

};

class App : public pixl::game::MainGame
{


protected:
	   
	void onAppInit() override
	{
		gl_context.subscribe(getWindow());
		gl_context.lock_context();

		using namespace pixl::ecs;
		registerScriptComponent();
		registerTransformComponent();
		registerCameraComponent();



	}
	void makeScreens() override
	{

		using namespace pixl::core;
		using namespace window_input;

		auto proxy = addScreen<GameScreen>();
		
		getWindow().callback<KEY>(Window::callback_t<KEY>::create<GameScreen, &GameScreen::key_handler>(proxy.as<GameScreen>()));
		getWindow().callback<MOUSECURSOR>(Window::callback_t<MOUSECURSOR>::create<GameScreen,&GameScreen::mouse_handler>(proxy.as<GameScreen>()));
		getWindow().callback<MOUSEBUTTON>(Window::callback_t<MOUSEBUTTON>::create< GameScreen, &GameScreen::mousebutton_handler>(proxy.as<GameScreen>()));
		
		makeScreenActive(proxy);
		
	}


};

PIXL_RUN_APPLICATION(App)