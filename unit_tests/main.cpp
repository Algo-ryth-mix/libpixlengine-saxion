#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <common.hpp>
#include <core/fast_delegate.hpp>

using namespace pixl;

namespace test::delegates
{
	class DelegateTest
	{
	public:
		int func()
		{
			return 10;
		}

		static int static_func()
		{
			return 20;
		}
	};

	inline int free_func()
	{
		return 30;
	}

	auto lambda_func = [&]
	{
		return DelegateTest::static_func() + 30;
	};

	using delegate_t = sa::delegate<int()>;
	
}


TEST_CASE("Delegates","[core]")
{
	using namespace test::delegates;
	SECTION("free functions"){
		SECTION("calling a bound function via a delegate yields the same result as calling it directly")
		{
			delegate_t FreeFuncDelegate = delegate_t::create<&free_func>();
			REQUIRE(FreeFuncDelegate() == free_func());
		}
		SECTION("calling a lambda function via delegate is the same as calling it directly")
		{
			delegate_t LambdaFuncDelegate = delegate_t::create(lambda_func);
			REQUIRE(LambdaFuncDelegate() == lambda_func());
		}
	}
	SECTION("class functions"){
		SECTION("calling a bound Class Function via delegate yields the same result as calling it directly")
		{
			DelegateTest delegate_test;
			delegate_t ClassFunction = delegate_t::create<DelegateTest, & DelegateTest::func>(&delegate_test);
			REQUIRE(ClassFunction() == delegate_test.func());
		}
		SECTION("calling a static Class Function via delegate yields the same result as calling it directly")
		{
			delegate_t ClassFunction = delegate_t::create<& DelegateTest::static_func>();
			REQUIRE(ClassFunction() == DelegateTest::static_func());
		}
	}
}

#include <core/file_core.hpp>

namespace test::file_core
{
	static byte_vec	test_data = { 0x13,0x37,0x04,0x20 };

	bool exists(std::string path)
	{
		FILE* fp = fopen(path.c_str(), "rb");
		if (fp)
		{
			fclose(fp);
			return true;
		}
		return false;
	}

	bool compare_vec(const byte_vec& lhs,const byte_vec& rhs)
	{
		if (lhs.size() != rhs.size()) return false;
		for(size_t i = 0; i < lhs.size(); ++i)
		{
			if (lhs[i] != rhs[i]) return false;
		}
		return true;
	}
	
	
}


TEST_CASE("File Core","[core]")
{
	using namespace test::file_core;
	SECTION("I can write files")
	{
		core::io::write_file("test_write.txt", test_data);
		REQUIRE(exists("test_write.txt"));
	}
	SECTION("I can read back files")
	{
		byte_vec read_back = core::io::read_file("test_write.txt");
		REQUIRE(compare_vec(test_data, read_back));
	}
}
#include <core/instruction_set.hpp>

TEST_CASE("ISA","[core]")
{
	SECTION("I can get info about the ISA")
	{
		std::string vendor_string = core::isa::InstructionSet::Vendor();
		REQUIRE(!vendor_string.empty());
	}
}

#include <core/subscriber.hpp>
#include <core/subject.hpp>

namespace test::observer
{
	class Subject : public core::subject_base
	{
	public:
		enum Events : event_t
		{
			TEST_NOTIFICATION
		};

	public:
		void notify_(Events e) { notify(e); }
		void notifyFirst_(Events e) { notifyFirst(e); }
		void notifyLast_(Events e) { notifyLast(e); }
		void notifyNext_(Events e) { notifyNext(e); }
	
	};

	class Subscriber : public core::subscriber_base
	{

	public:
		bool triggered = false;
		void onNotify(core::subject_base* subject_base, core::subject_base::event_t event) override
		{
			if (event == Subject::TEST_NOTIFICATION) triggered = true;
		}
	};
}


TEST_CASE("Observers","[core]")
{
	using namespace test::observer;
	
	SECTION("Triggering a notify call notifies at least one member")
	{
		Subject subject;
		Subscriber subscriber;
		subscriber.subscribe(subject);
		subject.notify_(Subject::TEST_NOTIFICATION);
		REQUIRE(subscriber.triggered);
	}

	SECTION("Triggering a notify call notifies all members")
	{
		Subject subject;
		Subscriber subscriber1, subscriber2, subscriber3, subscriber4;
		subscriber1.subscribe(subject); subscriber2.subscribe(subject); subscriber3.subscribe(subject);
		subject.notify_(Subject::TEST_NOTIFICATION);
		REQUIRE(subscriber1.triggered);
		REQUIRE(subscriber2.triggered);
		REQUIRE(subscriber3.triggered);
	}
	SECTION("Triggering a notifyFirst call notifies the first member")
	{
		Subject subject;
		Subscriber subscriber1, subscriber2, subscriber3, subscriber4;
		subscriber1.subscribe(subject); subscriber2.subscribe(subject); subscriber3.subscribe(subject);
		subject.notifyFirst_(Subject::TEST_NOTIFICATION);
		REQUIRE(subscriber1.triggered);
		REQUIRE(!subscriber2.triggered);
		REQUIRE(!subscriber3.triggered);
	}
	SECTION("Triggering a notifyLast call notifies the last member")
	{
		Subject subject;
		Subscriber subscriber1, subscriber2, subscriber3, subscriber4;
		subscriber1.subscribe(subject); subscriber2.subscribe(subject); subscriber3.subscribe(subject);
		subject.notifyLast_(Subject::TEST_NOTIFICATION);
		REQUIRE(!subscriber1.triggered);
		REQUIRE(!subscriber2.triggered);
		REQUIRE(subscriber3.triggered);
	}
	SECTION("Triggering a notifyNext call notifies the first then the second member")
	{
		Subject subject;
		Subscriber subscriber1, subscriber2, subscriber3, subscriber4;
		subscriber1.subscribe(subject); subscriber2.subscribe(subject); subscriber3.subscribe(subject);
		subject.notifyNext_(Subject::TEST_NOTIFICATION);
		REQUIRE(subscriber1.triggered);
		subject.notifyNext_(Subject::TEST_NOTIFICATION);
		REQUIRE(subscriber2.triggered);
		REQUIRE(!subscriber3.triggered);
	}

}


#include <ecs/component_base.hpp>
#include <ecs/entity.hpp>
#include <ecs/component_storage.hpp>
#include <ecs/builtin_script.hpp>
namespace test::entities
{
	class TestInterfaceComponent : public pixl::ecs::ComponentBase
	{
	public:
		TestInterfaceComponent() : ComponentBase("TestInterface",16){}
		bool wasTriggered = false;
		void onUpdate() override
		{
			wasTriggered = true;
		}
	};
	
}
struct EntitiesRegistrySetupListener : Catch::TestEventListenerBase
{
	using TestEventListenerBase::TestEventListenerBase;
	void testCaseStarting(Catch::TestCaseInfo const& testInfo) override
	{
		if (testInfo.name == "Entities")
		{
			ecs::registerScriptComponent();
			ecs::ComponentStore::registerComponent<test::entities::TestInterfaceComponent>("TestInterfaceComponent");

		}
	}
};
CATCH_REGISTER_LISTENER(EntitiesRegistrySetupListener);

TEST_CASE("Entities","[ecs]")
{
	using namespace test::entities;
	SECTION("Basic"){
	ecs::Entity e("Test");

		SECTION("Entity has a name")
		{
			REQUIRE(e.getName() == "Test");
		}

		SECTION("Entity can update Components")
		{
			const auto test_interface = e.addComponent<TestInterfaceComponent>();
			e.update(0);
			auto interface_raw = test_interface.lock();
			REQUIRE(interface_raw->wasTriggered);
			
		}
		
		SECTION("Entity can have childs & updates them accordingly")
		{
			const auto child = e.addChild("Test-Child");
			const auto test_interface = child->addComponent<TestInterfaceComponent>();
			e.update(0);
			auto interface_raw = test_interface.lock();
			REQUIRE(interface_raw->wasTriggered);		
		}
	}
	SECTION("Registry"){
		ecs::Entity e("Test");
		
		SECTION("Entities can receive foreign components from the Registry")
		{
			const auto registryComponent = e.addComponent("TestInterfaceComponent");
			REQUIRE(registryComponent.is<TestInterfaceComponent>());
		}
		SECTION("Entities can receive builtin components from the Registry")
		{
			const auto registryComponent = e.addComponent("ScriptComponent16");
			REQUIRE(registryComponent.is<ecs::ScriptComponent>());
		}
		
	}
	
}