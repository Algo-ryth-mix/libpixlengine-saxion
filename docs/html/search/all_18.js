var searchData=
[
  ['x',['x',['../group___math.html#gad1e526ce0943fe3b7435083b7f244879',1,'pixl::math::vec&lt; T, 2 &gt;::x()'],['../group___math.html#ga8209c2c98f60971dbe87243820848049',1,'pixl::math::vec&lt; T, 3 &gt;::x()'],['../group___math.html#gabb9bcc83fb3ec7263a857d1607f544f3',1,'pixl::math::vec&lt; T, 4 &gt;::x()']]],
  ['xop',['XOP',['../classpixl_1_1core_1_1isa_1_1_instruction_set.html#a3ff1112f81160b990185674119499ce2',1,'pixl::core::isa::InstructionSet']]],
  ['xsave',['XSAVE',['../classpixl_1_1core_1_1isa_1_1_instruction_set.html#a5c8e4a746e17b97c9a6bbc9e093b19de',1,'pixl::core::isa::InstructionSet']]]
];
