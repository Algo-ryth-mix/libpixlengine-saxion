var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "acdeilmqrstvw",
  2: "prs",
  3: "acdefgilmqrstvw",
  4: "_abcdefghijklmnopqrstuvwxz~",
  5: "abcdefghiklmnoprsuvwxyz",
  6: "bcefikmrstv",
  7: "acekmst",
  8: "acdefklmnprsu",
  9: "acdemost",
  10: "_cegmps",
  11: "cm"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules"
};

