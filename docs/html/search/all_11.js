var searchData=
[
  ['quaternion',['quaternion',['../structpixl_1_1math_1_1quaternion.html',1,'pixl::math::quaternion&lt; T &gt;'],['../structpixl_1_1math_1_1quaternion.html#a7fb6eb8c18ad1944d2ea96eb6bf57a87',1,'pixl::math::quaternion::quaternion()=default'],['../structpixl_1_1math_1_1quaternion.html#a9d906915fad5f1268bf61867871fa9b3',1,'pixl::math::quaternion::quaternion(const scalar_type &amp;s, vec3&lt; scalar_type &gt; v)'],['../structpixl_1_1math_1_1quaternion.html#a124153f31446fb5bf064e23a7c1dcb9c',1,'pixl::math::quaternion::quaternion(scalar_type &amp;&amp;s, vec3&lt; scalar_type &gt; v) noexcept'],['../structpixl_1_1math_1_1quaternion.html#a55c5010a04a12b2d17c4e437694a2b3f',1,'pixl::math::quaternion::quaternion(const quaternion &amp;other)=default'],['../structpixl_1_1math_1_1quaternion.html#a559fa78b0952ac12bab86f6cce3724bf',1,'pixl::math::quaternion::quaternion(quaternion &amp;&amp;other) noexcept=default']]],
  ['quaternion_2ehpp',['quaternion.hpp',['../quaternion_8hpp.html',1,'']]],
  ['queued_5fdispatcher_2ehpp',['queued_dispatcher.hpp',['../queued__dispatcher_8hpp.html',1,'']]],
  ['queueddispatcher',['QueuedDispatcher',['../classpixl_1_1threading_1_1_queued_dispatcher.html',1,'pixl::threading']]]
];
