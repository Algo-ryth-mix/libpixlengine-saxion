# LIBPIXLENGINE
_A high performace C++ Engine_
>This Project is currently lacking support ... if you are interested in developing for this Engine, read on.
[![Gitter](https://badges.gitter.im/Libpixl/community.svg)](https://gitter.im/Libpixl/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

The goal of this engine is to provide a crossplatform SSE-accelerated Game-Backend that is easy to use and has as little dependencies as possible.

### Installation
Getting up and running is easiest when using windows:
- clone the repository `git clone git@gitlab.com:Algo-ryth-mix/libpixlengine-saxion.git`
- install Vulkan (this is only temporary, I plan to revert to OpenGL for initial development)
- run the dependency resolver `libpixlengine-saxion/resolve-dependencies.bat`
- thats it, you should be able to open and compile the solution now.

For linux and Mac:
- you obviously need an up to date c++ compiler (c++17 at least) I recommend `clang++` or `g++-8` (`sudo apt install clang`)
- make sure you have the dependencies installed`sudo apt-get install cmake xorg-dev libglu1-mesa-dev libglfw3 libglfw3-dev libvulkan1 mesa-vulkan-drivers vulkan-utils libluajit-5.1.2-dev`
- you need premake5 to generate the make-scripts(in the future I hope to do this with an automation on gitlab) download it from here https://premake.github.io/download.html#v5
  - one liner `wget -qO- https://github.com/premake/premake-core/releases/download/v5.0.0-alpha14/premake-5.0.0-alpha14-linux.tar.gz | tar xvz premake5`
  - (optional) I recommend installing premake to `/usr/bin` aka `sudo mv premake5 /usr/bin/`  
- clone the repository `git clone git@gitlab.com:Algo-ryth-mix/libpixlengine-saxion.git`
- generate the build-scripts `cd libpixlengine-saxion-git && premake5 gmake`
- `make`
- test with `cp /bin/Debug/test unit_tests && cd unit_tests && ./test`

Theoretically a simmilar method should work on Mac ... obviously you would need to find a different method to install glfw and vulkan, `brew` maybe ?

### Where is all the Rendering?

Ah yes the keen among you have already realized that there is not a whole lot going on renderingwise right now. this is where I need a bit of support. I have no real Idea on how to create a good rendering pipeline. (I'm more of an optimization guy). if you have any good idea on how this engine should go forth with rendering feel free to experiment in your own branches and annoy me a little when you have an idea. Any help is very appreaciated.

### What is all this SSE nonesense anyways?
If you look through the namespace especially the `math` one you might have found funny functions like
`_mm_storeps` or `_mm_shuffle_ps` and funny types like `__m128` these are optimizations to do rudamentary things like multiplying two matricies very fast. don't worry if you don't understand them, nobody really does, all I can tell you is that they are quite a bit faster than the casual float addition.

### What witchcraft is going on in fast_inv_sqrt?
thats a little easter egg that comes from the good old Quake3 era. there is a bunch more quirky stuff in the code-base and I would like to keep it that way ... if you add your easter-eggs just make sure that they don't obstruct the normal workflow and that they are somewhat interesting (aka not just ASCII-Art)

### It feels a lot like the stl
Yes, please try to keep it like that.

(the why for that is that it is much easier to use the standard library with libpixl, when libpixl is actually like the stl)

### Optimizations
- Threading with Pool 
- Template metaprogamming and early-branch ellimnations
- SSE2+
