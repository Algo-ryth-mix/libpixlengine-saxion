solution "LibPixlEngine"
	configurations { "Debug","Release" }

project "pixlengine"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"

	targetdir "lib/%{cfg.buildcfg}"

	files {"libpixlengine/**.hpp" , "libpixlengine/**.cpp","libpixlengine/**.c","libpixlengine/**.h" }
	includedirs { "libpixlengine/", "deps/include/"}

	filter "configurations:Debug"
		defines {"DEBUG"}
		symbols "On"

	filter "configurations:Release"
		defines {"NDEBUG"}
		optimize "On"

project "test"
	links { "pixlengine", "glfw" , "rt" ,"m" ,"dl", "pthread" ,"vulkan","stdc++fs","luajit-5.1" }
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"

	targetdir "bin/%{cfg.buildcfg}"

	files {"unit_tests/main.cpp" }
	includedirs { "libpixlengine/", "deps/include/"}

	filter "configurations:Debug"
		defines {"DEBUG"}
		symbols "On"

	filter "configurations:Release"
		defines {"NDEBUG"}
		optimize "On"
